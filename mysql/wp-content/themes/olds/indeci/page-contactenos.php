<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<style type="text/css">
	.page-contact .post-content{
		width: 100%;
	}
	.page-contact .form{
		position: relative;
		border-bottom-left-radius: 50px;
		overflow: hidden;
		margin-bottom: 3em;
		min-height: 475px;
	}
	.page-contact .form h1{
		color :#fff;
	}
	.page-contact .form:before{
		content: "";
		background-color: #00639E;
		content: "";
		position: absolute;
		left: 0;
		top: 0;
		width: 150%;
		height: 100%;
		z-index: -1;
	}
	.page-contact form input{
		border-radius: 20px;
		border: 0;
		outline: none;
		padding: .25em 0.5em .25em 1em;
		height: 30px;
		margin-bottom: 1.5em;

	}
	.page-contact form label{
		margin-left: 2.5em;
		margin-bottom: .5em;
	}
	.regions-contact .container{
		width: 100%;
	}
</style>
<section class="content general page-contact m-0 p-0" style="    padding: 0;">
		<div class="bg">
			
			<div class="wrapper">
				<div class="post-content">
					<article>
						<h1 style="margin-top: 2em;">Encuéntranos</h1>
						<ul style="padding-left: 4em;">
							<li>Calle Ricardo Angulo Ramírez N° 694 <br> Urb. Corpac - San Isidro Lima Perú</li>
							<li><strong>Central Telefónica:</strong> (51) 1 225-9898 <br> contactenos@indeci.gob.pe</li>
							<li><strong>Horario de Atención:</strong> Lunes a Viernes de 08:30 am - 16:30 pm</li>
						</ul>
					</article>
					<div class="form" style="">
						<h1 style=" color: #fff; margin-top: 2em;">Escríbenos</h1>
						<form >
							<div>
								<label>Empresa</label>
								<input type="text" name="empresa">
							
								<label >Nombres</label>
								<input type="text" name="names">

								<label>Distrito</label>
								<input type="text" name="distrito">

								<label>Teléfono</label>
								<input type="text" name="names">

							</div>
							<div>
								<label>Email</label>
								<input type="text" name="email">
							
								<label>Déjanos un Mensaje</label>
								<input type="text" name="empresa" style="height: 177px;">
							</div>
							<button type="button">enviar </button>
						</form>
					</div>
				</div>
				<div class="post-content regions-contact">
					<article style="width: 90%;margin: 0 5%;">
						<h1>Encuéntranos en tu Región</h1>
						<?php get_template_part( 'partials/content', 'regiones' ) ?>
					</article>
				</div>
			</div>	

		</div>
		
	</section>

<?php get_footer();
