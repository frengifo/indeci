<?php 
	
	add_theme_support( 'post-thumbnails' ); 
	add_theme_support( 'menus' );

	add_theme_support( 'post-formats', array( 'gallery','video' ) );
	

	function is_page_parent($pid) {      // $pid = The ID of the page we're looking for pages underneath
		global $post;         // load details about this page
		if( ($post->post_parent==$pid||is_page($pid) ) ) 
	               return true;   // we're at the page or at a sub page
		else 
	               return false;  // we're elsewhere
	};


	function themename_custom_post_formats_setup() {
	    // add post-formats to post_type 'page'
	    add_post_type_support( 'que_hacemos', 'post-formats' );
	 
	    // add post-formats to post_type 'my_custom_post_type'
	    //add_post_type_support( 'my_custom_post_type', 'post-formats' );
	}
	add_action( 'init', 'themename_custom_post_formats_setup' );
 ?>