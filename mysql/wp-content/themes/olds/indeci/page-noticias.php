<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>Portal de Noticias</h3>
					<ul>
						<li style="width: 97.5%"><a href="#" class="active">Noticias</a></li>
						<li><a href="#">COEN</a></li>
						<li><a href="#">#INDECITeRecomienda</a></li>
					</ul>
				</nav>

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
			<div class="post-content">
				<h1>Noticias</h1>
				<div class="filter">
					<span>Buscar por:</span>
					<select><option>Todas las Categorías</option></select>
					<select><option>Junio</option></select>
					<select><option>2018</option></select>
				</div>

				<section class="list-news">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

						<article>
							<a href="<?php the_permalink() ?>" class="img">
								<?php the_post_thumbnail() ?>
							</a>
							<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
							<p><?php the_date() ?></p>
							<a href="<?php the_permalink(); ?>" class="lnk-view">Ver más</a>
						</article>

					<!-- post -->
					<?php endwhile; ?>
					<!-- post navigation -->
					<?php else: ?>
					<!-- no posts found -->
					<?php endif; ?>

				</section>
			</div>
		</div>	
		
	</section>

<?php get_footer();
