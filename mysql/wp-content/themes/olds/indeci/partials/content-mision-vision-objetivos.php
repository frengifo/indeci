<?php $asset_path = get_template_directory_uri(); ?>
<div class="article-custom">

	<div class="vision">
		<h2>Visión</h2>
		<div> <img src="<?php echo $asset_path; ?>/assets/img/Indeci-jefe.png"> </div>
		<div>
			<?php the_field('contenido_vision') ?>
		</div>
	</div>
	<div class="mision">
		<h2>Misión</h2>
		<div>
			<?php the_field('contenido_mision') ?>
		</div>
		<div>
			<img src="<?php echo $asset_path; ?>/assets/img/mision-indeci.png">
		</div>
	</div>					
	<div class="objetivos">

		<h2>Objetivos</h2>

		<?php if (get_field('generales')): ?>
			
			<?php foreach (get_field('generales') as $k => $v): ?>
	
				<div class="goal <?php echo isset($styles[$k]) ? $styles[$k]:$styles[0] ?>-style">
					<div>
						<a href="#"><?php echo ($k+1) ?></a> <span><?php echo $v['titulo_generales'] ?></span>
					</div>
					<div>
						<ul>
							<?php foreach ($v['especificos'] as $j => $ob): ?>
								
								<li><?php echo $ob['titulo_especificos']; ?></li>

							<?php endforeach ?>

						</ul>
					</div>
				</div>

			<?php endforeach ?>

		<?php endif ?>

	</div>
	
</div>