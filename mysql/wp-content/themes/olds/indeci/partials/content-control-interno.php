<?php $asset_path = get_template_directory_uri(); ?>
<div class="table-block blue-table">
	<h3>ARCHIVOS</h3>
	<?php $documentos = get_field('documentos'); ?>
	
	<?php if ($documentos): ?>

		<?php foreach ($documentos as $k => $v): ?>
			
			<table>
				<thead>
					<tr>
						<th><?php echo $v['nombre']; ?></th>
						<th width="200">ARCHIVO</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $v['sumilla']; ?></td>
						<td><a href="<?php echo $v['archivo']; ?>" style="color:#333;font-size: 18px;" target="_blank"><i class="fas fa-download"></i></a></td>
					</tr>
				</tbody>
			</table>

		<?php endforeach ?>
	
	<?php endif ?>

</div>