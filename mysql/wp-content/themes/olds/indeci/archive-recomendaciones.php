<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
			<div class="post-content">
				<h1>Recomendaciones</h1>
				
				<section class="list-news alerts-archive">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

						<article>
							<a href="<?php the_permalink() ?>" class="img">
								<img src="<?php the_post_thumbnail_url(); ?>" width="225" height="180">
							</a>
							<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
							<!-- <p><?php the_date(); ?></p> -->
							<a href="<?php the_permalink(); ?>" class="lnk-view">Ver más</a>
						</article>

					<!-- post -->
					<?php endwhile; ?>
					<!-- post navigation -->
					<?php else: ?>
					<!-- no posts found -->
					<?php endif; ?>

				</section>
			</div>
		</div>	
		
	</section>

<?php get_footer();
