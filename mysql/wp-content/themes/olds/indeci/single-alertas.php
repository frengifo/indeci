<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>Portal de Alertas</h3>
					<!-- <ul>
						<li style="width: 97.5%"><a href="/alertas/" class="active">Alertas</a></li>
						<li><a href="#">COEN</a></li>
						<li><a href="#">#INDECITeRecomienda</a></li>
					</ul> -->

					<?php wp_nav_menu( ['menu' => 'menu-alertas'] ) ?>
				</nav>

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php 
				$terms = get_the_terms( get_the_ID(), 'informe' );
				$term = array_shift( $terms );
			?>
			<div class="post-content">
				<h1><a href="/alertas/">Alertas</a> / <?php echo $term->name ?></h1>
				<div class="filter">
					<span>Buscar por:</span>
					<select><option>Todas las Categorías</option></select>
					<select><option>Junio</option></select>
					<select><option>2018</option></select>
				</div>


				<article>
					<span class="date"><?php the_date(); ?></span>
					<h1><?php the_title(); ?></h1>
					<div class="article">
						<?php the_content() ?>
						<p><a href="<?php the_field('archivo') ?>" class="btn-download" target="_blank"> <i class="fas fa-download"></i>  DESCARGAR ARCHIVO</a></p>
					</div>

				</article>
			</div>
		<!-- post -->
		<?php endwhile; ?>
		<!-- post navigation -->
		<?php else: ?>
		<!-- no posts found -->
		<?php endif; ?>
		</div>	
		
	</section>

<?php get_footer();
