<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>Información Institucional</h3>
					<?php wp_nav_menu( ['menu' => 'menu-institucion'] ) ?>
					<!-- <ul>
						<li ><a href="/institucion/acerca-de-indeci/" class="active">Acerca de Indeci</a></li>
						<li><a href="/institucion/principios/">Principios de Defensa Civil</a></li>
						<li><a href="/institucion/organigrama/">Organigrama</a></li>
						<li><a href="/institucion/marco-legal/">Marco Legal</a></li>
						<li><a href="/institucion/directorio/">Directorio</a></li>
						<li><a href="/institucion/codigo-etica/">Código de Ética</a></li>
						<li><a href="/institucion/control-interno/">Sistema de Control Interno</a></li>
					</ul> -->
				</nav>

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
			<?php $hide_article = [12,14,17]; ?>
			<div class="post-content">
				<article>

					<h1><?php the_title(); ?></h1>
					<div class="article <?php echo ( in_array(get_the_ID(), $hide_article ) OR ($post->post_parent == 21) ) ? "hidden":""; ?>">
						<?php the_content() ?>
					</div>

					<!-- ACERCA DE INDECI -->
					<?php if (get_field('contenido_mision')): ?>

						<?php get_template_part( 'partials/content', 'mision-vision-objetivos' ); ?>

					<?php endif ?>
					
					<!-- REGIONES -->
					<?php if ( get_the_ID() == 19 ): ?>
						<?php get_template_part( 'partials/content', 'regiones' ) ?>
					<?php endif ?>

					<!-- MARCO LEGAL -->
					<?php if ( get_the_ID() == 12 ): ?>
						<?php get_template_part( 'partials/content', 'marco-legal' ) ?>
					<?php endif ?>

					<!--DIRECTORIO -->
					<?php if ( get_the_ID() == 14 ): ?>
						<?php get_template_part( 'partials/content', 'directorio' ) ?>
					<?php endif ?>

					<!--CODIGO DE ETICA -->
					<?php if ( get_the_ID() == 17 ): ?>
						<?php get_template_part( 'partials/content', 'codigo-etica' ) ?>
					<?php endif ?>

					<!--CONTROL INTERNO -->
					<?php if ( $post->post_parent == 21 ): ?>
						<?php get_template_part( 'partials/content', 'control-interno' ) ?>
					<?php endif ?>

				</article>
			</div>
		</div>	
		
	</section>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>
<?php get_footer();
