<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
			<div class="post-content">

				<article>
					<h1 class="text-center" style="font-size: 2.5em;margin-bottom: 0;"><?php the_title(); ?></h1>
					<div class="article">
						<?php the_content() ?>
						<h3>Galería</h3>
						<div class="gallery" style="float: left;margin-bottom: 2em;">
							<?php foreach (get_field('galeria') as $k => $v): ?>
								<a href="<?php echo $v['url']; ?>" data-fancybox="gallery" style="background: url('<?php echo $v['url']; ?>') center no-repeat;float: left;background-size: cover;">
									<img src="<?php echo $v['url']; ?>" width="200" height="150" style="opacity: 0;">
								</a>
							<?php endforeach ?>
						</div>
						<p>&nbsp;</p>
						<h3>Archivos</h3>
						<?php foreach (get_field('archivos') as $k => $v): ?>
							
							<p><a href="<?php echo $v['archivo'] ?>" style="float:left;font-size: 14px;display: inline-block;max-width: initial;" class="btn-download" target="_blank"> <i class="fas fa-download"></i>  <?php echo $v['nombre'] ?></a></p>

						<?php endforeach ?>
						<p>&nbsp;</p>

						<section class="list-news alerts-archive" style="  padding-left: 0;  width: 100%;    text-align: left;    padding-top: 2em;">
							<?php 
								$recomendaciones = get_posts([
								  'post_type' => 'recomendaciones',
								  'post_status' => 'publish',
								  'numberposts' => 5
								]);
							 ?>
							 <h3 style="  font-size: 2.5em; text-align: center; ">Recomendaciones</h3>
							<?php foreach ( $recomendaciones as $post ) : setup_postdata( $post ); ?>

								<article>
									<a href="<?php the_permalink() ?>" class="img">
										<img src="<?php the_post_thumbnail_url(); ?>" width="225" height="180">
									</a>
									<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
									<!-- <p><?php the_date(); ?></p> -->
									<a href="<?php the_permalink(); ?>" class="lnk-view">Ver más</a>
								</article>

							<?php endforeach; 
							wp_reset_postdata();?>
						</section>
					</div>

				</article>
			</div>
		<!-- post -->
		<?php endwhile; ?>
		<!-- post navigation -->
		<?php else: ?>
		<!-- no posts found -->
		<?php endif; ?>
		</div>	
		
	</section>

<?php get_footer();
