<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<link href="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps.css" rel="stylesheet" type="text/css" />
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>Respuesta</h3>
					<ul>
						<li style="width:97.5%;"><a href="/que_hacemos/nosotros-respuesta/" class="active">Nosotros</a></li>
						<li ><a href="/que_hacemos/nosotros-respuesta/">Organización</a></li>
						<li ><a href="/que_hacemos/nosotros-respuesta/">Funciones</a></li>
					</ul>

					<div class="others">
						<h3 ><a href="/que_hacemos/nosotros-preparacion/">Preparación</a></h3>
						<h3 ><a href="/que_hacemos/nosotros-defocaph/">DEFOCAPH</a></h3>
					</div>
				</nav>

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="post-content ">
					<article>
						<h1><?php the_title(); ?></h1>
						<div class="article">
							<?php the_content(); ?>
							<!-- <img src="<?php echo $asset_path; ?>/assets/img/respuesta.png"> -->
							<p>&nbsp;</p>
							<?php $procedimientos = get_field('como_procedemos'); ?>
							<?php if ($procedimientos): ?>
								<div class="process">
									<div class="btns">
										<?php foreach ($procedimientos as $k => $v): ?>
											
											<a href="javascript:;" data-display="#proc-<?php echo $k ?>" class="<?php echo $k == 0 ? "active":""; ?>" style="background-image: url('<?php echo $v['icono']; ?>')"><?php echo $v['titulo']; ?></a>

										<?php endforeach ?>

									</div>
									<?php foreach ($procedimientos as $k => $v): ?>
										<div id="proc-<?php echo $k; ?>" class="display-info text-center" style="display:<?php echo $k == 0 ? "block":"none"; ?>;">
											<h3><?php echo strtoupper($v['titulo']); ?></h3>
											<?php echo $v['descripcion']; ?>
										</div>
									<?php endforeach ?>
								</div>
							<?php endif ?>

							<?php $actividades = get_field('actividades'); ?>
							<?php if ($actividades): ?>
								<div class="activities">
									<div class="btns">
										<?php foreach ($actividades as $k => $v): ?>
											
											<a href="javascript:;" data-display="#act-<?php echo $k ?>" class="<?php echo $k == 0 ? "active":""; ?>"><?php echo $v['titulo']; ?></a>

										<?php endforeach ?>

									</div>
									<?php foreach ($actividades as $k => $v): ?>
										<div id="act-<?php echo $k; ?>" class="display-info text-center" style="display:<?php echo $k == 0 ? "block":"none"; ?>;">
											<h3><?php echo strtoupper($v['titulo']); ?></h3>
											<?php echo $v['descripcion']; ?>
										</div>
									<?php endforeach ?>
								</div>
							<?php endif ?>
							<p>&nbsp;</p>
							<h3 class="text-center">HERRAMIENTAS</h3>		

							<?php $herramientas = get_field('herramientas'); ?>

							<?php if ($herramientas): ?>	
									<div class="table-block blue-table">
										<!-- <p>Ley del Sistema Nacional de Gestión del Riesgo de Desastres</p> -->
											<table>
												<thead>
													<tr>
														<th width="280">Protocolo</th>
														<th>Sumilla</th>
														<th width="80">PDF</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($herramientas as $k => $v): ?>
														<tr>
															<td><?php echo $v['protocolo'] ?></td>
															<td><?php echo $v['sumilla'] ?></td>
															<td><a href="<?php echo $v['pdf'] ?>" target="_blank"><img src="<?php echo $asset_path; ?>/assets/img/icon-pdf.png"></a></td>
														</tr>
													<?php endforeach ?>
												</tbody>
											</table>
												
									</div>

							<?php endif ?>

						</div>
					</article>
				</div>

			<!-- post -->
			<?php endwhile; ?>
			<!-- post navigation -->
			<?php else: ?>
			<!-- no posts found -->
			<?php endif; ?>


		</div>	
		
	</section>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps-libs.js" type="text/javascript"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps-panzoom.js"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps.min.js" type="text/javascript"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/peru.js" type="text/javascript"></script>
<?php get_footer();
