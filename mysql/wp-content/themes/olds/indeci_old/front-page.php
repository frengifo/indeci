<?php

get_header(); ?>
<?php $asset_path = get_template_directory_uri(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>	
	<section class="content">
			
			<section class="banners">
				<ul >
					<?php $sliders = get_field('slide'); ?>
					<?php foreach ($sliders as $k => $v): ?>
						
						<li style="background-image:url('<?php echo $v['imagen_version_movil'] ?>'); ">
							<div class="wrapper">
								<a href="<?php echo $v['enlace']; ?>" target="_blank" class="btn-orange">ver más</a>
							</div>
							<a href="<?php echo $v['enlace']; ?>" target="_blank"><img src="<?php echo $v['imagen'] ?>"></a>
						</li>

					<?php endforeach ?>

				</ul>
			</section>
			<section class="notifications">
				<div class="first">
					
					<div class="slider">
						<?php $sliders = get_field('slides_1'); ?>
						<?php foreach ($sliders as $k => $v): ?>
							<div >
								<article class="bg-blue">
									<h2><a href="<?php echo $v['enlace'] ?>" target="_blank"><?php echo $v['titulo'] ?></a></h2>
									<img src="<?php echo $v['icono'] ?>">
									<h4><?php echo $v['tipo_emergencia'] ?></h4>
									<p><?php echo $v['descripcion'] ?></p>
								</article>
							</div>
						<?php endforeach ?>
					</div>

				</div>
				<div class="second">
					<div class="slider">
						<?php $sliders = get_field('slides_2'); ?>
						<?php foreach ($sliders as $k => $v): ?>
							<div>
								<article class="bg-blue">
									<a href="<?php echo $v['enlace'] ?>" target="_blank">
										<span><b><?php echo $v['titulo'] ?></b></span>
										<img src="<?php echo $v['portada'] ?>">
										<article class="info">
											<h4><?php echo $v['titulo'] ?></h4>
											<p><?php echo $v['descripcion'] ?></p>
										</article>
									</a>
								</article>
							</div>
						<?php endforeach ?>
					</div>

					<div class="slider">
						<?php $sliders = get_field('slides_3'); ?>
						<?php foreach ($sliders as $k => $v): ?>
							<div>
								<article class="bg-blue">
									<a href="<?php echo $v['enlace'] ?>" target="_blank">
										<span><b><?php echo $v['titulo'] ?></b></span>
										<img src="<?php echo $v['portada'] ?>">
										<article class="info">
											<h4><?php echo $v['titulo'] ?></h4>
											<p><?php echo $v['descripcion'] ?></p>
										</article>
									</a>
								</article>
							</div>
						<?php endforeach ?>
					</div>

					<div class="slider">
						<?php $sliders = get_field('slides_4'); ?>
						<?php foreach ($sliders as $k => $v): ?>
							<div>
								<article class="bg-blue">
									<a href="<?php echo $v['enlace'] ?>" target="_blank">
										<span><b><?php echo $v['titulo'] ?></b></span>
										<img src="<?php echo $v['portada'] ?>">
										<article class="info">
											<h4><?php echo $v['titulo'] ?></h4>
											<p><?php echo $v['descripcion'] ?></p>
										</article>
									</a>
								</article>
							</div>
						<?php endforeach ?>
					</div>

					<div class="slider">
						<?php $sliders = get_field('slides_5'); ?>
						<?php foreach ($sliders as $k => $v): ?>
							<div>
								<article class="bg-blue">
									<a href="<?php echo $v['enlace'] ?>" target="_blank">
										<span><b><?php echo $v['titulo'] ?></b></span>
										<img src="<?php echo $v['portada'] ?>">
										<article class="info">
											<h4><?php echo $v['titulo'] ?></h4>
											<p><?php echo $v['descripcion'] ?></p>
										</article>
									</a>
								</article>
							</div>
						<?php endforeach ?>
					</div>
				</div>
			</section>

			<?php $url ="/indeci-y-el-ejercito-firman-convenio-gestion-de-riesgo/" ;?>
			<section class="information posts">
				
				<div class="wrapper">
					<div class="back-white">
						<div class="content-box">
							<div class="buttons">
								<a href="/noticias" class="news">NOTICIAS</a>
								<a href="#" class="newsletter">BOLETINES</a>
								<a href="/noticias" class="all-news">Ver todas las noticias</a>
							</div>
							<div class="list">

								<?php 
									$noticias = get_posts([
									  'post_status' => 'publish',
									  'numberposts' => 3
									]);
								 ?>

								<?php foreach ( $noticias as $post ) : setup_postdata( $post ); ?>
									
									<article>
										<a href="<?php the_permalink() ?>" class="img">
											<?php the_post_thumbnail() ?>
										</a>
										<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
										<span class="date"> <?php the_date() ?></span>
										<p><?php the_excerpt() ?></p>
									</article>

								<?php endforeach; 
								wp_reset_postdata();?>

							</div>
							<p class="text-center m-0">
								<a href="/noticias" class="view-more">VER MÁS</a>
							</p>
						</div>
						<aside class="text-center">
							<a class="twitter-timeline" data-height="320" data-theme="light" data-link-color="#FAB81E" href="https://twitter.com/indeciperu?ref_src=twsrc%5Etfw"></a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
							<p>&nbsp;</p>
							<div class="fb-page" data-href="https://www.facebook.com/indeci/" data-tabs="timeline" data-width="390" data-height="320" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/indeci/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/indeci/">Instituto Nacional de Defensa Civil Perú</a></blockquote></div>
						</aside>
					</div>
				</div>


			</section>

			<section class="tato">
				<!-- DESKTOP -->
				<a href="#"><img src="<?php echo $asset_path; ?>/assets/img/banner-tato.png"></a>
			</section>

			
		
	</section>
<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>
<?php get_footer();
