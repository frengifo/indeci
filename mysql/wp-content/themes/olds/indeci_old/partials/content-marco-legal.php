<?php $rows = get_field('contenido_flex'); ?>
<?php $styles = ['blue','orange','green']; ?>
<?php $asset_path = get_template_directory_uri(); ?>
<?php if ( $rows ): ?>
	
	<?php foreach ($rows as $k => $nivel): ?>
		
		<div class="table-block <?php echo isset($styles[$k]) ? $styles[$k]:$styles[0] ?>-table">
			<h3><?php echo $nivel['titulo']; ?></h3>

			<?php foreach ($nivel['information'] as $j => $subnivel): ?>
				

				<p><?php echo $subnivel['sub_titulo']; ?></p>
				<table>
					<thead>
						<tr>
							<th>Cód.</th>
							<th>Norma</th>
							<th>Sumilla</th>
							<th>Fecha <br>Norma</th>
							<th>Fecha <br>Publicación</th>
							<th>PDF</th>
						</tr>
					</thead>
					<tbody>
						
						<?php foreach ($subnivel['documents'] as $i => $row): ?>
							
							<tr>
								<td><?php echo $row['codigo']; ?></td>
								<td><?php echo $row['norma']; ?></td>
								<td><?php echo $row['sumilla']; ?></td>
								<td><?php echo $row['fecha_normal']; ?></td>
								<td><?php echo $row['fecha_publicacion']; ?></td>
								<td><a href="<?php echo $row['archivo_pdf']; ?>" target="_blank"><img src="<?php echo $asset_path; ?>/assets/img/icon-pdf.png"></a></td>
							</tr>

						<?php endforeach ?>

					</tbody>
				</table>

			<?php endforeach ?>

		</div>

	<?php endforeach ?>

<?php endif ?>	