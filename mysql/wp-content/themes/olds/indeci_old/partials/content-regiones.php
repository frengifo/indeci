<?php $asset_path = get_template_directory_uri(); ?>
<link href="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps.css" rel="stylesheet" type="text/css" />
<p>&nbsp;</p>
<h3>Encuéntranos</h3>
<div class="regions">
	<?php 
		$regiones = get_posts([
		  'post_type' => 'regiones',
		  'post_status' => 'publish',
		  'numberposts' => -1
		]);
	 ?>

	<?php foreach ( $regiones as $post ) : setup_postdata( $post ); ?>
		<?php $reg = get_field_object('region'); ?>
		<a href="javascript:;" class="<?php the_field('region') ?>" data-code="<?php the_field('region') ?>" data-name="<?php echo $reg['choices'][ get_field('region') ] ?>"><?php the_field('nombre') ?></a>
	<?php endforeach; 
	wp_reset_postdata();?>
</div>
<p>&nbsp;</p>
<div class="container">
	<div class="info">
		<?php get_template_part( 'partials/content', 'regiones-map' ); ?>
	</div>
	<!-- Map html - add the below to your page -->
    <div class="jsmaps-wrapper" id="peru-map"></div>
    <!-- End Map html -->

</div>


<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps-libs.js" type="text/javascript"></script>
<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps-panzoom.js"></script>
<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps.min.js" type="text/javascript"></script>
<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/peru.js" type="text/javascript"></script>