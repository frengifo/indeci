<div class="article-custom">
							
	<nav class="about-us">
		
		<h2><a href="javascript:;" class="active" data-display=".mision">Misión</a></h2>
		<h2><a href="javascript:;" data-display=".vision">Visión</a></h2>
		<h2><a href="javascript:;" data-display=".objetivos">Objetivos</a></h2>

	</nav>

	<div class="display">
		<div class="mision">
			<?php the_field('contenido_mision') ?>
		</div>

		<div class="vision">
			<?php the_field('contenido_vision') ?>
		</div>
		<?php $styles = ['blue','orange', 'purple']; ?>
		<div class="objetivos">

			<?php if (get_field('generales')): ?>
				
				<?php foreach (get_field('generales') as $k => $v): ?>
		
					<div class="goal <?php echo isset($styles[$k]) ? $styles[$k]:$styles[0] ?>-style">
						<div>
							<a href="#"><?php echo ($k+1) ?></a> <span><?php echo $v['titulo_generales'] ?></span>
						</div>
						<div>
							<ul>
								<?php foreach ($v['especificos'] as $j => $ob): ?>
									
									<li><?php echo $ob['titulo_especificos']; ?></li>

								<?php endforeach ?>

							</ul>
						</div>
					</div>

				<?php endforeach ?>

			<?php endif ?>

		</div>
	</div>
</div>