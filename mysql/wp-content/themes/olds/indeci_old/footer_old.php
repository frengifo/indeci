<?php $asset_path = get_template_directory_uri(); ?>
		<section class="stats">
			<div class="wrapper">
				<?php $numeros = get_field('indeci_numeros', 2); ?>

                <?php if($numeros): ?>

                	<?php foreach ($numeros as $k => $v): ?>
                		
							<span>
								<b><?php echo $v['numero']; ?></b>
								<small><?php echo $v['titulo_numero']; ?></small>
							</span>

                	<?php endforeach ?>

				<?php endif; ?>
				
			</div>

		</section>

		<section class="links wrapper">
			<h2>Enlaces de Interés</h2>
			<?php $enlaces = get_field('enlaces_interes', 2); ?>
			<div class="slider">
				 <?php if($enlaces): ?>

                	<?php foreach ($enlaces as $k => $v): ?>
                		
						<div>
							<a href="<?php echo $v['enlace_interes']; ?>" target="_blank">
								<img src="<?php echo $v['image_interes']; ?>">
							</a>
						</div>

					<?php endforeach ?>

				<?php endif; ?>

			</div>
		</section>
		<footer class="site-footer">
			<a href="javascript:;" class="btn"><i class="fas fa-arrow-down"></i></a>
			<div class="wrapper">
				<div>
					<h3>¿Qué Hacemos?</h3>
					<div class="block">
						<ul>
							<li><a href="#">Preparación</a></li>
							<li><a href="#">Respuesta</a></li>
							<li><a href="#">Rehabilitacíón</a></li>
							<li><a href="#">Políticas, Planes y Evaluación</a></li>
							<li><a href="#">DEFOCAPCH</a></li>
						</ul>
						<h3>Direcciones Desconcentradas</h3>
						<ul>
							<li><a href="#">Direcciones Desconcentradas del INDECI</a></li>
							<li><a href="#">Almacénes DDI</a></li>
						</ul>
					</div>
				</div>

				<div>
					<h3>Cooperación Internacional</h3>
					<div class="block">
						<ul>
							<li><a href="#">Funciones de la OGCAI</a></li>
							<li><a href="#">Actividades de Cooperación Internacional</a></li>
							<li><a href="#">Asistencia Humanitaria Internacional en casos de desastres de gran magnitud</a></li>
							<li><a href="#">Red Humanitaria Nacional</a></li>
						</ul>
					</div>
				</div>

				<div>
					<h3>COEN</h3>
					<div class="block">
						<ul>
							<li><a href="#">Marco Legal</a></li>
							<li><a href="#">Organización</a></li>
							<li><a href="#">Funciones</a></li>
							<li><a href="#">Documentos de gestion</a></li>
							<li><a href="#">Herramientas de Información Geográficas</a></li>
							<li><a href="#">Instrumento de gestión de información</a></li>
							<li><a href="#">Sistema de Comunicaciones de los COEs</a></li>
							<li><a href="#">Galería</a></li>
							<li><a href="#">Protocolo Operativo del sistema Nacional de Alerta de Tsunami</a></li>
							<li><a href="#">Directorio</a></li>
						</ul>
					</div>
				</div>

				<div>
					<h3>COEN</h3>
					<div class="block">
						<ul>
							<li><a href="#">¿Qué es el FONDES?</a></li>
							<li><a href="#">Zonas Expuestas a Alto Peligro</a></li>
							<li><a href="#">Declaratorias de Emergecia</a></li>
							<li><a href="#">Participantes y Requisitos</a></li>
							<li><a href="#">Formatos de Solicitud</a></li>
							<li><a href="#">Reporte de Transferencias</a></li>
							<li><a href="#">Contáctenos</a></li>
						</ul>
					</div>
				</div>

				<div>
					<h3>CONTACTO</h3>
					<div class="block">
						<p>Calle Ricardo Angulo Ramirez N° 694</p>
					</div>
				</div>

			</div>
			<div class="bottom">
				<div class="wrapper">
					<div class="social">
						<a href="#"><i class="fab fa-facebook-f"></i></a>
						<a href="#"><i class="fab fa-twitter"></i></a>
						<a href="#"><i class="fab fa-youtube"></i></a>
					</div>
					<p>2018 C INDECI. Todos los derechos reservados</p>
				</div>
			</div>
		</footer><!-- #colophon -->
	</section><!-- .site-content-contain -->
</div><!-- #page -->


<script type="text/javascript" src="<?php echo $asset_path; ?>/assets/js/plugins.js"></script>
<script type="text/javascript" src="<?php echo $asset_path; ?>/assets/js/main.js"></script>
<?php wp_footer(); ?>

</body>
</html>
