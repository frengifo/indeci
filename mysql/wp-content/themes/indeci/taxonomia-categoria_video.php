<?php
get_header(); 

$asset_path = get_template_directory_uri();
?>


<div class="crumbs">
  <div class="wrapper">
    <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
  </div>
</div>
<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content wrapper" role="main" style="transform: none;">
      <div class="container" style="transform: none;">

         <div class="row single-main-content single-1" style="transform: none;">
            
            <?php $current_term_title = single_term_title("", false); ?>
            <div class="row">
	            <div class=" main-content col-sx-12 col-sm-12 ">
	               <div class="content-inner">
	                  <div class="box-article">
	                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
	                        <div class="box-title clearfix">
	                           <h2 class="title-left"><?php echo $current_term_title; ?></h2>
	                        </div>
							
						    <div class="row vid-lista box-blog archive-blog row large-vertical clearfix active">
	                        <?php $i = 0; ?>
	                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


									<?php if ( $i == 0): ?>
										
									
			                        	<div class="slider-last first_gallery">
				                        	
				                            <?php get_template_part( 'partials/content', 'post-list-item' ) ?>

			                            </div>

										<?php get_template_part( 'partials/content', 'category-list' ) ?>	                            
		
							        <?php else: ?>

							        	<div class="lists-posts-gallery">
	                                		<?php get_template_part( 'partials/content', 'post-list-item' ) ?>
							        	</div>
							        		

							        <?php endif ?>
					         	
		                        		

		                          	<?php $i++; ?>
		                        <!-- post -->
								<?php endwhile; ?>
								<?php else: ?>
									<?php get_template_part( 'partials/content', 'category-list' ) ?>	                            
									<h3>Lo sentimos no encontramos la información que buscabas.</h3>
								<!-- post navigation -->
								<?php endif; ?>
						    </div>



	                     </article>
	                  </div>
	               </div>
	            </div>
	        </div>
         </div>
      </div>
   </div>
</div>

<?php get_footer();
