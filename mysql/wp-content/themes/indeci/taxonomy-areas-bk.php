<?php
get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<?php //if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="banner-featured" style="background-image: url('<?php echo $asset_path ?>/assets/img/banner-indeci.png')">
   <div class="container"> <h1>Instituto Nacional de Defensa Civil</h1> </div>
</div>
<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content" role="main" style="transform: none;">
      <div class="container" style="transform: none;">
         <div class="row single-main-content single-1" style="transform: none;">
            <div id="archive-sidebarsss" class="sidebar sidebar-left col-sx-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar single-sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
               <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 0px; left: 332.333px;">
                  <aside id="categories-8" class="widget widget_categories">
                     <?php $term_name = strtolower(single_term_title('',false)); ?>
                     <h2 class="widgettitle"><?php echo $term_name; ?></h2>
                     <?php if ( is_nav_menu( "menu-".$term_name) ): ?>
                           <?php wp_nav_menu( ['menu' => 'menu-'.$term_name ] ); ?>
                     <?php endif ?>
                  </aside>
               </div>
            </div>
            <div class=" content-padding-left main-content content-left col-sx-12 col-sm-12 col-md-9 col-lg-9">
               <div class="content-inner">
                  <div class="box-article">
                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
                        <div>
                              <?php
                                 $term_slug = get_queried_object()->slug;

                                $post = new WP_Query( array( 'posts_per_page' => 1, 'post_type' =>'que_hacemos', 'tax_query' => array( array('post' => 'areas','field' => 'slug', 'terms' => $term_slug ) )) );
                                 while ( $post->have_posts() )
                                 {
                                     $post->the_post();
                                 ?>

                                    <h2><?php the_title(); ?></h2>
                                    <div class="article ">
                                          <?php the_content() ?>
                                    </div>

                                 <?php
                                 }
                              ?>
                        </div>

                     </article>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php get_footer();
