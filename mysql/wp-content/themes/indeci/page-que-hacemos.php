<?php
get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="banner-featured" style="background-image: url('<?php echo $asset_path ?>/assets/img/banner-indeci.png')">
   <div class="container"> <h1><?php the_field('texto_frase') ?></h1> </div>
</div>
<div id="content" class="site-content initial-page" style="transform: none;">
   <div class="wrap-content wrapper" role="main" style="transform: none;">
      <div class="container" style="transform: none;">
         <div class="row single-main-content single-1" style="transform: none;">
           
            <div class=" content-padding-left main-content">
               <div class="content-inner">
                  <div class="box-article">
                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
                           
                        <!--<h1><?php the_title(); ?></h1>-->
                        <div class="col-xs-12">
                              <h1 class="title-subline"><?php the_title() ?></h1>
                              <div class="article">
                           
                              <?php the_content() ?>
                              </div>

                        </div>

                        <?php

                           $terms = get_field('area');

                           foreach ($terms as $term) { ?>
                              
                              <div class="col-md-6 col-sm-6">
                                 <div class="box">
                                    
                                    <a href="<?php echo $term['enlace']; ?>" class="ff-baloo">
                                       <img src="<?php echo $term['image_area']; ?>">
                                       <strong><span><?php echo $term['nombre_area']; ?></span></strong>
                                       <p style="margin: auto 2em;"><?php echo $term['descripcion_area']; ?></p>
                                    </a>
                                 </div>
                              </div>

                           <?php } ?>

                     </article>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>
<?php get_footer();
