<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
	<section class="content general alerts-archive">
		
		<div class="wrapper">

			<div id="archive-sidebarsss" class="sidebar sidebar-left col-sx-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar single-sidebar bg-azul" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                 <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 0px; left: 332.333px;">
                    <aside id="categories-8" class="widget widget_categories">
                       <h2 class="widgettitle">
                       	 ALERTA
                       </h2>
                       <div class="menu-menu-area-preparacion-container">
                       	<ul>
                       		<?php wp_list_categories( ['taxonomy' => 'informe', 'title_li' => '' ] ); ?>
                       	</ul>
                       </div>
                    </aside>
                 </div>
          	</div>

          	<div class=" content-padding-left main-content content-left col-sx-12 col-sm-12 col-md-9 col-lg-9">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php 
						$terms = get_the_terms( get_the_ID(), 'informe' );
						$term = array_shift( $terms );
					?>
					<div class="post-content" style="width: 100%;">
						<h1><a href="/alertas/">Alertas</a> / <?php echo $term->name ?></h1>
						<p>&nbsp;</p>
						<article>
							<span class="date"><?php the_date(); ?></span>
							<h1><?php the_title(); ?></h1>
							<div class="article">
								<?php the_content() ?>
								<p><a href="<?php the_field('archivo') ?>" download class="btn-download" target="_blank"> <i class="fas fa-download"></i>  DESCARGAR ARCHIVO</a></p>
							</div>

						</article>
					</div>
				<!-- post -->
				<?php endwhile; ?>
				<!-- post navigation -->
				<?php else: ?>
				<!-- no posts found -->
				<?php endif; ?>
			</div>

		</div>	
		
	</section>

<?php get_footer();
