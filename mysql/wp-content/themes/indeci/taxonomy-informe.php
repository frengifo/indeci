<?php
get_header(); 

$asset_path = get_template_directory_uri();
?>


<?php 
	$slug_post_type = get_post_type();
	$post_type_labels = get_post_type_labels( get_post_type_object( $slug_post_type ) );


  $banner_large = '';
  $slogan_banner = '';
  
  if ( isset($banner_information[ $slug_post_type ]) ) {

    $banner_large = get_field( 'banner_large'  , $banner_information[ $slug_post_type ] );
    $slogan_banner = get_field( 'slogan_banner'  , $banner_information[ $slug_post_type ] );

  }

  $banner_large = $banner_large != '' ?  $banner_large : $asset_path.'/assets/img/banner-indeci.png';
  $slogan_banner = $slogan_banner != '' ? $slogan_banner : $post->post_title;

?>


<div class="banner-featured <?php echo $slug_post_type; ?>" style="background-image: url('<?php echo $banner_large ?>')">
   <div class="container"> <h1>Alertas</h1> </div>
</div>
<div class="crumbs">
  <div class="wrapper">
        <div class="container">
    <?php //custom_breadcrumb(); ?>
      <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
        </div>  
  </div>
</div>
<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content" role="main" style="transform: none;">
      <div class="" style="transform: none;">
        <div class="wrapper">

           <div class=" single-main-content single-1" style="transform: none;">
              <div id="archive-sidebarsss" class="sidebar sidebar-left col-sx-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar single-sidebar bg-azul" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                 <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 0px; left: 332.333px;">
                    <aside id="categories-8" class="widget widget_categories">
                       <h2 class="widgettitle <?php echo $term_slug; ?>">
                       	 <?php echo $post_type_labels->singular_name;  ?>
                       </h2>
                       <div class="menu-menu-area-preparacion-container">
                       	<ul>
                       		<?php wp_list_categories( ['taxonomy' => 'informe', 'title_li' => '' ] ); ?>
                       	</ul>
                       </div>
                    </aside>
                 </div>
              </div>
              <div class=" content-padding-left main-content content-left col-sx-12 col-sm-12 col-md-9 col-lg-9">
                 <div class="content-inner">
                    
                 	<div class="post-content">
						<h1>Alertas</h1>
						<p>&nbsp;</p>
						<section class="list-news alerts-archive">
							<?php $current_term = get_queried_object(); ?>
							<?php query_posts( 
								[ 
									'post_type' => 'alertas',
									'posts_per_page' => 15, 
									'paged' => get_query_var( 'paged' ), 
									'tax_query' => [ 
										[
											'taxonomy' => 'informe', 
											'field' => 'slug', 
											'terms' => $current_term->slug 
										] 
									]

								] ); ?>

							<?php $styles = [ 'amarilla' => 'yellow', 'azul' => 'blue', 'naranja' => 'orange', 'verde' => 'green', 'roja' => 'red' ]; ?>

							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

								<?php $color = get_the_terms( $post, 'tipo' ); $color = isset($color[0]) ? $color[0]:null; ?>
								<?php $cls_bg = ( $color AND isset($styles[$color->slug]) ) ? $styles[$color->slug]:""; ?>
								<?php $icon = get_field('icono', 'tipo_' . $color->term_id); ?>
								<article class="<?php echo "bg-".$cls_bg; ?>">
									<a href="<?php the_permalink() ?>" class="img-icon">
										<img src="<?php echo $icon; ?>" />
									</a>
									<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
									<!-- <p><?php the_date(); ?></p> -->
									<a href="<?php the_field('archivo'); ?>" class="btn-download" target="_blank"> <i class="fas fa-download"></i> DESCARGAR</a>
									<a href="<?php the_permalink(); ?>" class="lnk-view">Ver más</a>
								</article>

							<!-- post -->
							<?php endwhile; ?>
							<!-- post navigation -->

							<ul class="pagination">
		                        <?php echo paginate_links(); ?>
		                    </ul>


							<?php else: ?>
							<!-- no posts found -->
							<?php endif; ?>

						</section>
					</div>

                 </div>
              </div>
           </div>
        </div>
      </div>
   </div>
</div>

<?php get_footer();
 