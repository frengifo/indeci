<?php
get_header(); 

$asset_path = get_template_directory_uri();
?>

<?php 
	$slug_post_type = get_post_type();
	$post_type_labels = get_post_type_labels( get_post_type_object( $slug_post_type ) );
?>

<div class="crumbs">
  <div class="wrapper">
    <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
  </div>
</div>
<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content wrapper" role="main" style="transform: none;">
      <div class="container" style="transform: none;">

         <div class="row single-main-content single-1" style="transform: none;">
            
            
            <div class="row">
	            <div class=" main-content col-sx-12 col-sm-12 ">
	               <div class="content-inner">
	                  <div class="box-article">
	                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
	                        <div class="box-title clearfix">
	                           <h2 class="title-left">Últimas Galerías</h2>
	                        </div>
							
						    <div class="row vid-lista box-blog archive-blog row large-vertical clearfix active">
	                        <?php 

	                        	$galerias_latest_six = get_posts([
		                          'post_type' => 'galerias',
		                          'numberposts' => 4
		                        ]);

	                         ?>

	                        	<div class="slider-last">

		                        	<?php foreach ( $galerias_latest_six as $post ) : setup_postdata( $post ); ?>
		                        	
		                        	<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
		                        	<?php 
	                                    $terms = get_the_terms( get_the_ID(), 'categoria' );
	                                    if ( !empty( $terms ) ){ $term = array_shift( $terms ); }
	                                 ?>

										<div class="col-xs-12 hidden-description hidden-meta first_gallery">
			                                <article class="post-item post-grid disss clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video">
			                                    <div class="article-tran hover-share-item">
			                                       <div class="post-image">
			                                          <a href="<?php echo the_permalink(); ?>" class="bgr-item"></a>
			                                          <a href="<?php echo the_permalink(); ?>" class="bg-img" style="background-image: url('<?php echo $featured_img_url; ?>')"> 	<?php the_post_thumbnail() ?> </a>
			                                          <span class="post-cat ">
			                                          	<a href="<?php echo get_term_link( $term, 'categoria' ) ?>" title="<?php echo $term->name ?>"> <?php echo $term->name ?> 	</a>
			                                          </span>
			                                       </div>
			                                       <div class="article-content   hidden-view hidden-comments">
			                                          <div class="entry-header clearfix">
			                                             <div class="entry-header-title">
			                                                <h3 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
			                                             </div>
			                                          </div>
			                                          <div class="entry-content"></div>
			                                       </div>
			                                    </div>
			                                </article>
		                              	</div>

									<?php endforeach; 
	                              	wp_reset_postdata(); ?>
	                            </div>

	                            <?php
									$args =  array(
									  'hide_empty' => false,
									  'orderby'    => 'name',
									  'order'      => 'ASC',
									);
									$terms = get_terms( 'categoria' , $args );

									$current_term_title = single_term_title("", false);
								?>

					         	<ul class="sub-menu-category">
									<li><a href="/galerias" >Todos:</a></li>
					         		<?php
										foreach( $terms as $term ) { ?>
											<?php $cls_active = $term->name == $current_term_title ? 'active':''; ?>
					                 		<li class="page_item page-item-942 <?php echo $cls_active; ?> ">
					                 			<a href="<?php echo get_term_link( $term ); ?>"> <?php echo $term->name; ?> </a>
					                 		</li>
									<?php } ?>
					         	</ul>


					         	<?php 

		                        	$galerias_full = get_posts([
			                          'post_type' => 'galerias',
			                          'numberposts' => -4
			                        ]);

		                        ?>

		                        <?php foreach ( $galerias_full as $post ) : setup_postdata( $post ); ?>
		                        	
		                        	<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
		                        	<?php 
	                                    $terms = get_the_terms( get_the_ID(), 'categoria' );
	                                    if ( !empty( $terms ) ){ $term = array_shift( $terms ); }
	                                ?>

									<div class="col-xs-12 col-sm-4 col-md-4 hidden-description hidden-meta lists-posts-gallery">
		                                <article class="post-item post-grid disss clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video">
			                                    <div class="article-tran hover-share-item">
			                                       <div class="post-image">
			                                          <a href="<?php echo the_permalink(); ?>" class="bgr-item"></a>
			                                          <a href="<?php echo the_permalink(); ?>" class="bg-img" style="background-image: url('<?php echo $featured_img_url; ?>')"> 	<?php the_post_thumbnail() ?> </a>
			                                          <span class="post-cat ">
			                                          	<a href="<?php echo get_term_link( $term, 'categoria' ) ?>" title="<?php echo $term->name ?>"> <?php echo $term->name ?> 	</a>
			                                          </span>
			                                       </div>
			                                       <div class="article-content   hidden-view hidden-comments">
			                                          <div class="entry-header clearfix">
			                                             <div class="entry-header-title">
			                                                <h3 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
			                                             </div>
			                                          </div>
			                                          <div class="entry-content"></div>
			                                       </div>
			                                    </div>
			                                </article>
		                          	</div>

		                        <?php endforeach; 
	                            wp_reset_postdata(); ?>

						    </div>



	                     </article>
	                  </div>
	               </div>
	            </div>
	        </div>
         </div>
      </div>
   </div>
</div>

<?php get_footer();
