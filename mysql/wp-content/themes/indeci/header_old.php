<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php $asset_path = get_template_directory_uri(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/normalize.css">
<link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/plugins.css">
<link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/main.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<script type="text/javascript" src="<?php echo $asset_path; ?>/assets/js/jquery-3.3.1.min.js"></script>

<?php wp_head(); ?>
<style type="text/css">
	#wpadminbar{ display: none !important; }
	html{    margin-top: 0 !important; }
</style>
<!-- FACEBOOK -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.0&appId=218706935288983&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<style type="text/css">
	.banners img{
		width: 100%;
	}
</style>
</head>

<body <?php body_class(); ?>>
<div off-canvas="id-1 right overlay" class="menu-mobile-container">
	<a href="javascript:;" class="menu-close" data-close="id-1"><i class="fas fa-times"></i></a>

	<nav>
		<ul>
			<li><a href="/institucion/acerca-de-indeci/">¿Quiénes Somos?</a></li>
			<li><a href="/que_hacemos/nosotros-preparacion/" >¿Qué Hacemos?</a></li>
			<li><a href="/institucion/regiones/" title="Direcciones Desconcentradas">Regiones</a></li>
			<li><a href="/ayuda_internacional/funciones-de-la-ogcai/" title="Centro de Operaciones Emergencia Nacional">Cooperación Internacional</a></li>
			<!-- DEFOCA -->
			<li><a href="/capacitacion" title="Capacitacion">Capacitación</a></li>
			<li><a href="/coen/nosotros/" title="COEN">COEN</a></li>
			<li><a href="/fondes/que-es-el-fondes/" title="FONDES">FONDES</a></li>
		</ul>
		<a href="/" target="_blank" class="ministerio text-center"><img src="<?php echo $asset_path; ?>/assets/img/ministerio-de-defensa.png" alt="Ministerio de Defensa de Perú"></a>
	</nav>
</div>

<div off-canvas="id-2 top overlay" class="menu-top-mobile-container">
	<a href="javascript:;" class="menu-close" data-close="id-2"><i class="fas fa-times"></i></a>

	<!--<a href="#">Web Accesible</a>-->
	<a href="/mapa-de-sitio/">Mapa Sitio</a>
	<a href="http://sinpad.indeci.gob.pe/LibroReclamaciones/Views/Inicio.aspx" target="_blank">Libro de reclamaciones</a>
	<a href="/contactenos/">Contáctenos</a>
	<a href="#">Intranet</a>
	<a href="https://mail.indeci.gob.pe" target="_blank">Webmail</a>
	<a href="/trabaja-con-nosotros/">Trabaja con Nosotros</a>
</div>
<div class="fixed-menu" style="position: fixed;top: 15%; right: 2em;z-index: 9999999;">
	<a href="https://web.facebook.com/indeci/" target="_blank"><img src="http://indeci.apprende.com.pe/wp-content/uploads/2018/07/MENU-FLOTANTE-RRSS.png"></a>
</div>
<div class="site" canvas="container">
	

	<header class="site-header" >
			
			<div class="top">
				<a href="javascript:;" class="menu-top-mobile" data-open="id-2"><i class="fas fa-bars"></i></a>
				<div class="wrapper">
					<!--<a href="#">Web Accesible</a>-->
					<a href="/mapa-de-sitio/">Mapa Sitio</a>
					<a href="http://sinpad.indeci.gob.pe/LibroReclamaciones/Views/Inicio.aspx" target="_blank">Libro de reclamaciones</a>
					<a href="/contactenos/">Contáctenos</a>
					<a href="#">Intranet</a>
					<a href="https://mail.indeci.gob.pe" target="_blank">Webmail</a>
					<a href="/trabaja-con-nosotros/">Trabaja con Nosotros</a>
					<div class="search-box">
						<form class="fas">
							<input type="text" name="search" placeholder="Buscar...">
						</form>
					</div>
				</div>
			</div>
			<div class="wrapper">
				<a href="/" class="logo"> <img src="<?php echo $asset_path; ?>/assets/img/logo-indeci.png"> </a>
				<nav>
					<ul>
						
							
							<li><a href="/institucion/acerca-de-indeci/">¿Quiénes Somos?</a></li>
							<li><a href="/que_hacemos/nosotros-preparacion/" >¿Qué Hacemos?</a></li>
							<li><a href="/institucion/regiones/" title="Direcciones Desconcentradas">Regiones</a></li>
							<li><a href="/ayuda_internacional/funciones-de-la-ogcai/" title="Centro de Operaciones Emergencia Nacional">Cooperación Internacional</a></li>
							<!-- DEFOCA -->
							<li><a href="/capacitacion" title="Capacitacion">Capacitación</a></li>
							<li><a href="/coen/nosotros/" title="COEN">COEN</a></li>
							<li><a href="/fondes/que-es-el-fondes/" title="FONDES">FONDES</a></li>

						
					</ul>
				</nav>
				<a href="http://www.transparencia.gob.pe/enlaces/pte_transparencia_enlaces.aspx?id_entidad=128&id_tema=1&ver=D#.W0eHNdVKjtR" target="_blank" class="ministerio"><img src="<?php echo $asset_path; ?>/assets/img/ministerio-de-defensa.png" alt="Ministerio de Defensa de Perú"> <i class="fas fa-search"></i> Transparencia</a>

				<a href="javascript:;" class="menu-mobile" data-open="id-1"><i class="fas fa-bars"></i></a>

			</div>
	</header><!-- #masthead -->
	<?php $styles = [ 'amarilla' => 'yellow', 'azul' => 'blue', 'naranja' => 'orange', 'verde' => 'green', 'roja' => 'red' ]; ?>
	<section class="site-content-contain">
		<div class="wrapper">

			<div class="alerts">
			<?php 
				$alertas = get_posts([
				  'post_type' => 'alertas',
				  'post_status' => 'publish',
				  'numberposts' => 5
				]);
			 ?>

			<?php foreach ( $alertas as $post ) : setup_postdata( $post ); ?>

				<?php 
					$terms = get_the_terms( $post->ID, 'tipo' );
					$term = array_shift( $terms );
					$icono = get_field('icono', $term);
				?>
				<div>
					<article class="bg-<?php echo $styles[$term->slug];  ?>" style="<?php echo !empty($icono) ? "background-image: url('".$icono."');":""; ?>">
						<h2>
							<?php $sus = strlen(get_the_title()) > 180 ? "...":""; ?>
							<a href="<?php the_permalink(); ?>"><?php echo mb_strimwidth(get_the_title(), 0, 180, $sus); ?></a>
						</h2>
					</article>
				</div>

			<?php endforeach; 
			wp_reset_postdata();?>
<!-- 
				<div>
					<article class="bg-green">
						<h2>
							<a href="#">ALERTA AMARILLA 05/03/2018 SAN MARTIN - Lamas - Tabaloso (San Miguel del Río de Mayo): Inundación (Reporte Complementario 01)</a>
						</h2>
					</article>
				</div>
				<div>
					<article class="bg-orange">
						<h2>
							<a href="#">ALERTA AMARILLA 05/03/2018 SAN MARTIN - Lamas - Tabaloso (San Miguel del Río de Mayo): Inundación (Reporte Complementario 01)</a>
						</h2>
					</article>
				</div>
				<div>
					<article class="bg-red">
						<h2>
							<a href="#">ALERTA AMARILLA 05/03/2018 SAN MARTIN - Lamas - Tabaloso (San Miguel del Río de Mayo): Inundación (Reporte Complementario 01)</a>
						</h2>
					</article>
				</div>
				<div>
					<article class="bg-orange">
						<h2>
							<a href="#">ALERTA AMARILLA 05/03/2018 SAN MARTIN - Lamas - Tabaloso (San Miguel del Río de Mayo): Inundación (Reporte Complementario 01)</a>
						</h2>
					</article>
				</div>
				<div>
					<article class="bg-green">
						<h2>
							<a href="#">ALERTA AMARILLA 05/03/2018 SAN MARTIN - Lamas - Tabaloso (San Miguel del Río de Mayo): Inundación (Reporte Complementario 01)</a>
						</h2>
					</article>
				</div> -->
			</div>
		</div>