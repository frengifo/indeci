<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); $banner_large = $asset_path.'/assets/img/banner-indeci.png'; ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class="banner-featured <?php echo $slug_post_type; ?>" style="background-image: url('<?php echo $banner_large ?>')">
	   <div class="container"> <h1>Recomendaciones</h1> </div>
	</div>
	<div class="crumbs">
	  <div class="wrapper">
	    <a href="/recomendaciones"> <i class="fas fa-home"></i>  Todas las Recomendaciones</a>
	  </div>
	</div>
	<section class="single-causes-area section">
        <div class="container">
            <div class="row">
            	
                <div class="col-xs-12">
                	<h1><?php the_title() ?></h1>
                	<?php if (get_field('galeria')): ?>
                		<div class="gallery-slider first_gallery">
		                	<?php foreach (get_field('galeria') as $k => $v): ?>
		                		<div>
		                			
									
										<img src="<?php echo $v['url']; ?>" width="100%" >

		                		</div>
							<?php endforeach ?>
                	<?php else: ?>
                		
	                    <div class="causes-img">
	                        <figure>
	                            <img src="<?php the_post_thumbnail_url( 'full' ) ?>" alt="" width="100%">
	                        </figure>
	                    </div>
                	<?php endif ?>
                </div>
               
                <div class="clear"></div>
                <div class="col-xs-12">
                	<p>&nbsp;</p>
                	<?php the_content() ?>
                </div>
            </div>
            <div class="row mt50">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="disce mr-t70 mr-b70">
						
						<p>&nbsp;</p>
						<h3>Archivos</h3>
						<?php foreach (get_field('archivos') as $k => $v): ?>
							
							<p><a href="<?php echo $v['archivo'] ?>" style="float:left;font-size: 14px;display: inline-block;max-width: initial;" class="btn-download" target="_blank"> <i class="fas fa-download"></i>  <?php echo $v['nombre'] ?></a></p>

						<?php endforeach ?>
						<p>&nbsp;</p>
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="row">
            	
            </div>
            
        </div>
    </section>
		
					

	<!-- post -->
	<?php endwhile; ?>
	<!-- post navigation -->
	<?php else: ?>
	<!-- no posts found -->
	<?php endif; ?>

<?php get_footer();
