<?php 
	
	$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
	
	$current_term = get_queried_object();
	if ( $current_term->taxonomy ) {
		
		$tax_slug = $current_term->taxonomy;

	}else{
		
		$tax_slug = isset($tax_slug) ? $tax_slug:'categoria';

	}
	
	

    $terms = get_the_terms( get_the_ID(), $tax_slug );
    if ( !empty( $terms ) ){ $term = array_shift( $terms ); }

    $cls_cols = !isset($cls_cols) ? 'col-xs-12 col-sm-4 col-md-4': $cls_cols;
    // $cls_cols = '';

?>

<div class=" <?php echo ( ( isset($cols) AND $cols ) OR !isset($cols) ) ? $cls_cols:'';  ?> hidden-description hidden-meta">
    <article class="post-item post-grid disss clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video">
        <div class="article-tran hover-share-item">
           <div class="post-image">
              <a href="<?php echo the_permalink(); ?>" class="bgr-item"></a>
              <a href="<?php echo the_permalink(); ?>" class="bg-img" style="background-image: url('<?php echo $featured_img_url; ?>')"> 	<?php the_post_thumbnail() ?> </a>
              <span class="post-cat ">
              	<a href="<?php echo get_term_link( $term, $tax_slug ) ?>" title="<?php echo $term->name ?>"> <?php echo $term->name ?> 	</a>
              </span>
           </div>
           <div class="article-content   hidden-view hidden-comments">
              <div class="entry-header clearfix">
                 <div class="entry-header-title">
                    <h3 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                 </div>
              </div>
              <div class="entry-content"></div>
           </div>
        </div>
    </article>
</div>