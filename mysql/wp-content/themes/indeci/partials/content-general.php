     <?php 
   // QUIENES SOMOS
   if( have_rows('media') ):
       while ( have_rows('media') ) : the_row();
          if( get_row_layout() == 'video_presentacion' ):
            ?>
               <div class="video-indeci">
                  <?php the_sub_field('video'); ?>
               </div>
            <?php 
            ?>
            <div class="media-desc">
              <?php the_sub_field('descripcion_video'); ?>
            </div>
            <div class="box-vermas mt-1 mb-3 text-right">
              <a href="<?php the_sub_field('link_mas_informacion'); ?>" target="_blank" class="link-vermas">Ver más</a>
            </div>
            <?php 
          endif;
       endwhile;
   endif;
   ?>

   <div class="content-text">
      <?php the_content() ?>
   </div>
   <?php 
   // ACCIONES 
  if( have_rows('media') ):
       while ( have_rows('media') ) : the_row();
          if( get_row_layout() == 'acciones_area' ):

            $acciones = get_sub_field('accion');

            ?>

              <?php foreach ($acciones as $accion): ?>
                
                 <div class="row mt-2">
                    <div class="col-md-6 mt-1">
                        <div class="acciones-slider">
                            <?php foreach( $accion['fotos'] as $image ): ?>
                                <div class="item">
                                   <img src="<?php echo $image['url'] ?>">
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-md-6 mt-1">
                       <h4><?php echo $accion['nombre_accion']; ?></h4>
                       <p><?php echo $accion['descripcion_accion']; ?></p>
                       <a href="<?php echo $accion['enlace_accion']; ?>" class="link-azul-wide">Ver más</a>
                    </div>
                 </div>

              <?php endforeach ?>

            <?php 
          endif;
       endwhile;
   endif;
   ?>

   <?php 
   // GALERIA IMAGENES
   if( have_rows('media') ):
       while ( have_rows('media') ) : the_row();
           if( get_row_layout() == 'galeria_de_fotos' ):
            ?>
               <div class="mt-1">
                  <?php 
                     $items = get_sub_field('gallery');
                     if( $items ): ?>
                         <div class="acciones-slider gallery-slider">
                             <?php foreach( $items as $item ): ?>
                                 <div class="item">
                                    <div class="video-indeci">
                                      <img src="<?php echo $item['image']; ?>" alt="">
                                    </div>
                                    <div class="item-resena">
                                       <p><strong><?php echo $item['titulo']; ?></strong></p>
                                       <span><?php echo $item['descripcion']; ?></span>
                                    </div>
                                 </div>
                             <?php endforeach; ?>
                         </div>
                     <?php endif;
                  ?>
               </div>
            <?php 
           endif;
       endwhile;
   endif;

   // GALERIA FOTO GALERIAS
   if( have_rows('media') ):
       while ( have_rows('media') ) : the_row();
           if( get_row_layout() == 'imagenes_fotogaleria' ):
            ?>
               <div class="mt-1">
                  <?php 
                     $items = get_sub_field('gallery');
                     if( $items ): ?>
                         <div class="acciones-slider gallery-slider">
                             <?php foreach( $items as $item ): ?>
                                 <div class="item">
                                    <div class="video-indeci">
                                      <img src="<?php echo $item['image']; ?>" alt="">
                                    </div>
                                    <div class="item-resena">
                                       <p><strong><?php echo $item['titulo']; ?></strong></p>
                                       <span><?php echo $item['descripcion']; ?></span>
                                    </div>
                                 </div>
                             <?php endforeach; ?>
                         </div>
                     <?php endif;
                  ?>
               </div>
            <?php 
           endif;
       endwhile;
   endif;

  // Mas Galerias IMG
  if ( has_post_format( 'gallery' )) { ?>
    <?php 
     $query = new WP_Query( array(
        'post_type' => 'que_hacemos',
        'posts_per_page' => '8',
        'orderby' => 'date',
        'order' => 'desc',
        'tax_query' => array(
          array(
            'taxonomy' => 'post_format',
            'field'    => 'slug',
            'terms' => array( 'post-format-gallery' )
          )),
    ));

    if ( $query->have_posts() ) { ?>
      <div class="mt-4">
        <h3>GALERÍAS RECIENTES</h3>
      </div>
      <div class="row mt-2 acc-directos-list">
        <?php 
        while ( $query->have_posts() ) : $query->the_post();  ?>
            <div class="col-xs-6 col-sm-2 col-md-4">
              <a href="<?php the_permalink(); ?>">
                 <?php the_post_thumbnail(); ?>
              </a>
              <p class="mt-1"><?php the_title(); ?></p>
              <a href="<?php the_permalink(); ?>" class="link-azul-wide mt-1">Ver</a>
            </div>
      <?php 
        endwhile; ?>
      </div>
      <?php 
    }

  }

  // Mas Galerias VIDEO
  if ( has_post_format( 'video' )) { ?>
    <?php 
     $query = new WP_Query( array(
        'post_type' => 'que_hacemos',
        'posts_per_page' => '8',
        'orderby' => 'date',
        'order' => 'desc',
        'tax_query' => array(
          array(
            'taxonomy' => 'post_format',
            'field'    => 'slug',
            'terms' => array( 'post-format-video' )
          )),
    ));

    if ( $query->have_posts() ) { ?>
      <div class="mt-4">
        <h3>VIDEOS RECIENTES</h3>
      </div>
      <div class="row mt-2 acc-directos-list">
        <?php 
        while ( $query->have_posts() ) : $query->the_post();  ?>
            <div class="col-xs-6 col-sm-2 col-md-4">
              <a href="<?php the_permalink(); ?>">
                 <?php the_post_thumbnail(); ?>
              </a>
              <p class="mt-1"><?php the_title(); ?></p>
              <a href="<?php the_permalink(); ?>" class="link-azul-wide mt-1">Ver</a>
            </div>
      <?php 
        endwhile; ?>
      </div>
      <?php 
    }

  }
  ?>

   <?php 
   // ACCESOS DIRECTOS
   if( have_rows('media') ):
       while ( have_rows('media') ) : the_row();
           if( get_row_layout() == 'enlaces' ):
            ?>
               <div class="row mt-1 acc-directos-list">
                  <?php 
                     $items = get_sub_field('accesos_directos');
                     if( $items ): ?>
                       <?php foreach( $items as $item ): ?>
                           <div class="col-sm-3 col-md-4 mb-2">
                              <a href="<?php echo $item['link_enlace']; ?>" target="_blank">
                                 <img src="<?php echo $item['imagen_enlace']; ?>" alt="">
                              </a>
                              <p class="mt-1"><?php echo $item['titulo_enlace']; ?></p>
                              <a href="<?php echo $item['link_enlace']; ?>" class="link-azul-wide mt-1" target="_blank">Ver</a>
                           </div>
                       <?php endforeach; ?>
                     <?php endif;
                  ?>
               </div>
            <?php 
           endif;
       endwhile;
   endif;
   ?>

   <?php 
   // MAS INFORMACION
   if( have_rows('media') ):
       while ( have_rows('media') ) : the_row();
           if( get_row_layout() == 'mas_informacion' ):
            ?>
               <!-- <div class="row mt-2 masinfo-list"> -->
                <div class="row mt-1 acc-directos-list">
                  <?php 
                     $items = get_sub_field('archivos_informacion');
                     if( $items ): ?>
                       <?php foreach( $items as $item ): ?>
                           <div class="col-sm-3 col-md-4 mb-2">
                              <a href="<?php echo $item['archivo_info']; ?>" target="_blank">
                                 <img src="<?php echo $item['imagen_info']; ?>" alt="">
                              </a>
                              <p class="mt-1"><?php echo $item['titulo_info']; ?></p>
                              <a href="<?php echo $item['archivo_info']['url']; ?>" target="_blank" class="link-azul-wide mt-1">Ver</a>
                           </div>
                       <?php endforeach; ?>
                     <?php endif;
                  ?>
               </div>
            <?php 
           endif;
       endwhile;
   endif;
   ?>