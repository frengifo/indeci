    <?php $asset_path = get_template_directory_uri(); ?>

    <section class="stats">
    	<div class="wrapper">
    		<?php $numeros = get_field('indeci_numeros', 2); ?>
            <?php if($numeros): ?>
            	<?php foreach ($numeros as $k => $v): ?>
    					<span>
    						<b><?php echo $v['numero']; ?></b>
    						<small><?php echo $v['titulo_numero']; ?></small>
    					</span>
            	<?php endforeach ?>
    		<?php endif; ?>
    	</div>
    </section>
    		
    <footer id="na-footer">
        <div class="na-footer-main">
            <div class="footer-top">
                <div class="container">
                    <div class="container-inner">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div id="contact_info-3" class="widget first widget_contact_info">
                                    <div class="contact-inner clearfix">
                                        <img class="about-image" src="http://indeci2.apprende.com.pe/wp-content/themes/indeci/assets/img/logo-indeci.png" alt="img">
                                        <p class="description">
                                            "Un país preparado y resiliente ante emergencias y desastres."
                                        </p>
                                        <ul class="contact-info">
                                            <li>
                                                <b>Dirección: </b><br>
                                                <span>Calle Ricardo Angulo 694, San Isidro 15036</span></li>
                                            <li>
                                                <b>Teléfonos: </b>
                                                <span>999 999 999</span></li>
                                            <li>
                                                <b>Email: </b>
                                                <a href="mailto:contacto@indeci.gob.pe"><span>contacto@indeci.gob.pe</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="bitther_social-3" class="widget first bitther_social">
                                    <div class="bitther-social-icon clearfix">
                                    	<a href="#" target="_blank" title="ion-social-facebook" class="ion-social-facebook"><span class="ti-facebook"></span></a>
                                    	<a href="#" target="_blank" title="ion-social-twitter" class="ion-social-twitter"><span class="ti-twitter-alt"></span></a><a href="#" target="_blank" title="ion-social-instagram" class="ion-social-instagram"><span class="ti-instagram"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div id="bitther_post-10" class="widget first widget_bitther_post">
                                    <aside class="widget_tabs_post">
                                        <h2 class="widgettitle">QUIENES SOMOS</h2>
                                        <ul>
                                        	<li><a href="">Link #1</a></li>
                                        	<li><a href="">Link #2</a></li>
                                        	<li><a href="">Link #3</a></li>
                                            <li><a href="">Link #1</a></li>
                                            <li><a href="">Link #2</a></li>
                                            <li><a href="">Link #3</a></li>
                                        </ul>
                                    </aside>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div id="twitter-4" class="widget first widget_twitter">
                                    <h2 class="widgettitle">QUE HACEMOS</h2>
                                    <ul>
                                    	<li><a href="">Link #1</a></li>
                                    	<li><a href="">Link #2</a></li>
                                    	<li><a href="">Link #3</a></li>
                                        <li><a href="">Link #1</a></li>
                                            <li><a href="">Link #2</a></li>
                                            <li><a href="">Link #3</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="coppy-right">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <small><span>Copyrights © 2018 INDECI. All Rights Reserved.</span></small>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    	</section><!-- .site-content-contain -->
    </div><!-- #page -->
    <!-- <script type="text/javascript" src="<?php echo $asset_path; ?>/assets/js/plugins.js"></script> -->
    <?php //wp_footer(); ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo $asset_path; ?>/assets/js/main.js"></script>
    <script>
        $(function() {
            $('.acciones-slider').slick();
            $('.box-article a').each(function( ) {
                texto = $(this).text();
                if(texto == "Ver más" || texto == "ver más" || texto == "VER MÁS" || texto == "VER" || texto == "ver"){
                    $(this).addClass('link-vermas');
                }
            });
        });
    </script>
    </body>
    </html>
