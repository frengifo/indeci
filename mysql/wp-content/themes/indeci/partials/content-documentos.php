<?php 
$asset_path = get_template_directory_uri();
$arr_iconos = array('pdf' => 'icon-pdf.png', 'doc'=>'icon-doc.png', 'docx'=>'icon-doc.png');

?>
						<?php if (get_field('informacion')): ?>
							
							<?php $informacion = get_field('informacion'); ?>
							<?php foreach ($informacion as $key => $value): ?>
								
								<div class="table-block blue-table" style="margin-left: 0;">
									<table style="text-align: left;">
										<thead>
											<tr>
												<th colspan="3"><?php echo $value['titulo']; ?></th>
											</tr>
										</thead>
										<tbody>
											
											<?php foreach ($value['documentos'] as $i => $row): ?>
												
												<tr>
													<td><?php echo $row['nombre']; ?></td>
													<td><?php echo $row['descripcion']; ?>
														<?php $icono = wp_check_filetype( $row['archivo'])['ext']; ?>
													</td>
													<td width="80"><a href="<?php echo $row['archivo']; ?>" target="_blank">
														<img src="<?php echo $asset_path; ?>/assets/img/<?php echo $arr_iconos[$icono] ?>" width="38">
													</a></td>
												</tr>

											<?php endforeach ?>

										</tbody>
									</table>
								</div>

							<?php endforeach ?>

						<?php endif ?>