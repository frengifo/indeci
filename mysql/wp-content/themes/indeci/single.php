<?php
get_header(); 

$asset_path = get_template_directory_uri();
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php 
	$slug_post_type = get_post_type();
	$post_type_labels = get_post_type_labels( get_post_type_object( $slug_post_type ) );

  $page_quehacemos = 685; 

  $banner_information = [
      'preparacion' => $page_quehacemos,
      'respuesta' => $page_quehacemos,
      'rehabilitacion' => $page_quehacemos,
      'defocaph' => $page_quehacemos,
      'direccion-politicas-y-planes' => $page_quehacemos,
      'ayuda_internacional' => 730,
      'capacitacion' => 732,
      'coen' => 734,
      'fondes' => 735,
  ];

  $banner_large = '';
  $slogan_banner = '';
  
  if ( isset($banner_information[ $slug_post_type ]) ) {

    $banner_large = get_field( 'banner_large'  , $banner_information[ $slug_post_type ] );
    $slogan_banner = get_field( 'slogan_banner'  , $banner_information[ $slug_post_type ] );

  }

  $banner_large = $banner_large != '' ?  $banner_large : $asset_path.'/assets/img/banner-indeci.png';
  $slogan_banner = $slogan_banner != '' ? $slogan_banner : $post->post_title;

?>
<div class="banner-featured <?php echo $slug_post_type; ?>" style="background-image: url('<?php echo $banner_large ?>')">
   <div class="container"> <h1><?php echo $slogan_banner; ?></h1> </div>
</div>
<div class="crumbs">
  <div class="wrapper">
        <div class="container">
    <?php //custom_breadcrumb(); ?>
      <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
        </div>  
  </div>
</div>
<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content" role="main" style="transform: none;">
      <div class="" style="transform: none;">
        <div class="wrapper">

           <div class=" single-main-content single-1" style="transform: none;">
              <div id="archive-sidebarsss" class="sidebar sidebar-left col-sx-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar single-sidebar bg-azul" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                 <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 0px; left: 332.333px;">
                    <aside id="categories-8" class="widget widget_categories">
                       <h2 class="widgettitle <?php echo $term_slug; ?>">
                       	 <?php echo $post_type_labels->singular_name;  ?>
                       </h2>
                       <div class="menu-menu-area-preparacion-container">
                       	<ul>
                       		<?php wp_list_pages( ['post_type' => $slug_post_type, 'title_li' => '', 'walker' => new Nav_Page_Custom_Walker() ] ); ?>
                       	</ul>
                       </div>
                    </aside>
                 </div>
              </div>
              <div class=" content-padding-left main-content content-left col-sx-12 col-sm-12 col-md-9 col-lg-9">
                 <div class="content-inner">
                    <div class="box-article">
                       <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
                          <h1 class=""><?php the_title(); ?></h1>
                          <div class="article">
                            <?php 
                              get_template_part( 'partials/content', 'general' );
                              
                              if ($slug_post_type == 'fondes'):
                                get_template_part( 'partials/content', 'documentos' );
                              endif;
                            ?>
                          </div>
                       </article>

                    </div>
                 </div>
              </div>
           </div>
        </div>
      </div>
   </div>
</div>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>
<?php get_footer();
