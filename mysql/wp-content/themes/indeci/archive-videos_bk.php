<?php
get_header(); 

$asset_path = get_template_directory_uri();
?>

<?php 
	$slug_post_type = get_post_type();
	$post_type_labels = get_post_type_labels( get_post_type_object( $slug_post_type ) );
?>

<div class="crumbs">
  <div class="container">
    <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
  </div>
</div>
<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content" role="main" style="transform: none;">
      <div class="container" style="transform: none;">

         <div class="row single-main-content single-1" style="transform: none;">
            <div id="archive-sidebarsss" class="sidebar sidebar-left col-sx-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar single-sidebar bg-azul" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
               <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 0px; left: 332.333px;">
                  <aside id="categories-8" class="widget widget_categories">
                     <h2 class="widgettitle <?php echo $term_slug; ?>">
                     	 <?php //echo $post_type_labels->singular_name;  ?>
                     	 Categorías
                     </h2>
                     <div class="menu-menu-area-preparacion-container">
                 		<?php 
                 		 	$taxonomy = 'categoria_video';
							$args =  array(
							  'hide_empty' => false,
							  'orderby'    => 'name',
							  'order'      => 'ASC'
							);
							$terms = get_terms( $taxonomy , $args );
						?>
                     	<ul>
                     		<?php
								foreach( $terms as $term ) { ?>
		                     		<li class="page_item page-item-942">
		                     			<a href="<?php echo get_term_link( $term ); ?>">
		                     				<?php echo $term->name.' ('.$term->count.')'; ?>
		                     			</a>
		                     		</li>
							<?php } ?>
                     	</ul>
                     </div>
                  </aside>
               </div>
            </div>
            <div class=" content-padding-left main-content content-left col-sx-12 col-sm-12 col-md-9 col-lg-9">
               <div class="content-inner">
                  <div class="box-article">
                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
                        <h1><?php echo $post_type_labels->singular_name;  ?></h1>
						<!-- <div class="box-video video-horizontal clearfix">
						    <div class="col-md-12 slider-video">
						        <div class="post-item video-1448 1 active">
						            <div class="article-video"></div>
						            <div class="article-image">
						                <img class="lazy" src="" alt="post-image" style="display: inline;">
						                <span class="bgr-item"></span>
						                <span class="btn-play" data-id="1448">
										<i class="fa fa-play" aria-hidden="true"></i>
										<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i>
										</span></div>
						            <div class="article-content ">
						                <div class="entry-header clearfix">
						                     <h3 class="entry-title"><a href="">Japan’s Banking Giant SBI Delays Launch</a></h3>
						                </div>
						                <div class="article-meta clearfix">
						                    <span class="byline author-title"><span class="ti-minus"></span><strong>Categpria</strong>
						                    <span class="posted-on"><i class="icon ti-calendar" aria-hidden="true"></i>
							                    <a href="" rel="bookmark">
							                        <time class="entry-date published">April 15, 2018</time>
							                        <time class="updated">July 2, 2018</time>
							                    </a>
						                    </span>
						                </div>
						            </div>
						        </div>
						    </div>					    
						</div> -->

                        <?php 
      //                   $terms = array('link');
						// $tax = array(16)
						// $args = array( 
						//        'post_type' => 'post',
						//        'tax_query' => array(
						//         'relation' => 'OR',
						//                 array(
						//                     'taxonomy' => 'post_format',
						//                     'field'    => 'slug',
						//                     'terms'    => $terms,
						//                 ),
						//                 array(
						//                     'taxonomy' => 'space',
						//                     'field'    => 'id',
						//                     'tag__in'    => $tax,
						//                 ),
						//         ),
						//        'posts_per_page' => 2
						//     );


						// $the_query = new WP_Query( $args );
                        ?>
					    <div class="row vid-lista row-flex">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        	<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
						           // the_post_thumbnail('thumbnail');
                        	?>
					    	<div class="col-sm-4 mb-2">
							    <div class="post-image btn-play">
							        <a class="post-image-arg" href="<?php the_permalink(); ?>">
          								<img src="<?php echo esc_url($featured_img_url); ?>" />
							            <i class="fa fa-play" aria-hidden="true"></i>
							        </a>
							    </div>
							    <div class="mt-xs">
							    	<span class="cate-tag"><a href="" title="">Categoria</a></span>
							        <p class="mt-xs"><a href="<?php the_permalink(); ?>"><strong><?php the_title(); ?></strong></a></p>
							    </div>
							</div>
							<?php endwhile; ?>
							<?php endif; ?>
					    </div>



                     </article>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php get_footer();
