<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>Ayuda Internacional</h3>
					<!-- <ul>
						<li style="width: 97.5%"><a href="/alertas/" class="active">Alertas</a></li>
						<li><a href="#">COEN</a></li>
						<li><a href="#">#INDECITeRecomienda</a></li>
					</ul> -->
					<?php wp_nav_menu( ['menu' => 'menu-fondes'] ) ?>
					
				</nav>

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			<div class="post-content">

				<article>
					<h1><?php the_title(); ?></h1>
					<div class="article">
						<?php the_content() ?>
						
						<?php if (get_field('informacion')): ?>
							
							<?php $informacion = get_field('informacion'); ?>
							<?php foreach ($informacion as $key => $value): ?>
								
								<div class="table-block blue-table" style="margin-left: 0;">
									<table style="text-align: left;">
										<thead>
											<tr>
												<th colspan="3"><?php echo $value['titulo']; ?></th>
											</tr>
										</thead>
										<tbody>
											
											<?php foreach ($value['documentos'] as $i => $row): ?>
												
												<tr>
													<td><?php echo $row['nombre']; ?></td>
													<td><?php echo $row['descripcion']; ?></td>
													<td width="80"><a href="<?php echo $row['archivo']; ?>" target="_blank"><img src="<?php echo $asset_path; ?>/assets/img/icon-pdf.png"></a></td>
												</tr>

											<?php endforeach ?>

										</tbody>
									</table>
								</div>

							<?php endforeach ?>

						<?php endif ?>


					</div>

				</article>
			</div>
		<!-- post -->
		<?php endwhile; ?>
		<!-- post navigation -->
		<?php else: ?>
		<!-- no posts found -->
		<?php endif; ?>
		</div>	
		
	</section>

<?php get_footer();
