<?php
get_header(); 

$asset_path = get_template_directory_uri();
?>

<?php 
	$slug_post_type = get_post_type();
	$post_type_labels = get_post_type_labels( get_post_type_object( $slug_post_type ) );
?>

<div class="crumbs">
  <div class="wrapper">
    <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
  </div>
</div>
<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content wrapper" role="main" style="transform: none;">
      <div class="container" style="transform: none;">

         <div class=" row single-main-content single-1" style="transform: none;">
            
            <div class="">
	            <div class=" main-content ">
	               <div class="content-inner">
	                  <div class="box-article">
	                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
	                        <div class="box-title clearfix">
	                           <h2 class="title-left">Últimas Noticias</h2>
	                        </div>
							
						    <div class=" vid-lista box-blog archive-blog  large-vertical clearfix active">
		                        
		                        <?php 

		                        	$galerias_latest_six = get_posts([
			                          // 'post_type' => 'post',
			                          'numberposts' => 4
			                        ]);
			                        
		                        	$tax_slug = 'category';
		                        	$section_slug = 'noticias';
		                        	$cols = false;

		                        ?>

	                        	<div class="slider-last-videos first_gallery">

		                        	 <?php foreach ( $galerias_latest_six as $post ) : setup_postdata( $post ); ?>
		                        		
		                        		<?php include(locate_template('partials/content-post-list-item.php')); ?>

									<?php endforeach; 
	                              	wp_reset_postdata(); ?> 
	                              	
	                            </div>

	                            <?php include(locate_template('partials/content-category-list.php')); ?>

					         	<?php 

		                        	$galerias_full = get_posts([
			                          // 'post_type' => 'galerias',
			                          'numberposts' => -1,
			                          'category' => $current_term->term_id,
			                        ]);
		                        	$cols = true;

		                        	$row_count = 1;
		                        ?>

			                    <div class="lists-posts-gallery ">
			                    	<?php foreach ( $galerias_full as $post ) : setup_postdata( $post ); ?>
			                    	
				                        
				                        	
				                       		<?php include(locate_template('partials/content-post-list-item.php')); ?>

				                        
				                        <?php if ( $row_count % 3 == 0): ?>
				                        	<div class="clear"></div>
				                        <?php endif ?>

				                        <?php $row_count++; ?>
			                    
				                    <?php endforeach; 
			                        wp_reset_postdata(); ?>
	                        	</div>

						    </div>



	                     </article>
	                  </div>
	               </div>
	            </div>
	        </div>
         </div>
      </div>
   </div>
</div>

<?php get_footer();
