<?php
get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="banner-featured" style="background-image: url('<?php echo $asset_path ?>/assets/img/banner-indeci.png')">
   <div class="container"> <h1>Instituto Nacional de Defensa Civil</h1> </div>
</div>
<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content" role="main" style="transform: none;">
      <div class="container" style="transform: none;">
         <div class="row single-main-content single-1" style="transform: none;">
            <div id="archive-sidebarsss" class="sidebar sidebar-left col-sx-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar single-sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
               <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 0px; left: 332.333px;">
                  <aside id="categories-8" class="widget widget_categories">
                     <h2 class="widgettitle">La Institución</h2>
                     <?php wp_nav_menu( ['menu' => 'menu-institucion'] ) ?>
                  </aside>
               </div>
            </div>
            <div class=" content-padding-left main-content content-left col-sx-12 col-sm-12 col-md-9 col-lg-9">
               <div class="content-inner">
                  <div class="box-article">
                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
                           
                        <!--<h1><?php the_title(); ?></h1>-->
                        <div class="article>">
                              <?php the_content() ?>
                        </div>


                     </article>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>

<?php get_footer();
