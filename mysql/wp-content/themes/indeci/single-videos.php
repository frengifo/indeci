<?php
get_header(); 

$asset_path = get_template_directory_uri();


?>


<div class="crumbs">
  <div class="wrapper">
    <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
  </div>
</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php $tax_slug = 'categoria_video'; ?>

<?php 
	$terms = get_the_terms( get_the_ID(), $tax_slug );
    if ( !empty( $terms ) ){ $term = array_shift( $terms ); }

?>
<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content " role="main" style="transform: none;">
   		<div class="wrapper">
   			<div class="box-title clearfix">
               <h2 class="title-left"><?php echo $term->name; ?></h2>
            </div>
           
            <?php $section_slug = 'videos'; ?>
           	<?php include(locate_template('partials/content-category-list.php')); ?>
			
            		
           
   		</div>
      <div class="full-gallery-single full-video-page" style="transform: none;">

	        <div class=" wrapper single-main-content single-1" style="transform: none;">
	            
	            <?php $current_term_title = single_term_title("", false); ?>
	            <div class="row">
		            <div class=" col-sx-12 col-sm-12 ">
		               <div class="content-inner">
		                  <div class="box-article">
		                  		<h1 class="title-detail-section"><?php the_title() ?></h1>
		                     <article  class="post-1421 post type-post status-publish ">
		                        
		                     	<?php the_content(); ?>


		                     </article>
		                  </div>
		               </div>
		            </div>
		        </div>

	        </div>

      </div>


   </div>
</div>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php endif; ?>
<?php get_footer();
