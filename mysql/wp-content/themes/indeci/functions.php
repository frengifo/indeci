<?php 
add_theme_support( 'post-thumbnails' ); 
add_theme_support( 'menus' );
add_theme_support( 'post-formats', array( 'gallery','video' ) );
function is_page_parent($pid) {      // $pid = The ID of the page we're looking for pages underneath
	global $post;         // load details about this page
	if( ($post->post_parent==$pid||is_page($pid) ) ) 
               return true;   // we're at the page or at a sub page
	else 
               return false;  // we're elsewhere
};
function themename_custom_post_formats_setup() {
    // add post-formats to post_type 'page'
    add_post_type_support( 'que_hacemos', 'post-formats' );
    // add post-formats to post_type 'my_custom_post_type'
    //add_post_type_support( 'my_custom_post_type', 'post-formats' );
}
add_action( 'init', 'themename_custom_post_formats_setup' );

function the_breadcrumbs() {

    global $post;

    if (!is_home()) {

        echo "<a href='";
        echo get_option('home');
        echo "'>";
        echo "<i class='fas fa-home'></i>";
        echo "</a>";

        if (is_category() || is_single()) {

            echo " > ";
            $cats = get_the_category( $post->ID );

            foreach ( $cats as $cat ){
                echo $cat->cat_name;
                echo " > ";
            }
            if (is_single()) {
                the_title();
            }
        } elseif (is_page()) {

            if($post->post_parent){
                $anc = get_post_ancestors( $post->ID );
                $anc_link = get_page_link( $post->post_parent );

                foreach ( $anc as $ancestor ) {
                    $output = " > <a href=".$anc_link.">".get_the_title($ancestor)."</a> > ";
                }

                echo $output;
                the_title();

            } else {
                echo ' > ';
                echo the_title();
            }
        }
    }
elseif (is_tag()) {single_tag_title();}
elseif (is_day()) {echo"Archive: "; the_time('F jS, Y'); echo'</li>';}
elseif (is_month()) {echo"Archive: "; the_time('F, Y'); echo'</li>';}
elseif (is_year()) {echo"Archive: "; the_time('Y'); echo'</li>';}
elseif (is_author()) {echo"Author's archive: "; echo'</li>';}
elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "Blogarchive: "; echo'';}
elseif (is_search()) {echo"Search results: "; }
}


class Nav_Page_Custom_Walker extends Walker_Page {

    function start_el( &$output, $page, $depth, $args, $current_page = 0 ) {
        if ( $depth )
            $indent = str_repeat("\t", $depth);
        else
            $indent = '';
            extract($args, EXTR_SKIP);
            $css_class = array('page_item', 'page-item-'.$page->ID);
        if ( !empty($current_page) ) {
            $_current_page = get_post( $current_page );
            if ( in_array( $page->ID, $_current_page->ancestors ) )
                $css_class[] = 'current_page_ancestor';
            if ( $page->ID == $current_page )
                $css_class[] = 'current_page_item';
            elseif ( $_current_page && $page->ID == $_current_page->post_parent )
                $css_class[] = 'current_page_parent';
        }
        elseif ( $page->ID == get_option('page_for_posts') ) {
            $css_class[] = 'current_page_parent';
        }

        $hide = get_field( 'ocultar_menu' , $page->ID ) ? true:false;

        $css_class = implode( ' ', apply_filters( 'page_css_class', $css_class, $page, $depth, $args, $current_page ) );
        //$css_class = $hide ? $css_class." hide_menu":$css_class;
        $icon_class = get_post_meta($page->ID, 'icon_class', true); //Retrieve stored icon class from post meta

        if( !$hide ){
	        $output .= $indent . '<li class="' . $css_class . '">';

                $permalink_url = get_permalink($page->ID);

                if ( get_field('redireccion', $page->ID) ) {
                    $permalink_url =     get_field( 'enlace_redirect', $page->ID);
                }

	            $output .= '<a href="' . $permalink_url . '">' ;

	                if (  get_field( 'nombre_diferente' , $page->ID ) ) {
	                	$output.= get_field( 'nombre_menu' , $page->ID );
	                }else{
	                	$output .= $page->post_title;
	                }

	            $output .= '</a></li>';

        }

    }
}


function get_posts_by_cat() {
    // Implement ajax function here
    //print_r( $_POST );
    $noticias = get_posts([
                  'post_status' => 'publish',
                  'numberposts' => 6,
                  'category'    => $_POST['category']
                ]); ?>

    <?php foreach ( $noticias as $post ) : setup_postdata( $post ); ?>
                              
         <?php 
            $the_permalink = get_the_permalink( $post->ID );
            $terms = get_the_terms( $post->ID, 'category' );
            if ( !empty( $terms ) ){ $term = array_shift( $terms ); }
         ?>

              <div class="col-xs-12 col-sm-6 hidden-description hidden-meta">
                 <article class="post-item post-grid disss clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video">
                    <div class="article-tran hover-share-item">
                       <div class="post-image">
                          <a href="<?php echo $the_permalink; ?>" class="bgr-item"></a>
                          <a href="<?php echo $the_permalink; ?>"> <img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'large' ); ?>"> </a>
                          <span class="post-cat "> <a href="<?php echo get_term_link( $term, 'category' ) ?>" title="<?php echo $term->name ?>"> <?php echo $term->name ?>  </a> </span>
                       </div>
                       <div class="article-content   hidden-view hidden-comments">
                          <div class="entry-header clearfix">
                             <div class="entry-header-title">
                                <h3 class="entry-title"><a href="<?php echo $the_permalink; ?>"><?php echo $post->post_title ?></a></h3>
                             </div>
                          </div>
                          <div class="entry-content"></div>
                       </div>
                    </div>
                 </article>
              </div>

      <?php endforeach; 
      wp_reset_postdata(); ?>

    <?php wp_die(); ?>
<?php 
}
add_action( 'wp_ajax_get_posts_by_cat', 'get_posts_by_cat' );    // If called from admin panel
add_action( 'wp_ajax_nopriv_get_posts_by_cat', 'get_posts_by_cat' );  
?>