<?php
get_header(); 

$asset_path = get_template_directory_uri();


?>


<div class="crumbs">
  <div class="wrapper">
    <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
  </div>
</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


<?php 
	$tax_slug = "tipo_capacitacion";
	$section_slug = "capacitacion";
	$terms = get_the_terms( get_the_ID(), $tax_slug );
    if ( !empty( $terms ) ){ $term = array_shift( $terms ); }

?>

<div id="content" class="site-content" style="transform: none;">
   <section class="welcome-section">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-8">
	                <div class="row">
	                    <div class="col-md-12">

	                        <div class="blog-single blog-details">
	                            <div class="blog-thumb">
	                                
	                                <?php  
		                            // GALERIA FOTO GALERIAS
					   
				                     $items = get_field('imagenes_fotogaleria'); ?>
				                    
				                    <?php if( $items ): ?>
					                    <div class="gallery-slider">
				                             <?php foreach( $items as $item ): ?>
				                                 
				                                 <div class="col-xs-12 col-sm-4 col-md-4 hidden-description hidden-meta">
												    <article class="post-item post-grid">
												        <div class="article-tran hover-share-item">
												           <div class="post-image">
												              <a href="javascript:;" class="bg-img" style="background-image: url('<?php echo $item['foto']; ?>')"> <img src="<?php echo $item['foto']; ?>" alt=""> </a>
												           </div>
												           <?php if (!empty($item['titulo'])): ?>
												           	
													           <div class="article-content   hidden-view hidden-comments">
													              <div class="entry-header clearfix">
													                 <div class="entry-header-title">
													                    <h3 class="entry-title"><a href="javascript:;"><?php echo $item['titulo']; ?></a></h3>
													                    <p><?php echo $item['descripcion']; ?></p>
													                 </div>
													              </div>
													           </div>

												           <?php endif ?>

												        </div>
												    </article>
												</div>

				                             <?php endforeach; ?>
				                        </div>
					                <?php else: ?>         
					                	<img src="<?php the_post_thumbnail_url( 'large' ) ?>" alt="<?php the_title(); ?>" width="100%">
				                    <?php endif; ?>

	                            </div>
	                            <div class="meta-date">
	                            	
	                                <h4><span><?php echo get_field('fecha_inicio') ; ?></span><h4>
	                            </div>
	                            <div class="blog-content">
	                                <h3><?php the_title(); ?></h3>
	                                
	                                <article>
	                                	<?php the_content() ?>
	                                </article>

	                                <div class="blog-btm-meta">
	                                    <div class="blog-socail">
	                                        <ul class="social-links">
	                                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
	                                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
	                                        </ul>
	                                   </div>
	                                    <ul class="blg-likes">
	                                        <li><?php echo $term->name; ?></li>
	                                    </ul>
	                                </div>

	                                <div class="clear"></div>
	                                <p>&nbsp;</p>
	                                <div class="box-title clearfix">
			                           <h2 class="title-left">TAMBIÉN TE PUEDE INTERESAR</h2>
			                        </div>
	                                <hr>
	                                <div class="clear"></div>
	                                <?php 

			                        	$galerias_full = get_posts([
			                        	  'exclude' => get_the_ID(),
				                          'numberposts' => 4,
				                          'category' => $term->term_id,
				                          'post_type' => 'capacitacion'
				                        ]);
			                        	$cols = true;

			                        	$row_count = 1;

			                        ?>

				                    <div class="lists-posts-gallery ">
				                    	<?php foreach ( $galerias_full as $post ) : setup_postdata( $post ); ?>
				                    	
					                        	<?php $cls_cols = 'col-xs-12 col-sm-6'; ?>
					                        	
					                       		<?php include(locate_template('partials/content-post-list-item.php')); ?>

					                        
					                        <?php if ( $row_count % 2 == 0): ?>
					                        	<div class="clear"></div>
					                        <?php endif ?>

					                        <?php $row_count++; ?>
				                    
					                    <?php endforeach; 
				                        wp_reset_postdata(); ?>
		                        	</div>
	                            </div>
	                        </div>
	                        
	                    </div>
	                </div>
	               
	            </div>
	            <div class="col-md-4">
	                <div class="sidebar-feature">
	                    <div class="row">
	                        
	                        <div class="col-md-12 mb-30">
	                            <h4 class="sidebar-title">Categorias</h4>
	                            <div class="cause-tags">
	                                <?php include(locate_template('partials/content-category-list.php')); ?>
	                            </div>
	                        </div>
	                        <div class="col-md-12 mb-30">
	                            <h4 class="sidebar-title">Últimas Capacitaciones</h4>
	                            
	                            <?php 

		                        	$galerias_full = get_posts([
			                          'numberposts' => 5,
			                          'exclude' => get_the_ID(),
			                          'post_type' => 'capacitacion'
			                        ]);
		                        	$cols = true;

		                        	$row_count = 1;
		                        ?>

		                        <?php foreach ( $galerias_full as $post ) : setup_postdata( $post ); ?>

		                        	<?php $terms = get_the_terms( get_the_ID(), $tax_slug );
    									if ( !empty( $terms ) ){ $term = array_shift( $terms ); } ?>
		                            <div class="news-post post-grid">
		                                <div class="thumb">
		                                    <img alt="" src="<?php the_post_thumbnail_url('small') ?>">
		                                </div>
		                                <div class="content">
		                                    <h4><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h4>
		                                    <p class="date"><i class="fa fa-clock-o"></i><?php echo get_the_date(); ?></p>
		                                </div>
		                                <span class="post-cat ">
							              	<a href="<?php echo get_term_link( $term, $tax_slug ) ?>" title="<?php echo $term->name ?>"> <?php echo $term->name ?> 	</a>
							            </span>
		                            </div>
		                        
		                        <?php endforeach; 
			                    wp_reset_postdata(); ?>

	                        </div>
	                      
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</div>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php endif; ?>
<?php get_footer();
