<?php

get_header(); 
?>
<style type="text/css">
	.loop-attach ul li a
	{
		font-size: 11px;
	}
</style>
<?php $asset_path = get_template_directory_uri(); $banner_large = $asset_path.'/assets/img/banner-indeci.png'; ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class="banner-featured <?php echo $slug_post_type; ?>" style="background-image: url('<?php echo $banner_large ?>')">
	   <div class="container"> <h1>Camapañas</h1> </div>
	</div>
	<div class="crumbs">
	  <div class="wrapper">
	    <a href="/campanas"> <i class="fas fa-home"></i>  Todas las Campañas</a>
	  </div>
	</div>
	<section class="single-causes-area section">
        <div class="container">
            <div class="row">
            	
                <div class="col-xs-12">
                	<h1><?php the_title() ?></h1>
                	<?php if (get_field('galeria')): ?>
                		<div class="gallery-slider first_gallery">
		                	<?php foreach (get_field('galeria') as $k => $v): ?>
		                		<div>
		                			
									
										<img src="<?php echo $v['url']; ?>" width="100%" >

		                		</div>
							<?php endforeach ?>
                	<?php else: ?>
                		
	                    <div class="causes-img">
	                        <figure>
	                            <img src="<?php the_post_thumbnail_url( 'full' ) ?>" alt="" width="100%">
	                        </figure>
	                    </div>
                	<?php endif ?>
                </div>
               
                <div class="clear"></div>
                <div class="col-xs-12">
                	<p>&nbsp;</p>
                	<?php the_content() ?>
                </div>
            </div>
            <div class="row mt50">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="disce mr-t70 mr-b70">
						
						<div class="info-<?php the_field('region') ?>">
							
							<?php $sections = get_field('type_content'); $i=1; ?>
							<div class="row">
								<?php foreach ($sections as $k => $section): ?>
									
									<div class="col-xs-12 col-sm-6">
										<h3><?php echo $section['section_title'] ?></h3>

										<div class="loop-attach">

											<?php foreach ($section['files_gallery'] as $p => $files_gallery): ?>
													
												<?php if ( $files_gallery['acf_fc_layout'] == 'files_type' ): ?>
														
														<ul>
															
															<?php foreach ($files_gallery['attach_files'] as $q => $attach): ?>
																<li>
																	<a href="<?php echo $attach['file_source'] ?>" download>
																		<?php echo $attach['name_file'] ?> <span class="ti-download"></span>
																	</a>
																</li>
																
															<?php endforeach ?>	

														</ul>

												<?php endif ?>

												<?php if ( $files_gallery['acf_fc_layout'] == 'link_type' ): ?>
														
														<ul>
															
															<?php foreach ($files_gallery['links'] as $q => $attach): ?>
																<li>
																	<a href="<?php echo $attach['url'] ?>" target="_blank">
																		<?php echo $attach['name_link'] ?> <span class="ti-download"></span>
																	</a>
																</li>
																
															<?php endforeach ?>	

														</ul>

												<?php endif ?>

												<?php if ( $files_gallery['acf_fc_layout'] == 'gallery' ): ?>

													<?php foreach ($files_gallery['gallery_pictures'] as $q => $image): ?>
															<a href="<?php echo $image['url'] ?>" data-fancybox="gallery">
																<img src="<?php echo $image['sizes']['medium'] ?>">
															</a>
													<?php endforeach ?>	

												<?php endif ?>	

											<?php endforeach ?>
										
										</div>

									</div>

									<?php if ($i % 2 == 0): ?>
										<div class="clear"></div>
									<?php endif ?>
									<?php $i++; ?>
								<?php endforeach ?>
							</div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
		
					

	<!-- post -->
	<?php endwhile; ?>
	<!-- post navigation -->
	<?php else: ?>
	<!-- no posts found -->
	<?php endif; ?>

<?php get_footer();
