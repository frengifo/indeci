<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
	<section class="content general alerts-archive">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>Portal de Alertas</h3>
					<!-- <ul>
						<li style="width: 97.5%"><a href="/noticias/" class="active">Alertas</a></li>
						<li><a href="#">COEN</a></li>
						<li><a href="#">#INDECITeRecomienda</a></li>
					</ul> -->
					<?php $tax_slug = 'informe'; ?>
					<?php include(locate_template('partials/content-category-list.php')); ?>
				</nav>

				<div class="advertising">
					<a href="/campanas">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="/campanas">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
			
			<div class="post-content">
				<h1><a href="/alertas">Alertas</a> / <?php single_term_title(); ?> </h1>
				<p>&nbsp;</p>

				<section class="list-news alerts-archive">

					<?php 
						$current_term = get_queried_object();
						$tax_query =  [
				            'taxonomy' => $current_term->taxonomy,
				            'field' => 'slug',
				            'terms' => $current_term->slug,
				        ] ?>

					<?php query_posts( [ 'posts_per_page' => 15, 'paged' => get_query_var( 'paged' ), 'post_type' => 'alertas', 'tax_query' =>[ $tax_query ] ] ) ?>
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

						<article>
							<a href="<?php the_permalink() ?>" class="img">
								<?php the_post_thumbnail() ?>
							</a>
							<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
							<!-- <p><?php the_date(); ?></p> -->
							<a href="<?php the_field('archivo'); ?>" class="btn-download" target="_blank"> <i class="fas fa-download"></i> DESCARGAR</a>
							<a href="<?php the_permalink(); ?>" class="lnk-view">Ver más</a>
						</article>

					<!-- post -->
					<?php endwhile; ?>
					<!-- post navigation -->
					<ul class="pagination">
                        <?php echo paginate_links(); ?>
                    </ul>

					<?php else: ?>
					<!-- no posts found -->
						<p>No hay alertas disponibles.</p>
					<?php endif; ?>
				</section>
			</div>
		</div>	
		
	</section>

<?php get_footer();
