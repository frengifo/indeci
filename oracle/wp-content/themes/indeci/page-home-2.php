<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
   <meta charset="<?php bloginfo( 'charset' ); ?>">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="profile" href="http://gmpg.org/xfn/11">
   <?php $asset_path = get_template_directory_uri(); ?>
   <link rel="shortcut icon" href="<?php echo $asset_path; ?>/favicon.ico" />
   <link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/components.css">
   <link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/style.css">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
   <!-- <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|Poppins" rel="stylesheet"> -->
   <link href="https://fonts.googleapis.com/css?family=Baloo+Bhaina|Poppins" rel="stylesheet">
   <?php //wp_head(); ?>
   <style type="text/css"> #wpadminbar{ display: none !important; } html{    margin-top: 0 !important; } </style>
   <!-- Add -->
   <link rel="stylesheet" href="http://kenwheeler.github.io/slick/slick/slick.css">
   <link rel="stylesheet" href="<?php echo $asset_path; ?>/assets/custom.css" />
   <!-- Fin -->
   <!-- FACEBOOK -->
   <div id="fb-root"></div>
   <script>(function(d, s, id) {
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) return;
   js = d.createElement(s); js.id = id;
   js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.0&appId=218706935288983&autoLogAppEvents=1';
   fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));</script>
</head>


<body <?php body_class( array( "page-template-default", "page", "wpb-js-composer","js-comp-ver-5.5.2","vc_responsive" ) ); ?> >
   <div id="page" class="wrapper site">
        <div  class="canvas-overlay"></div>

      <header id="masthead" class="site-header header-left header-left2 style_white">

           <div id="bitther-header">
              <div class="header-topbar">
               <div id="bitther-top-navbar" class="top-navbar">
                  <div class="container">
                     <div class="row">
                        <div class="topbar-left col-xs-12 col-sm-8 col-md-7">
                           <a href="/mapa-de-sitio/">Mapa Sitio</a>
                           <a href="http://sinpad.indeci.gob.pe/LibroReclamaciones/Views/Inicio.aspx" target="_blank">Libro de reclamaciones</a>
                           <a href="#">Intranet</a>
                           <a href="https://mail.indeci.gob.pe" target="_blank">Webmail</a>
                           <a href="/trabaja-con-nosotros/">Trabaja con Nosotros</a>
                           <a href="/contactenos/" class="link-high">Contáctenos</a>
                        </div>
                        <div class="topbar-right hidden-xs col-sm-4 col-md-2 clearfix">
                           <div id="bitther_social-2" class="widget first bitther_social">
                              <div class="bitther-social-icon clearfix">
                                 <a href="#fdsfdsf" target="_blank"  class="ion-social-facebook"><span class="ti-facebook"></span></a>
                                 <a href="#" target="_blank" class="ion-social-twitter"><span class="ti-twitter-alt"></span></a>
                                 <a href="#" target="_blank" class="ion-social-youtube"><i class="fab fa-youtube"></i></a>
                              </div>
                           </div>
                        </div>
                        <div class="topbar-right hidden-xs col-md-3 text-right">
                           <div class="">
                                <div class="d-inblock mr-1"><a href="">
                                 <img src="<?php echo $asset_path; ?>/assets/img/pte.png" alt="" width="35">
                                </a></div>
                                <div class="d-inblock"><a href="">
                                 <img src="<?php echo $asset_path; ?>/assets/img/ministerio-defensa.png" alt="" width="180">
                                </a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
              <div class="header-content-logo container">
                 <div class="site-logo" id="logo">
                    <a href="/" rel="home"><img src="<?php echo $asset_path; ?>/assets/img/logo-indeci.png" alt="logo"></a>
                 </div>

                 <div class="main-menu">
               <div id="na-menu-primary" class="nav-menu clearfix">
                       <nav class="text-center na-menu-primary clearfix">
                          <?php wp_nav_menu( [
                              'menu' => 'menu-principal', 
                              'menu_class' => 'nav nav-tabs nav navbar-nav na-menu mega-menu'] ) ?>
                       </nav>
                    </div>
                 </div>

              </div>
              <div class="header-content-right hidden-md hidden-lg">
                 <div class="searchform-mini searchform-moblie hidden-md hidden-lg">
                    <button class="btn-mini-search"><i class="ti-search"></i></button>
                 </div>
                 <div class="searchform-wrap search-transition-wrap bitther-hidden">
                    <div class="search-transition-inner">
                       <form method="get" class="searchform" action="/">
                          <div class="input-group">
                             <input type="text" class="form-control" placeholder="Search ... " value="" name="s">
                             <span class="input-group-btn">
                             <button class="btn btn-primary"><i class="ti-search"></i></button>
                             </span>
                          </div>
                       </form>
                       <button class="btn-mini-close pull-right"><i class="ti-close"></i></button>
                    </div>
                 </div>
              </div>
           </div>
       </header>
       
<?php $asset_path = get_template_directory_uri(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>  
<div id="content" class="site-content">
            <?php $sliders = get_field('slide'); ?>
            
            <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid home1_slide vc_custom_1529559600872">
               <div class="wpb_column vc_column_container vc_col-sm-12">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div  class="box-sliders article-carousel sliders-column4 clearfix featured_content" data-rtl="false"  data-number="3" data-mobile = "1" data-table="2" data-mobilemin = "1" data-dots="false" data-arrows="false">
                           
                              <?php foreach ($sliders as $k => $v): ?>
                               

                                 <div class="col-sm-6 col-md-3 box-large article-item description-hidden">
                                    <article class="post-item post-tran clearfix post-1445 post type-post status-publish format-gallery has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-gallery">
                                       <div class="article-tran hover-share-item">
                                          <div class="post-image lazy" style = "background-image:url('<?php echo $v['imagen_version_movil'] ?>')">
                                             <a href="<?php echo $v['enlace']; ?>"></a>
                                          </div>
                                          <span class="bg-rgb"></span>
                                          <span class="post-cat">                <a href="javascript:;"   title="" >Preparación</a> </span>
                                          <div class="article-content   hidden-view hidden-comments">
                                             <div class="article-content-inner">
                                                <div class="entry-header clearfix">
                                                   <div class="entry-header-title">
                                                      <h3 class="entry-title">
                                                         <a href="<?php echo $v['enlace']; ?>" rel="bookmark">
                                                            <?php echo $v['nombre_slide'] ?>
                                                         </a>
                                                      </h3>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                 </div>
                              <?php endforeach ?>

                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="container" style="margin-bottom: 15px; display:block;">
               <div class="row">
                  <div class="col-xs-12">
                     <a href="<?php echo $sliders[0]['enlace'] ?>" class="banner-top" style = "background-image:url('<?php echo $sliders[0]['imagen'] ?>')">
                     </a>
                  </div>
               </div>
            </div>

<?php $styles = [ 'amarilla' => 'yellow', 'azul' => 'blue', 'naranja' => 'orange', 'verde' => 'green', 'roja' => 'red' ]; ?>
            <div  class="vc_row-full-width vc_clearfix"></div>

            <div class="container">



               <div class="row">




                  <div class="site-main page-content col-sm-12 " role="main">

                     <div class="vc_row wpb_row vc_row-fluid vc_custom_1523892410511 alerts" style="transform: none;">

                        <div class="sidebar wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_hidden-md vc_hidden-sm vc_hidden-xs" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                           <div class="theiaStickySidebar" >
                              <div class="vc_column-inner vc_custom_1530519855841">
                                 <div class="wpb_wrapper">
                                    <div class="wpb_widgetised_column wpb_content_element">
                                       <div class="wpb_wrapper">
                                          <aside id="featured_post-5" class="widget widget_featured_post">
                                             <div class="article-content archive-blog">
                                                <div class="featured-post">
                                                   <div class="row">

                                                      <?php 
                                                         $alertas = get_posts([
                                                           'post_type' => 'alertas',
                                                           'post_status' => 'publish',
                                                           'numberposts' => 5
                                                         ]);
                                                       ?>

                                                      <?php foreach ( $alertas as $post ) : setup_postdata( $post ); ?>

                                                         <?php 
                                                            $terms = get_the_terms( $post->ID, 'tipo' );
                                                            $term = array_shift( $terms );
                                                            $icono = get_field('icono', $term);
                                                         ?>
                                                      
                                                         <div class="col-md-12 item-related category-hidden  description-hidden item-1 ">
                                                            <article class="post-item clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video bg-<?php echo $styles[$term->slug];  ?>">
                                                               <div class="article-tran hover-share-item top_storie">
                                                                  <div class="feature_post_widget bitther-tb">
                                                                     <div class="number bitther-cell left">
                                                                        <img src="http://indeci.apprende.com.pe/wp-content/uploads/2018/07/icon-transparent.png">
                                                                     </div>
                                                                     <div class="caption bitther-cell">
                                                                        <div class="entry-header clearfix">
                                                                           <div class="entry-header-title">
                                                                              <h3 class="entry-title">
                                                                                 <?php $sus = strlen(get_the_title()) > 220 ? "...":""; ?>
                                                                                 <a href="<?php the_permalink(); ?>"><?php echo mb_strimwidth(get_the_title(), 0, 220, $sus); ?></a>
                                                                              </h3>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </article>
                                                         </div>
                                                      
                                                      <?php endforeach; 
                                                      wp_reset_postdata();?>

                                                   </div>
                                                </div>
                                             </div>
                                          </aside>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <aside class="text-center">
                              <a class="twitter-timeline" data-height="400" data-theme="light" data-link-color="#FAB81E" href="https://twitter.com/indeciperu?ref_src=twsrc%5Etfw"></a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                              <p>&nbsp;</p>
                              <div class="fb-page" data-href="https://www.facebook.com/indeci/" data-tabs="timeline" data-width="390" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/indeci/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/indeci/">Instituto Nacional de Defensa Civil Perú</a></blockquote></div>
                           </aside>
                        </div>

                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-9 content-right">
                           <div class="vc_column-inner vc_custom_1530519865827">
                              <div class="wpb_wrapper">
                                 <div class="wrapper-posts box-recent type-loadMore layout-grid pagi-no description-hidden post-format-yes" data-layout="grid" data-paged="3" data-col="col-xs-12 col-sm-6 col-md-6 col-item-2" data-cat="bitcoin" data-number="4" data-ads="large-rectangle">
                                    <div class="dot_title box-title clearfix">
                                       <h2 class="title-left">Información destacada</h2>
                                    </div>
                                    <span class="agr-loading"></span>
                                    <div class="tab-content">
                                       <div id="allCat" class="archive-blog affect-isotope row active description-hidden" style="position: relative;">

                                          <?php $sliders = get_field('slides_2')[0]; ?>
                                              
                                          <div class="col-item col-xs-12 col-sm-6 col-md-6 col-item-2">
                                             <article class="post-item post-grid disss clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video">
                                                <div class="article-tran hover-share-item">
                                                   <div class="post-image">
                                                      <a href="<?php echo $sliders['enlace'] ?>" class="bgr-item"></a>
                                                      <a href="<?php echo $sliders['enlace'] ?>">
                                                      <img class="lazy" src="<?php echo $sliders['portada'] ?>" alt="post-image" style="display: inline;">
                                                      </a>
                                                      <span class="post-cat "> <a href="<?php echo $sliders['enlace'] ?>" title=""><?php echo $sliders['titulo'] ?></a>
                                                      </span>
                                                   </div>
                                                   <div class="article-content   hidden-view hidden-comments">
                                                      <div class="entry-header clearfix">
                                                         <div class="entry-header-title">
                                                            <h3 class="entry-title"><a href="<?php echo $sliders['enlace'] ?>"><?php echo $sliders['titulo'] ?></a></h3>
                                                         </div>
                                                      </div>
                                                      <div class="entry-content"></div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>


                                          <div class="col-item col-xs-12 col-sm-6 col-md-6 col-item-2">
                                             <article class="post-item post-grid disss clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video">
                                                <div class="article-tran hover-share-item">
                                                   <div class="post-image">
                                                      <a href="<?php echo $sliders['enlace'] ?>" class="bgr-item"></a>
                                                      <a href="<?php echo $sliders['enlace'] ?>">
                                                      <img class="lazy" src="<?php echo $sliders['portada'] ?>" alt="post-image" style="display: inline;">
                                                      </a>
                                                      <span class="post-cat "> <a href="<?php echo $sliders['enlace'] ?>" title=""><?php echo $sliders['titulo'] ?></a>
                                                      </span>
                                                   </div>
                                                   <div class="article-content   hidden-view hidden-comments">
                                                      <div class="entry-header clearfix">
                                                         <div class="entry-header-title">
                                                            <h3 class="entry-title"><a href="<?php echo $sliders['enlace'] ?>"><?php echo $sliders['titulo'] ?></a></h3>
                                                         </div>
                                                      </div>
                                                      <div class="entry-content"></div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>


                                          <div class="col-item col-xs-12 col-sm-6 col-md-6 col-item-2">
                                             <article class="post-item post-grid disss clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video">
                                                <div class="article-tran hover-share-item">
                                                   <div class="post-image">
                                                      <a href="<?php echo $sliders['enlace'] ?>" class="bgr-item"></a>
                                                      <a href="<?php echo $sliders['enlace'] ?>">
                                                      <img class="lazy" src="<?php echo $sliders['portada'] ?>" alt="post-image" style="display: inline;">
                                                      </a>
                                                      <span class="post-cat "> <a href="<?php echo $sliders['enlace'] ?>" title=""><?php echo $sliders['titulo'] ?></a>
                                                      </span>
                                                   </div>
                                                   <div class="article-content   hidden-view hidden-comments">
                                                      <div class="entry-header clearfix">
                                                         <div class="entry-header-title">
                                                            <h3 class="entry-title"><a href="<?php echo $sliders['enlace'] ?>"><?php echo $sliders['titulo'] ?></a></h3>
                                                         </div>
                                                      </div>
                                                      <div class="entry-content"></div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>


                                          <div class="col-item col-xs-12 col-sm-6 col-md-6 col-item-2">
                                             <article class="post-item post-grid disss clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video">
                                                <div class="article-tran hover-share-item">
                                                   <div class="post-image">
                                                      <a href="<?php echo $sliders['enlace'] ?>" class="bgr-item"></a>
                                                      <a href="<?php echo $sliders['enlace'] ?>">
                                                      <img class="lazy" src="<?php echo $sliders['portada'] ?>" alt="post-image" style="display: inline;">
                                                      </a>
                                                      <span class="post-cat "> <a href="<?php echo $sliders['enlace'] ?>" title=""><?php echo $sliders['titulo'] ?></a>
                                                      </span>
                                                   </div>
                                                   <div class="article-content   hidden-view hidden-comments">
                                                      <div class="entry-header clearfix">
                                                         <div class="entry-header-title">
                                                            <h3 class="entry-title"><a href="<?php echo $sliders['enlace'] ?>"><?php echo $sliders['titulo'] ?></a></h3>
                                                         </div>
                                                      </div>
                                                      <div class="entry-content"></div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>
                                             



                                       </div>
                                    </div>
                                 </div>

                                 <div class="recent-post_wrap slide">
                                    <h2 class="widgettitle">Recomendaciones de #INDECI</h2>
                                    <div class="recent-post-slider slick-initialized slick-slider">
                                       <i class="ti-angle-right slick-arrow" style="display: block;"></i>
                                       <div aria-live="polite" class="slick-list draggable">
                                          <div class="slick-track" role="listbox">
                                             <div class="recent-post disable_cat yes slick-slide slick-cloned" data-slick-index="-2" aria-hidden="true" style="width: 408px;" tabindex="-1">
                                                <article class="post-item post-sidebar clearfix post-1445 post type-post status-publish format-gallery has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-gallery">
                                                   <div class="article-image">
                                                      <div class="post-image">
                                                         <a href="" tabindex="-1">
                                                         <img class="lazy" src="http://bitther.nanoagency.co/wp-content/themes/bitther/assets/images/layzyload-sidebar.jpg" data-original="http://bitther.nanoagency.co/wp-content/uploads/2018/04/24-100x90.png" data-src="http://bitther.nanoagency.co/wp-content/uploads/2018/04/24-100x90.png" alt="post-image">
                                                         </a>
                                                      </div>
                                                   </div>
                                                   <div class="article-content side-item-text ">
                                                      <div class="entry-header sidebar_recent_post clearfix">
                                                         <div class="entry-header-title info_post">
                                                            <div class="category-view">
                                                               <span class="post-cat">                <a href="/category/bitcoin/" title="" tabindex="-1">Inundaciones</a>
                                                               </span>
                                                            </div>
                                                            <h3 class="entry-title"><a href="" rel="bookmark" tabindex="-1">Cuando debo aplicar mi Plan de Emergencia en caso de...</a></h3>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </article>
                                             </div>
                                             <div class="recent-post disable_cat yes slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" style="width: 408px;" tabindex="-1">
                                                <article class="post-item post-sidebar clearfix post-1442 post type-post status-publish format-standard has-post-thumbnail hentry category-bitcoin tag-blockchain tag-coin tag-crypto">
                                                   <div class="article-image">
                                                      <div class="post-image">
                                                         <a href="" tabindex="-1">
                                                         <img class="lazy" src="http://bitther.nanoagency.co/wp-content/uploads/2018/04/29-100x90.png" data-original="http://bitther.nanoagency.co/wp-content/uploads/2018/04/29-100x90.png" alt="post-image" style="display: block;">
                                                         </a>
                                                      </div>
                                                   </div>
                                                   <div class="article-content side-item-text ">
                                                      <div class="entry-header sidebar_recent_post clearfix">
                                                         <div class="entry-header-title info_post">
                                                            <div class="category-view">
                                                               <span class="post-cat">                <a href="" title="" tabindex="-1">Inundaciones</a>
                                                               </span>
                                                            </div>
                                                            <h3 class="entry-title"><a href="" rel="bookmark" tabindex="-1">At Tax Time, Who Really Owns That Crypto Anyway? – Expert Take</a></h3>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </article>
                                             </div>
                                          </div>
                                       </div>
                                       <i class="ti-angle-left slick-arrow" style="display: block;"></i>
                                    </div>
                                 </div>
                                 <div class="box-cats wrapper-posts layout-box1 mb-30 " data-layout="box1" data-typepost="views" data-dates="-2 year" data-paged="4" data-col="col-xs-12 col-sm-4 col-md-4" data-cat="bitcoin,blockchain,investment" data-number="6">
                                    <div class="box-title clearfix">
                                       <h2 class="title-left">Últimas Noticias</h2>
                                       <div class="box-filter clearfix">
                                          <ul class="wrapper-filter" data-filter="true">
                                             <li class="active"><span class="cat-item loaded" data-catfilter="allCat-28">Todo</span></li>
                                             <li class=""><span class="cat-item loaded" data-catfilter="bitcoin">Bóletines</span></li>
                                             <li><span class="cat-item" data-catfilter="blockchain">Preparación</span></li>
                                             <li><span class="cat-item" data-catfilter="investment">Respuesta</span></li>
                                             <li><span class="cat-item" data-catfilter="investment">Rehabilitación</span></li>
                                          </ul>
                                       </div>
                                    </div>
                                    <span class="agr-loading"></span>
                                    <div class="tab-content">
                                       <div id="allCat-28" class="box-blog archive-blog row large-vertical clearfix active">
                                          <div class="col-xs-12 col-sm-4 col-md-4 hidden-description hidden-meta">
                                             <article class="post-item post-grid disss clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video">
                                                <div class="article-tran hover-share-item">
                                                   <div class="post-image">
                                                      <a href="/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-3/" class="bgr-item"></a>
                                                      <a href="2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-3/">
                                                      <img class="lazy" src="http://bitther.nanoagency.co/wp-content/uploads/2018/04/01-1-510x328.jpg" data-lazy="http://bitther.nanoagency.co/wp-content/uploads/2018/04/01-1-510x328.jpg" alt="post-image" style="display: inline;">
                                                      </a>
                                                      <span class="post-cat ">                <a href="/category/bitcoin/" title="">Bitcoin</a>
                                                      </span>
                                                   </div>
                                                   <div class="article-content   hidden-view hidden-comments">
                                                      <div class="article-meta clearfix">
                                                         <span class="byline author-title"><span class="ti-minus"></span><span class="by">By</span><span class="author vcard"><span class="screen-reader-text">Author </span><a class="url fn n" href="/author/admin/">Daigo</a></span></span><span class="posted-on"><i class="icon ti-calendar" aria-hidden="true"></i><span class="screen-reader-text">Posted on </span><a href="/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-3/" rel="bookmark"><time class="entry-date published" datetime="2018-04-15T17:39:12+00:00">April 15, 2018</time><time class="updated" datetime="2018-07-02T04:28:16+00:00">July 2, 2018</time></a></span>
                                                         <div class="entry-meta-right">
                                                            <div class="total-view">
                                                               <i class="ti-eye"></i> 713
                                                            </div>
                                                            <span class="comments-link">
                                                            <a href="/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-3/#respond" class="text-comment"><i class="icon ti-comment-alt" aria-hidden="true"></i> 0</a>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="entry-header clearfix">
                                                         <div class="entry-header-title">
                                                            <h3 class="entry-title"><a href="/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-3/">Japan’s Banking Giant SBI Delays Launch</a></h3>
                                                         </div>
                                                      </div>
                                                      <div class="entry-content"></div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>
                                          <div class="col-xs-12 col-sm-4 col-md-4 hidden-description hidden-meta">
                                             <article class="post-item post-grid disss clearfix post-1445 post type-post status-publish format-gallery has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-gallery">
                                                <div class="article-tran hover-share-item">
                                                   <div class="post-image">
                                                      <a href="/2018/04/15/liechtensteins-bank-frick-introduces-direct-cryptocurrency-investment-4/" class="bgr-item"></a>
                                                      <a href="2018/04/15/liechtensteins-bank-frick-introduces-direct-cryptocurrency-investment-4/">
                                                      <img class="lazy" src="http://bitther.nanoagency.co/wp-content/uploads/2018/04/24-510x328.png" data-lazy="http://bitther.nanoagency.co/wp-content/uploads/2018/04/24-510x328.png" alt="post-image" style="display: inline;">
                                                      </a>
                                                      <span class="post-cat ">                <a href="/category/bitcoin/" title="">Bitcoin</a>
                                                      </span>
                                                   </div>
                                                   <div class="article-content   hidden-view hidden-comments">
                                                      <div class="article-meta clearfix">
                                                         <span class="byline author-title"><span class="ti-minus"></span><span class="by">By</span><span class="author vcard"><span class="screen-reader-text">Author </span><a class="url fn n" href="/author/admin/">Daigo</a></span></span><span class="posted-on"><i class="icon ti-calendar" aria-hidden="true"></i><span class="screen-reader-text">Posted on </span><a href="/2018/04/15/liechtensteins-bank-frick-introduces-direct-cryptocurrency-investment-4/" rel="bookmark"><time class="entry-date published" datetime="2018-04-15T17:32:31+00:00">April 15, 2018</time><time class="updated" datetime="2018-07-02T07:26:21+00:00">July 2, 2018</time></a></span>
                                                         <div class="entry-meta-right">
                                                            <div class="total-view">
                                                               <i class="ti-eye"></i> 640
                                                            </div>
                                                            <span class="comments-link">
                                                            <a href="/2018/04/15/liechtensteins-bank-frick-introduces-direct-cryptocurrency-investment-4/#respond" class="text-comment"><i class="icon ti-comment-alt" aria-hidden="true"></i> 0</a>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="entry-header clearfix">
                                                         <div class="entry-header-title">
                                                            <h3 class="entry-title"><a href="/2018/04/15/liechtensteins-bank-frick-introduces-direct-cryptocurrency-investment-4/">Cambodia May Issue A National Cryptocurrency</a></h3>
                                                         </div>
                                                      </div>
                                                      <div class="entry-content"></div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>
                                          <div class="col-xs-12 col-sm-4 col-md-4 hidden-description hidden-meta">
                                             <article class="post-item post-grid disss clearfix post-1442 post type-post status-publish format-standard has-post-thumbnail hentry category-bitcoin tag-blockchain tag-coin tag-crypto">
                                                <div class="article-tran hover-share-item">
                                                   <div class="post-image">
                                                      <a href="/2018/04/15/at-tax-time-who-really-owns-that-crypto-anyway-expert-take-2/" class="bgr-item"></a>
                                                      <a href="2018/04/15/at-tax-time-who-really-owns-that-crypto-anyway-expert-take-2/">
                                                      <img class="lazy" src="http://bitther.nanoagency.co/wp-content/uploads/2018/04/29-510x328.png" data-lazy="http://bitther.nanoagency.co/wp-content/uploads/2018/04/29-510x328.png" alt="post-image" style="display: inline;">
                                                      </a>
                                                      <span class="post-cat ">                <a href="/category/bitcoin/" title="">Bitcoin</a>
                                                      </span>
                                                   </div>
                                                   <div class="article-content   hidden-view hidden-comments">
                                                      <div class="article-meta clearfix">
                                                         <span class="byline author-title"><span class="ti-minus"></span><span class="by">By</span><span class="author vcard"><span class="screen-reader-text">Author </span><a class="url fn n" href="/author/admin/">Daigo</a></span></span><span class="posted-on"><i class="icon ti-calendar" aria-hidden="true"></i><span class="screen-reader-text">Posted on </span><a href="/2018/04/15/at-tax-time-who-really-owns-that-crypto-anyway-expert-take-2/" rel="bookmark"><time class="entry-date published" datetime="2018-04-15T17:31:30+00:00">April 15, 2018</time><time class="updated" datetime="2018-07-02T04:28:41+00:00">July 2, 2018</time></a></span>
                                                         <div class="entry-meta-right">
                                                            <div class="total-view">
                                                               <i class="ti-eye"></i> 417
                                                            </div>
                                                            <span class="comments-link">
                                                            <a href="/2018/04/15/at-tax-time-who-really-owns-that-crypto-anyway-expert-take-2/#respond" class="text-comment"><i class="icon ti-comment-alt" aria-hidden="true"></i> 0</a>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="entry-header clearfix">
                                                         <div class="entry-header-title">
                                                            <h3 class="entry-title"><a href="/2018/04/15/at-tax-time-who-really-owns-that-crypto-anyway-expert-take-2/">At Tax Time, Who Really Owns That Crypto Anyway? – Expert Take</a></h3>
                                                         </div>
                                                      </div>
                                                      <div class="entry-content"></div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>
                                          <div class="col-xs-12 col-sm-4 col-md-4 hidden-description hidden-meta">
                                             <article class="post-item post-grid disss clearfix post-1433 post type-post status-publish format-video has-post-thumbnail hentry category-investment tag-blockchain post_format-post-format-video">
                                                <div class="article-tran hover-share-item">
                                                   <div class="post-image">
                                                      <a href="/2018/04/15/australians-can-now-purchase-btc-eth-across-1200-newsstands-2/" class="bgr-item"></a>
                                                      <a href="2018/04/15/australians-can-now-purchase-btc-eth-across-1200-newsstands-2/">
                                                      <img class="lazy" src="http://bitther.nanoagency.co/wp-content/uploads/2018/04/15-510x328.jpg" data-lazy="http://bitther.nanoagency.co/wp-content/uploads/2018/04/15-510x328.jpg" alt="post-image" style="display: inline;">
                                                      </a>
                                                      <span class="post-cat ">                <a href="/category/investment/" title="">Investment</a>
                                                      </span>
                                                   </div>
                                                   <div class="article-content   hidden-view hidden-comments">
                                                      <div class="article-meta clearfix">
                                                         <span class="byline author-title"><span class="ti-minus"></span><span class="by">By</span><span class="author vcard"><span class="screen-reader-text">Author </span><a class="url fn n" href="/author/admin/">Daigo</a></span></span><span class="posted-on"><i class="icon ti-calendar" aria-hidden="true"></i><span class="screen-reader-text">Posted on </span><a href="/2018/04/15/australians-can-now-purchase-btc-eth-across-1200-newsstands-2/" rel="bookmark"><time class="entry-date published" datetime="2018-04-15T17:27:04+00:00">April 15, 2018</time><time class="updated" datetime="2018-07-02T07:27:39+00:00">July 2, 2018</time></a></span>
                                                         <div class="entry-meta-right">
                                                            <div class="total-view">
                                                               <i class="ti-eye"></i> 298
                                                            </div>
                                                            <span class="comments-link">
                                                            <a href="/2018/04/15/australians-can-now-purchase-btc-eth-across-1200-newsstands-2/#respond" class="text-comment"><i class="icon ti-comment-alt" aria-hidden="true"></i> 0</a>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="entry-header clearfix">
                                                         <div class="entry-header-title">
                                                            <h3 class="entry-title"><a href="/2018/04/15/australians-can-now-purchase-btc-eth-across-1200-newsstands-2/">Australians Can Now Purchase BTC, ETH Across 1,200 Newsstands</a></h3>
                                                         </div>
                                                      </div>
                                                      <div class="entry-content"></div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>
                                          <div class="col-xs-12 col-sm-4 col-md-4 hidden-description hidden-meta">
                                             <article class="post-item post-grid disss clearfix post-1430 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-blockchain post_format-post-format-video">
                                                <div class="article-tran hover-share-item">
                                                   <div class="post-image">
                                                      <a href="/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-2/" class="bgr-item"></a>
                                                      <a href="2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-2/">
                                                      <img class="lazy" src="http://bitther.nanoagency.co/wp-content/uploads/2018/04/27-510x328.png" data-lazy="http://bitther.nanoagency.co/wp-content/uploads/2018/04/27-510x328.png" alt="post-image" style="display: inline;">
                                                      </a>
                                                      <span class="post-cat ">                <a href="/category/bitcoin/" title="">Bitcoin</a>
                                                      </span>
                                                   </div>
                                                   <div class="article-content   hidden-view hidden-comments">
                                                      <div class="article-meta clearfix">
                                                         <span class="byline author-title"><span class="ti-minus"></span><span class="by">By</span><span class="author vcard"><span class="screen-reader-text">Author </span><a class="url fn n" href="/author/admin/">Daigo</a></span></span><span class="posted-on"><i class="icon ti-calendar" aria-hidden="true"></i><span class="screen-reader-text">Posted on </span><a href="/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-2/" rel="bookmark"><time class="entry-date published" datetime="2018-04-15T17:24:37+00:00">April 15, 2018</time><time class="updated" datetime="2018-07-02T07:27:34+00:00">July 2, 2018</time></a></span>
                                                         <div class="entry-meta-right">
                                                            <div class="total-view">
                                                               <i class="ti-eye"></i> 164
                                                            </div>
                                                            <span class="comments-link">
                                                            <a href="/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-2/#respond" class="text-comment"><i class="icon ti-comment-alt" aria-hidden="true"></i> 0</a>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="entry-header clearfix">
                                                         <div class="entry-header-title">
                                                            <h3 class="entry-title"><a href="/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-2/">Goldman Sachs Keeps Criticizing Bitcoin, But There Are Certain Ties</a></h3>
                                                         </div>
                                                      </div>
                                                      <div class="entry-content"></div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>
                                          <div class="col-xs-12 col-sm-4 col-md-4 hidden-description hidden-meta">
                                             <article class="post-item post-grid disss clearfix post-1414 post type-post status-publish format-standard has-post-thumbnail hentry category-investment tag-scams">
                                                <div class="article-tran hover-share-item">
                                                   <div class="post-image">
                                                      <a href="/2018/04/15/australians-can-now-purchase-btc-eth-across-1200-newsstands/" class="bgr-item"></a>
                                                      <a href="2018/04/15/australians-can-now-purchase-btc-eth-across-1200-newsstands/">
                                                      <img class="lazy" src="http://bitther.nanoagency.co/wp-content/uploads/2018/04/15-510x328.jpg" data-lazy="http://bitther.nanoagency.co/wp-content/uploads/2018/04/15-510x328.jpg" alt="post-image" style="display: inline;">
                                                      </a>
                                                      <span class="post-cat ">                <a href="/category/investment/" title="">Investment</a>
                                                      </span>
                                                   </div>
                                                   <div class="article-content   hidden-view hidden-comments">
                                                      <div class="article-meta clearfix">
                                                         <span class="byline author-title"><span class="ti-minus"></span><span class="by">By</span><span class="author vcard"><span class="screen-reader-text">Author </span><a class="url fn n" href="/author/admin/">Daigo</a></span></span><span class="posted-on"><i class="icon ti-calendar" aria-hidden="true"></i><span class="screen-reader-text">Posted on </span><a href="/2018/04/15/australians-can-now-purchase-btc-eth-across-1200-newsstands/" rel="bookmark"><time class="entry-date published" datetime="2018-04-15T17:11:36+00:00">April 15, 2018</time><time class="updated" datetime="2018-05-25T19:21:08+00:00">May 25, 2018</time></a></span>
                                                         <div class="entry-meta-right">
                                                            <div class="total-view">
                                                               <i class="ti-eye"></i> 114
                                                            </div>
                                                            <span class="comments-link">
                                                            <a href="/2018/04/15/australians-can-now-purchase-btc-eth-across-1200-newsstands/#respond" class="text-comment"><i class="icon ti-comment-alt" aria-hidden="true"></i> 0</a>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="entry-header clearfix">
                                                         <div class="entry-header-title">
                                                            <h3 class="entry-title"><a href="/2018/04/15/australians-can-now-purchase-btc-eth-across-1200-newsstands/">Australians Can Now Purchase BTC, ETH Across 1,200 Newsstands</a></h3>
                                                         </div>
                                                      </div>
                                                      <div class="entry-content"></div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>

                     </div>




                     <div  class="vc_row-full-width vc_clearfix"></div>
                     
                  </div>
               </div>
            </div>
         </div>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>

<?php $asset_path = get_template_directory_uri(); ?>

      <section class="stats">
         <div class="wrapper">
            <?php $numeros = get_field('indeci_numeros', 2); ?>
                <?php if($numeros): ?>
                  <?php foreach ($numeros as $k => $v): ?>
                     <span>
                        <b><?php echo $v['numero']; ?></b>
                        <small><?php echo $v['titulo_numero']; ?></small>
                     </span>
                  <?php endforeach ?>
            <?php endif; ?>
         </div>
      </section>
<!--     <section class="links wrapper">
         <h2>Enlaces de Interés</h2>
         <?php $enlaces = get_field('enlaces_interes', 2); ?>
         <div class="slider">
             <?php if($enlaces): ?>
                  <?php foreach ($enlaces as $k => $v): ?>
                  <div>
                     <a href="<?php echo $v['enlace_interes']; ?>" target="_blank">
                        <img src="<?php echo $v['image_interes']; ?>">
                     </a>
                  </div>
               <?php endforeach ?>
            <?php endif; ?>
         </div>
      </section> -->
      
<footer id="na-footer">
    <div class="na-footer-main">
        <div class="footer-top">
            <div class="container">
                <div class="container-inner">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div id="contact_info-3" class="widget first widget_contact_info">
                                <div class="contact-inner clearfix">
                                    <img class="about-image" src="http://indeci2.apprende.com.pe/wp-content/themes/indeci/assets/img/logo-indeci.png" alt="img">
                                    <p class="description">
                                        "Un país preparado y resiliente ante emergencias y desastres."
                                    </p>
                                    <ul class="contact-info">
                                        <li>
                                            <b>Dirección: </b><br>
                                            <span>Calle Ricardo Angulo 694, San Isidro 15036</span></li>
                                        <li>
                                            <b>Teléfonos: </b>
                                            <span>999 999 999</span></li>
                                        <li>
                                            <b>Email: </b>
                                            <a href="mailto:contacto@indeci.gob.pe"><span>contacto@indeci.gob.pe</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="bitther_social-3" class="widget first bitther_social">
                                <div class="bitther-social-icon clearfix">
                                 <a href="#" target="_blank" title="ion-social-facebook" class="ion-social-facebook"><span class="ti-facebook"></span></a>
                                 <a href="#" target="_blank" title="ion-social-twitter" class="ion-social-twitter"><span class="ti-twitter-alt"></span></a><a href="#" target="_blank" title="ion-social-instagram" class="ion-social-instagram"><span class="ti-instagram"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div id="bitther_post-10" class="widget first widget_bitther_post">
                                <aside class="widget_tabs_post">
                                    <h2 class="widgettitle">QUIENES SOMOS</h2>
                                    <ul>
                                       <li><a href="">Link #1</a></li>
                                       <li><a href="">Link #2</a></li>
                                       <li><a href="">Link #3</a></li>
                                        <li><a href="">Link #1</a></li>
                                        <li><a href="">Link #2</a></li>
                                        <li><a href="">Link #3</a></li>
                                    </ul>
                                </aside>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div id="twitter-4" class="widget first widget_twitter">
                                <h2 class="widgettitle">QUE HACEMOS</h2>
                                <ul>
                                 <li><a href="">Link #1</a></li>
                                 <li><a href="">Link #2</a></li>
                                 <li><a href="">Link #3</a></li>
                                    <li><a href="">Link #1</a></li>
                                        <li><a href="">Link #2</a></li>
                                        <li><a href="">Link #3</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="coppy-right">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <small><span>Copyrights © 2018 INDECI. All Rights Reserved.</span></small>
                </div>
            </div>
        </div>
    </div>
</footer>

   </section><!-- .site-content-contain -->
</div><!-- #page -->
<!-- <script type="text/javascript" src="<?php echo $asset_path; ?>/assets/js/plugins.js"></script> -->
<?php //wp_footer(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script type="text/javascript" src="<?php echo $asset_path; ?>/assets/js/main.js"></script>
<script>
    $(function() {
        $('.acciones-slider').slick();
        $('.box-article a').each(function( ) {
            texto = $(this).text();
            if(texto == "Ver más" || texto == "ver más" || texto == "VER MÁS" || texto == "VER" || texto == "ver"){
                $(this).addClass('link-vermas');
            }
        });
    });
</script>
</body>
</html>
