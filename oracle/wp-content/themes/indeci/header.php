<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php $asset_path = get_template_directory_uri(); ?>
	<link rel="shortcut icon" href="<?php echo $asset_path; ?>/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/components.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/plugins.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/main.css">
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Bhaina|Roboto" rel="stylesheet">
	<?php wp_head(); ?>
	<style type="text/css"> #wpadminbar{ display: none !important; } html{    margin-top: 0 !important; } </style>
	<!-- Add -->
	<link rel="stylesheet" href="http://kenwheeler.github.io/slick/slick/slick.css">
	<link rel="stylesheet" href="<?php echo $asset_path; ?>/assets/custom.css" />
	<!-- Fin -->
	<!-- FACEBOOK -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.0&appId=218706935288983&autoLogAppEvents=1';
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>


<body <?php body_class( array( "page-template-default", "page", "wpb-js-composer","js-comp-ver-5.5.2","vc_responsive" ) ); ?> >
	<div id="page" class=" site">
        <div  class="canvas-overlay"></div>

		<header id="masthead" class=" site-header header-left header-left2 style_white">

	        <div id="bitther-header">
	           <div class="header-topbar">
				   <div id="bitther-top-navbar" class="top-navbar wrapper">
				      <div class="container">
						<a href="javascript:;" class="mobile-top"><i class="fas fa-chevron-down"></i></a>
				         <div class="row row-top-menu">
				            <div class="topbar-left col-xs-12 col-sm-8 col-md-7">
								<a href="/mapa-de-sitio/">Mapa Sitio</a>
								<a href="http://sinpad.indeci.gob.pe/LibroReclamaciones/Views/Inicio.aspx" target="_blank">Libro de reclamaciones</a>
								<a href="http://sinadeci.indeci.gob.pe/Intranet1/Login.aspx" target="_blank">Intranet</a>
								<a href="https://mail.indeci.gob.pe" target="_blank">Webmail</a>
								<!-- <a href="/trabaja-con-nosotros/">Trabaja con Nosotros</a> -->
								<a href="/contactenos/" class="link-high">Contáctenos</a>
				            </div>
				            <div class="topbar-right hidden-xs col-sm-4 col-md-2 clearfix social-top">
				               <div id="bitther_social-2" class="widget first bitther_social">
				                  <div class="bitther-social-icon clearfix">
				                  	<a href="#" target="_blank"  class="ion-social-facebook"><span class="ti-facebook"></span></a>
				                  	<a href="#" target="_blank" class="ion-social-twitter"><span class="ti-twitter-alt"></span></a>
				                  	<a href="#" target="_blank" class="ion-social-youtube"><i class="fab fa-youtube"></i></a>
				                  </div>
				               </div>
				            </div>
				            <div class="topbar-right hidden-xs col-md-3 text-right info-top">
				            	<div class="">
					                 <div class="d-inblock mr-1"><a href="">
					                 	<img src="<?php echo $asset_path; ?>/assets/img/pte.png" alt="" width="35">
					                 </a></div>
					                 <div class="d-inblock"><a href="">
					                 	<img src="<?php echo $asset_path; ?>/assets/img/ministerio-defensa.png" alt="" width="180">
					                 </a></div>
				              	</div>
				            </div>
				         </div>
				      </div>
				   </div>
				</div>
	           <div class="header-content-logo ">
	           			<a href="javascript:;" class="mobile-menu"><i class="fas fa-bars"></i></a>
	           		<div class="wrapper">
		              
		              <div class="site-logo" id="logo">
		                 <a href="/" rel="home"><img src="<?php echo $asset_path; ?>/assets/img/logo-indeci.png" alt="logo"></a>
		              </div>

		              <div class="main-menu">
						<div id="na-menu-primary" class="nav-menu clearfix">
		                    <nav class="text-center na-menu-primary clearfix">
		                       <?php wp_nav_menu( [
		                       		'menu' => 'menu-principal', 
		                       		'menu_class' => 'nav nav-tabs nav navbar-nav na-menu mega-menu'] ) ?>
		                    </nav>
		                 </div>
		              </div>

	           		</div>

	           </div>
	           <div class="header-content-right hidden-md hidden-lg">
	              <div class="searchform-mini searchform-moblie hidden-md hidden-lg">
	                 <button class="btn-mini-search"><i class="ti-search"></i></button>
	              </div>
	              <div class="searchform-wrap search-transition-wrap bitther-hidden">
	                 <div class="search-transition-inner">
	                    <form method="get" class="searchform" action="/">
	                       <div class="input-group">
	                          <input type="text" class="form-control" placeholder="Search ... " value="" name="s">
	                          <span class="input-group-btn">
	                          <button class="btn btn-primary"><i class="ti-search"></i></button>
	                          </span>
	                       </div>
	                    </form>
	                    <button class="btn-mini-close pull-right"><i class="ti-close"></i></button>
	                 </div>
	              </div>
	           </div>
	        </div>
	    </header>
	    <?php $styles = [ 'amarilla' => 'yellow', 'azul' => 'blue', 'naranja' => 'orange', 'verde' => 'green', 'roja' => 'red' ]; ?>
		
			<div class="">

				<div class="alerts alerts-slick">
				<?php 
					$alertas = get_posts([
					  'post_type' => 'alertas',
					  'post_status' => 'publish',
					  'numberposts' => 6
					]);
				 ?>

				<?php foreach ( $alertas as $post ) : setup_postdata( $post ); ?>

					<?php 
						$terms = get_the_terms( $post->ID, 'tipo' );
						$term = array_shift( $terms );
						$icono = get_field('icono', $term);
					?>
					<div>
						<article class="bg-<?php echo $styles[$term->slug];  ?>" style="<?php echo !empty($icono) ? "background-image: url('".$icono."');":""; ?>">
							<h2>
								<?php $sus = strlen(get_the_title()) > 180 ? "...":""; ?>
								<a href="<?php the_permalink(); ?>"><?php echo mb_strimwidth(get_the_title(), 0, 180, $sus); ?></a>
							</h2>
						</article>
					</div>

				<?php endforeach; 
				wp_reset_postdata();?>
				</div>
			</div>