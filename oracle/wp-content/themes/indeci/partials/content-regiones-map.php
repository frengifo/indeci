		<div class="item">
					
			<?php 
				$regiones = get_posts([
				  'post_type' => 'regiones',
				  'post_status' => 'publish',
				  'numberposts' => -1
				]);
			 ?>

			<?php foreach ( $regiones as $post ) : setup_postdata( $post ); ?>
				
				<div class="info-<?php the_field('region') ?>">
					<h2><?php the_title(); ?></h2>
					<p><?php the_field('responsable') ?></p>
					<p>
						<?php the_field('direccion') ?><br>
						Teléfono: <?php the_field('telefono') ?><br>
						Celular: <?php the_field('celular') ?><br>
						e-mail: <?php the_field('email') ?><br>
					</p>
					<h3>ALMACENES:</h3>
					<div class="storage">
						<?php the_field('almacenes') ?>
					</div>
					
					<?php $sections = get_field('type_content'); ?>

					<?php foreach ($sections as $k => $section): ?>
						
						<section class="aditional-info">
							<h3><?php echo $section['section_title'] ?></h3>

							<div class="loop-attach">

								<?php foreach ($section['files_gallery'] as $p => $files_gallery): ?>
										
									<?php if ( $files_gallery['acf_fc_layout'] == 'files_type' ): ?>
											
											<ul>
												
												<?php foreach ($files_gallery['attach_files'] as $q => $attach): ?>
													<li>
														<a href="<?php echo $attach['file_source'] ?>" download>
															<?php echo $attach['name_file'] ?> <span class="ti-download"></span>
														</a>
													</li>
													
												<?php endforeach ?>	

											</ul>

									<?php endif ?>

									<?php if ( $files_gallery['acf_fc_layout'] == 'gallery' ): ?>

										<?php foreach ($files_gallery['gallery_pictures'] as $q => $image): ?>
												<a href="<?php echo $image['url'] ?>" data-fancybox="gallery">
													<img src="<?php echo $image['sizes']['medium'] ?>">
												</a>
										<?php endforeach ?>	

									<?php endif ?>	

								<?php endforeach ?>
							
							</div>

						</section>

					<?php endforeach ?>

				</div>

			<?php endforeach; 
			wp_reset_postdata();?>
		</div>
		<div class="more">
			
		</div>
    