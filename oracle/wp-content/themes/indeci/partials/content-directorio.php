<?php $organos_primer_nivel = get_field('organos_primer_nivel'); ?>
<h3 class="dir-msj">Conoce al equipo INDECI <br> ¡Estamos para servirte!</h3>
<div class="btns-dir">
	<a href="javascript:;" class="active" data-display="#directorio">Sede Central</a>
	<a href="javascript:;" data-display="#desconcentradas">Direcciones Desconcentradas</a>
</div>

<article class="directorio" id="directorio">
	
	<?php foreach ($organos_primer_nivel as $k => $v): ?>
				
		<div class="block">
			<div class="title">
				<?php echo $v['nombre_organo']; ?><br>
			</div>
			<?php foreach ($v['oficinas'] as $j => $ofi): ?>
				
				<?php foreach ($ofi['personal'] as $i => $personal): ?>
					
					<div class="item">
						<h4><?php echo strtoupper($ofi['nombre_oficina']); ?></h4>
						<strong>
							<?php echo $personal['nombre']; ?> <br> <?php echo $personal['cargo']; ?>
						</strong>
						<p>
							<?php echo $personal['direccion']; ?> <br>
							Teléfono: <?php echo $personal['telefono']; ?><br>
							E-mail: <?php echo $personal['email']; ?> 
						</p>
					</div>

				<?php endforeach ?>

			<?php endforeach ?>

		</div>

	<?php endforeach ?>		


</article>


<?php $direcciones = get_field('direcciones'); ?>
<article class="directorio desconcentradas" id="desconcentradas" style="display:none;">
	
	<div class="block conflex">
	<!-- <div class="row-eq-height"> -->
		<?php foreach ($direcciones as $k => $v): ?>
					
				<div class="item">
				<!-- <div class="col-md-6"> -->
					<div class="title">
						<?php echo $v['direccion_desconcentrada']; ?>
					</div>
					<strong>
						<?php echo $v['nombre_desc']; ?> <br> <?php echo $v['cargo_desc']; ?>
					</strong>
					<p>
						<?php echo $v['direccion_desc']; ?> <br>
						Teléfono: <?php echo $v['telefono_desc']; ?><br>
						Celular: <?php echo $v['celular_desc']; ?><br>
						E-mail: <?php echo $v['email_desc']; ?> 
					</p>
				</div>
		<?php endforeach ?>	
	</div>

</article>