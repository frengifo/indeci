<?php $rows = get_field('contenido_flex'); ?>
<?php $styles = ['blue','orange','green']; ?>
<?php $asset_path = get_template_directory_uri(); ?>
<?php if ( $rows ): ?>
	<?php $par = 2; ?>
	<?php foreach ($rows as $k => $nivel): ?>
		<div class="table-block <?php echo isset($styles[$k]) ? $styles[$k]:$styles[0] ?>-table">
			<h3 style="margin-bottom: 10px;"><?php echo $nivel['titulo']; ?></h3>
			<?php 
				$fondo = ($par % 2 == 0) ? "bg-azul" : "bg-orange"; 
				$par += 1;
			?>
			<?php //foreach ($nivel['documents'] as $j => $subnivel): ?>
				<table>
					<thead class="<?php echo $fondo; ?>">
						<tr>
							<th width="228">Norma</th>
							<th>Sumilla</th>
							<th width="112">Fecha <br>Publicación</th>
							<th width="60">Archivo</th>
						</tr>
					</thead>
					<tbody>
						
						<?php foreach ($nivel['documents'] as $i => $row): ?>
							
							<tr>
								<td width="228"><?php echo $row['norma']; ?></td>
								<td><?php echo $row['sumilla']; ?></td>
								<td width="112"><?php echo $row['fecha_publicacion']; ?></td>
								<td width="60"><a href="<?php echo $row['archivo_pdf']; ?>" target="_blank">
									<img src="<?php echo $asset_path; ?>/assets/img/icon-pdf.png" width="38">
								</a></td>
							</tr>

						<?php endforeach ?>

					</tbody>
				</table>

			<?php //endforeach ?>

		</div>

	<?php endforeach ?>

<?php endif ?>	