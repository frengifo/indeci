<?php $asset_path = get_template_directory_uri(); ?>
<div class="article-custom text-center acercade">

	<nav class="about-us">
		<h2><a href="javascript:;" class="active" data-display=".vision">Visión</a></h2>
		<h2><a href="javascript:;" data-display=".mision">Misión</a></h2>
		<h2><a href="javascript:;" data-display=".objetivos">Objetivos</a></h2>
	</nav>

	<div class="display">
		<div class="vision">
			<!-- <h2>Visión</h2> -->
			<?php 
			$image = get_field('imagen_vision');
			if( !empty($image) ): ?>
				<div>
					<img src="<?php echo $image['sizes']['large']; ?>" alt="Indeci Visión" />
				</div>
			<?php endif; ?>
			<div>
				<?php the_field('contenido_vision') ?>
			</div>
		</div>
		<div class="mision">
			<!-- <h2>Misión</h2> -->
			<?php 
			$image = get_field('imagen_mision');
			if( !empty($image) ): ?>
				<div>
					<img src="<?php echo $image['sizes']['large']; ?>" alt="Indeci Misión" />
				</div>
			<?php endif; ?>
			<div>
				<?php the_field('contenido_mision') ?>
			</div>
		</div>					
		<div class="objetivos">
			<!-- <h2>Objetivos</h2> -->
			<?php $fondos = array('1' => 'bg-azul', '2' => 'bg-orange', '3' => 'bg-morado'); $it = 0; ?>
			<?php if (get_field('generales')): ?>
				
				<?php foreach (get_field('generales') as $k => $v): ?>
					<?php $it += 1; ?>
		
					<div class="goal <?php echo isset($styles[$k]) ? $styles[$k]:$styles[0] ?>-style <?php echo $fondos[$it]; ?>">
						<div>
							<a href="#"><?php echo ($k+1) ?></a> <span><?php echo $v['titulo_generales'] ?></span>
						</div>
						<div>
							<ul>
								<?php foreach ($v['especificos'] as $j => $ob): ?>
									
									<li><?php echo $ob['titulo_especificos']; ?></li>

								<?php endforeach ?>

							</ul>
						</div>
					</div>

				<?php endforeach ?>

			<?php endif ?>

		</div>
	</div>
	
</div>