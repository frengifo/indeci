<?php $current_term_title = single_term_title("", false); ?>
<?php  	$terms_post= null; $term_post = null;  ?>
<?php
	$args =  array(
	  'hide_empty' => false,
	  'orderby'    => 'name',
	  'order'      => 'ASC',
	);
	
	$current_term = get_queried_object();

	$return_section = ['categoria' => 'galerias', 'categoria-video' => 'videos' ];

	if ( $current_term->taxonomy ) {
		
		$section_slug = !isset($section_slug) ? $return_section[ get_taxonomy( $current_term->taxonomy )->rewrite['slug'] ]: $section_slug;

		$tax_slug = $current_term->taxonomy;

	}else{
		
		$section_slug = isset($section_slug) ? $section_slug:'galerias';
		$tax_slug = isset($tax_slug) ? $tax_slug:'categoria';

	}

	$terms = get_terms( $tax_slug , $args );
?>

<div class="col-xs-12">

	<ul class="sub-menu-category">
	<li><a href="/<?php echo $section_slug ?>" >Todos:</a></li>
		<?php
		foreach( $terms as $term ) { ?>

			<?php $cls_active = $term->name == $current_term_title  ? 'active':''; ?>
			
			<?php 
				if ( is_single( get_the_ID() ) ) {
		
					$terms_post = get_the_terms( get_the_ID(), $tax_slug );
			    	
			    	if ( !empty( $terms_post ) ){ $term_post = array_shift( $terms_post ); }

			    	$cls_active = $term_post->slug == $term->slug ? "active":'';
				}
			?>

     		<li class="page_item page-item-942  <?php echo $cls_active; ?>">
     			<a href="<?php echo get_term_link( $term ); ?>" class="<?php echo $cls_active; ?>"> <?php echo $term->name; ?> </a>
     		</li>
	<?php } ?>
	</ul>
	
</div>
