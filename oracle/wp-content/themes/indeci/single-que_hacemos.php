<?php
get_header(); 

$asset_path = get_template_directory_uri();
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php 
$terms = get_the_terms( $post->ID, 'areas' );
if ( !empty( $terms ) ){
    $term = array_shift( $terms );
    $term_name = $term->name;
    $term_slug = $term->slug;
}
 ?>
<div class="banner-featured" style="background-image: url('<?php echo $asset_path ?>/assets/img/banner-indeci.png')">
   <div class="container"> <h1><?php the_title(); ?></h1> </div>
</div>
<div class="crumbs">
  <div class="container">
    <?php custom_breadcrumb(); ?>
  </div>
</div>
<?php
// if ( function_exists('yoast_breadcrumb') ) {
//   yoast_breadcrumb( '</p><p id="breadcrumbs">','</p><p>' );
// }
?>

<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content" role="main" style="transform: none;">
      <div class="container" style="transform: none;">

         <div class="row single-main-content single-1" style="transform: none;">
            <div id="archive-sidebarsss" class="sidebar sidebar-left col-sx-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar single-sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
               <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 0px; left: 332.333px;">
                  <aside id="categories-8" class="widget widget_categories">
                     <h2 class="widgettitle <?php echo $term_slug; ?>"><?php echo $term_name; ?></h2>
                     <?php if ( is_nav_menu( "menu-area-".$term_slug) ): ?>
                        <?php wp_nav_menu( ['menu' => 'menu-area-'.$term_slug ] ); ?>
                     <?php endif ?>
                  </aside>
               </div>
            </div>
            <div class=" content-padding-left main-content content-left col-sx-12 col-sm-12 col-md-9 col-lg-9">
               <div class="content-inner">
                  <div class="box-article">
                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
                        <h1><?php the_title(); ?></h1>
                        <div class="article">
                           <?php 
                           // QUIENES SOMOS
                           if( have_rows('media') ):
                               while ( have_rows('media') ) : the_row();
                                  if( get_row_layout() == 'video_presentacion' ):
                                    ?>
                                       <div class="video-indeci mt-1">
                                          <?php the_sub_field('video'); ?>
                                       </div>
                                    <?php 
                                  endif;
                                    ?>
                                    <div class="media-desc">
                                      <?php the_sub_field('descripcion_video'); ?>
                                    </div>
                                    <div class="box-vermas mt-1 text-right">
                                      <a href="<?php the_sub_field('link_mas_informacion'); ?>" target="_blank" class="link-vermas">Ver más</a>
                                    </div>
                                    <?php 
                               endwhile;
                           endif;
                           ?>

                           <div class="content-text mt-2">
                              <?php the_content() ?>
                           </div>

                           <?php 
                           // ACCIONES 
                           if( have_rows('galerias_acciones') ):
                               while ( have_rows('galerias_acciones') ) : the_row();
                                    ?>
                                       <div class="row mt-2">
                                          <div class="col-md-6 mt-1">
                                             <?php 
                                                $images = get_sub_field('galeria_img');
                                                $size = 'medium';
                                                if( $images ): ?>
                                                    <ul class="acciones-slider">
                                                        <?php foreach( $images as $image ): ?>
                                                            <li class="item">
                                                               <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                <?php endif;
                                             ?>
                                          </div>
                                          <div class="col-md-6 mt-1">
                                             <h4><?php the_sub_field('titulo_acciones'); ?></h4>
                                             <p><?php the_sub_field('descripcion_acciones'); ?></p>
                                             <a href="<?php the_sub_field('enlace_acciones'); ?>" class="link-azul-wide">Ver más</a>
                                          </div>
                                       </div>
                                    <?php 
                               endwhile;
                           endif;
                           ?>

                           <?php 
                           // GALERIA IMAGENES
                           if( have_rows('media') ):
                               while ( have_rows('media') ) : the_row();
                                   if( get_row_layout() == 'galeria_de_fotos' ):
                                    ?>
                                       <div class="mt-1">
                                          <?php 
                                             $items = get_sub_field('gallery');
                                             if( $items ): ?>
                                                 <ul class="acciones-slider">
                                                     <?php foreach( $items as $item ): ?>
                                                         <li class="item">
                                                            <div class="video-indeci">
                                                              <img src="<?php echo $item['image']; ?>" alt="">
                                                            </div>
                                                            <div class="item-resena">
                                                               <p><strong><?php echo $item['titulo']; ?></strong></p>
                                                               <span><?php echo $item['descripcion']; ?></span>
                                                            </div>
                                                         </li>
                                                     <?php endforeach; ?>
                                                 </ul>
                                             <?php endif;
                                          ?>
                                       </div>
                                    <?php 
                                   endif;
                               endwhile;
                           endif;

                          // Mas Galerias IMG
                          if ( has_post_format( 'gallery' )) { ?>
                            <?php 
                             $query = new WP_Query( array(
                                'post_type' => 'que_hacemos',
                                'posts_per_page' => '8',
                                'orderby' => 'date',
                                'order' => 'desc',
                                'tax_query' => array(
                                  array(
                                    'taxonomy' => 'post_format',
                                    'field'    => 'slug',
                                    'terms' => array( 'post-format-gallery' )
                                  )),
                            ));

                            if ( $query->have_posts() ) { ?>
                              <div class="mt-4">
                                <h3>GALERÍAS RECIENTES</h3>
                              </div>
                              <div class="row mt-2 acc-directos-list">
                                <?php 
                                while ( $query->have_posts() ) : $query->the_post();  ?>
                                    <div class="col-xs-6 col-sm-2 col-md-4">
                                      <a href="<?php the_permalink(); ?>">
                                         <?php the_post_thumbnail(); ?>
                                      </a>
                                      <p class="mt-1"><?php the_title(); ?></p>
                                      <a href="<?php the_permalink(); ?>" class="link-azul-wide mt-1">Ver</a>
                                    </div>
                              <?php 
                                endwhile; ?>
                              </div>
                              <?php 
                            }

                          }

                          // Mas Galerias VIDEO
                          if ( has_post_format( 'video' )) { ?>
                            <?php 
                             $query = new WP_Query( array(
                                'post_type' => 'que_hacemos',
                                'posts_per_page' => '8',
                                'orderby' => 'date',
                                'order' => 'desc',
                                'tax_query' => array(
                                  array(
                                    'taxonomy' => 'post_format',
                                    'field'    => 'slug',
                                    'terms' => array( 'post-format-video' )
                                  )),
                            ));

                            if ( $query->have_posts() ) { ?>
                              <div class="mt-4">
                                <h3>VIDEOS RECIENTES</h3>
                              </div>
                              <div class="row mt-2 acc-directos-list">
                                <?php 
                                while ( $query->have_posts() ) : $query->the_post();  ?>
                                    <div class="col-xs-6 col-sm-2 col-md-4">
                                      <a href="<?php the_permalink(); ?>">
                                         <?php the_post_thumbnail(); ?>
                                      </a>
                                      <p class="mt-1"><?php the_title(); ?></p>
                                      <a href="<?php the_permalink(); ?>" class="link-azul-wide mt-1">Ver</a>
                                    </div>
                              <?php 
                                endwhile; ?>
                              </div>
                              <?php 
                            }

                          }
                          ?>

                           <?php 
                           // ACCESOS DIRECTOS
                           if( have_rows('media') ):
                               while ( have_rows('media') ) : the_row();
                                   if( get_row_layout() == 'enlaces' ):
                                    ?>
                                       <div class="row mt-2 acc-directos-list">
                                          <?php 
                                             $items = get_sub_field('accesos_directos');
                                             if( $items ): ?>
                                               <?php foreach( $items as $item ): ?>
                                                   <div class="col-sm-3 col-md-4 mt-3">
                                                      <a href="<?php echo $item['link_enlace']; ?>">
                                                         <img src="<?php echo $item['imagen_enlace']; ?>" alt="">
                                                      </a>
                                                      <p class="mt-1"><?php echo $item['titulo_enlace']; ?></p>
                                                      <a href="<?php echo $item['link_enlace']; ?>" class="link-azul-wide mt-1" target="_blank">Ver</a>
                                                   </div>
                                               <?php endforeach; ?>
                                             <?php endif;
                                          ?>
                                       </div>
                                    <?php 
                                   endif;
                               endwhile;
                           endif;
                           ?>

                           <?php 
                           // MAS INFORMACION
                           if( have_rows('media') ):
                               while ( have_rows('media') ) : the_row();
                                   if( get_row_layout() == 'mas_informacion' ):
                                    ?>
                                       <div class="row mt-2 masinfo-list">
                                          <?php 
                                             $items = get_sub_field('archivos_informacion');
                                             if( $items ): ?>
                                               <?php foreach( $items as $item ): ?>
                                                   <div class="col-sm-3 col-md-4 mt-3">
                                                      <a href="<?php echo $item['archivo_info']; ?>" target="_blank">
                                                         <img src="<?php echo $item['imagen_info']; ?>" alt="">
                                                      </a>
                                                      <p class="mt-1"><?php echo $item['titulo_info']; ?></p>
                                                      <a href="<?php echo $item['archivo_info']; ?>" target="_blank" class="link-azul-wide mt-1">Ver</a>
                                                   </div>
                                               <?php endforeach; ?>
                                             <?php endif;
                                          ?>
                                       </div>
                                    <?php 
                                   endif;
                               endwhile;
                           endif;
                           ?>

                        </div>

                     </article>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>
<?php get_footer();
