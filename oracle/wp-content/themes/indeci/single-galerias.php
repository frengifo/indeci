<?php
get_header(); 

$asset_path = get_template_directory_uri();


?>


<div class="crumbs">
  <div class="wrapper">
    <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
  </div>
</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


<?php 
	$terms = get_the_terms( get_the_ID(), 'categoria' );
    if ( !empty( $terms ) ){ $term = array_shift( $terms ); }

?>
<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content " role="main" style="transform: none;">
   		<div class="wrapper">
   			<div class="box-title clearfix">
               <h2 class="title-left"><?php echo $term->name; ?></h2>
            </div>
           
					<?php get_template_part( 'partials/content', 'category-list' ) ?>
            		
           
   		</div>
      <div class="full-gallery-single" style="transform: none;">

	        <div class=" wrapper single-main-content single-1" style="transform: none;">
	            
	            <?php $current_term_title = single_term_title("", false); ?>
	            <div class="row">
		            <div class=" col-sx-12 col-sm-12 ">
		               <div class="content-inner">
		                  <div class="box-article">
		                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
		                        
							    <div class="row vid-lista box-blog archive-blog row large-vertical clearfix active">
		                        

							    		<h1 class="title-detail-section"><?php the_title() ?></h1>
											
										
				                        	<div class="gallery-slider first_gallery">


					                        		
					                            <?php  
					                            // GALERIA FOTO GALERIAS
								   
							                     $items = get_field('imagenes_fotogaleria');
							                     if( $items ): ?>
							                         
						                             <?php foreach( $items as $item ): ?>
						                                 
						                                 <div class="col-xs-12 col-sm-4 col-md-4 hidden-description hidden-meta">
														    <article class="post-item post-grid">
														        <div class="article-tran hover-share-item">
														           <div class="post-image">
														              <a href="javascript:;" class="bg-img" style="background-image: url('<?php echo $item['foto']; ?>')"> <img src="<?php echo $item['foto']; ?>" alt=""> </a>
														           </div>
														           <div class="article-content   hidden-view hidden-comments">
														              <div class="entry-header clearfix">
														                 <div class="entry-header-title">
														                    <h3 class="entry-title"><a href="javascript:;"><?php echo $item['titulo']; ?></a></h3>
														                    <p><?php echo $item['descripcion']; ?></p>
														                 </div>
														              </div>
														           </div>
														        </div>
														    </article>
														</div>

						                             <?php endforeach; ?>
							                         


							                     <?php endif; ?>
											          

				                            </div>
			
						         			
			                        
							    </div>



		                     </article>
		                  </div>
		               </div>
		            </div>
		        </div>

	        </div>

      </div>

      <div class="description-gallery ">
      	<div class="wrapper">
      		
	      	<div class="row">
	      		<div class="col-xs-12">
			      	<article>
			      		<?php the_content(); ?>
			      	</article>
	      			
	      		</div>
	      	</div>
      	</div>

      </div>
   </div>
</div>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php endif; ?>
<?php get_footer();
