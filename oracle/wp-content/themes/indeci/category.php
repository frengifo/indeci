<?php
get_header(); 

$asset_path = get_template_directory_uri();
?>

<div class="crumbs">
  <div class="wrapper">
    <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
  </div>
</div>

<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content wrapper" role="main" style="transform: none;">
      <div class="container" style="transform: none;">

         <div class="row single-main-content single-1" style="transform: none;">
            
            <?php $current_term_title = single_term_title("", false); ?>
            <?php $current_term = get_queried_object(); ?>
            <div class="row">
	            <div class=" main-content col-sx-12 col-sm-12 ">
	               <div class="content-inner">
	                  <div class="box-article">
	                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
	                        <div class="box-title clearfix">
	                           <h2 class="title-left"><?php echo $current_term_title; ?></h2>
	                        </div>
							
						    <div class=" vid-lista box-blog archive-blog  large-vertical clearfix active">
		                        
		                        <?php 

		                        	$galerias_latest_six = get_posts([
			                          // 'post_type' => 'galerias',
			                          'numberposts' => 4,
			                          'category' => $current_term->term_id,
			                        ]);
		                        	$cols = false;
		                        	$section_slug ="noticias" ;
		                        	$exclude_posts = [];
		                        ?>

	                        	<div class="slider-last first_gallery">

		                        	 <?php foreach ( $galerias_latest_six as $post ) : setup_postdata( $post ); ?>
		                        		
		                        		<?php array_push($exclude_posts, get_the_ID() ); ?>
		                        		<?php include(locate_template('partials/content-post-list-item.php')); ?>

									<?php endforeach; 
	                              	wp_reset_postdata(); ?> 
	                              	
	                            </div>

	                            <?php include(locate_template('partials/content-category-list.php')); ?>

					         	<?php 
		                        	$galerias_full = get_posts([
			                          // 'post_type' => 'galerias',
			                          'numberposts' => -1,
			                          'category' => $current_term->term_id,
			                          'exclude'  => $exclude_posts,
			                        ]);
		                        	$cols = true;

		                        	$row_count = 1;
		                        ?>

			                    <div class="lists-posts-gallery ">
			                    	<?php foreach ( $galerias_full as $post ) : setup_postdata( $post ); ?>
			                    	
				                        
				                        	
				                       		<?php include(locate_template('partials/content-post-list-item.php')); ?>

				                        
				                        <?php if ( $row_count % 3 == 0): ?>
				                        	<div class="clear"></div>
				                        <?php endif ?>
			                    
				                    <?php endforeach; 
			                        wp_reset_postdata(); ?>
	                        	</div>

						    </div>


	                     </article>
	                  </div>
	               </div>
	            </div>
	        </div>
         </div>
      </div>
   </div>
</div>

<?php get_footer();
