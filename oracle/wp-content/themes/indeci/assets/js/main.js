jQuery(document).ready(function($) {
	/*Alertas*/
	$('.alerts-slick').slick({
	  autoplay:true,
	  dots: false,
	  arrows:false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  infinite: true,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	      }
	    },
	    {
	      breakpoint: 769,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
	setTimeout( function(){
		$(".alerts").css({'opacity':1});
	}, 500);
	/*LINKS*/
	$('.links .slider-slick').slick({
	  autoplay:true,
	  dots: false,
	  arrows:false,
	  speed: 300,
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  infinite: true,
	  autoplay:true,
	  responsive: [
	  	{
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	      }
	    },
	    {
	      breakpoint: 769,
	      settings: {
	        slidesToShow: 2,
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	$('.gallery-slider, .acciones-slider').slick({
	  dots: false,
	  arrows:true,
	  speed: 300,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  infinite: false,
	  autoplay:false,
	});

	$('.recent-post-slider').slick({
	  dots: true,
	  arrows:false,
	  speed: 300,
	  slidesToShow: 2,
	  slidesToScroll: 1,
	  infinite: false,
	  autoplay:true,
	  delay:5000,
	  responsive: [
	    {
	      breakpoint: 769,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    },
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	/*Alertas*/
	$('.slider-last-videos, .slider-last').slick({
	  autoplay:false,
	  dots: false,
	  arrows:true,
	  speed: 300,
	  slidesToShow: 2,
	  slidesToScroll: 1,
	  infinite: true,
	  
	  responsive: [
	    {
	      breakpoint: 1025,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 680,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    },
	  ]
	});
	
	/*Alertas*/
	$('.notifications .first .slider').slick({
	  autoplay:true,
	  dots: true,
	  arrows:false,
	  speed: 300,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  infinite: true,
	  
	  responsive: [
	    {
	      breakpoint: 1025,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 680,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	$('.notifications .second .slider').slick({
	  dots: true,
	  arrows:false,
	  speed: 300,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  infinite: true,
	  autoplay:true,
	  
	});

	/*Bnaners*/
	$('.banners').unslider({
		autoplay: true,
		speed: 1000,
		delay:5500,
		arrows: {
			//  Unslider default behaviour
			prev: '<a class="unslider-arrow prev"><i class="fas fa-caret-left"></i></a>',
			next: '<a class="unslider-arrow next"><i class="fas fa-caret-right"></i></a>',
		}
	});

	$(".mobile-top").on('click', function(event) {
		$(this).next().slideToggle();
		event.preventDefault();
		/* Act on the event */
	});

	$(".mobile-menu").on('click', function(event) {
		
		$("nav.na-menu-primary").slideToggle();

		event.preventDefault();
		/* Act on the event */
	});

	$("footer .btn").on('click', function(event) {
		
		$(this).find("i").toggleClass('fa-arrow-up');
		$(".block").slideToggle();
		$("footer").toggleClass('open');
	});

	$(".menu-mobile, .menu-top-mobile").on('click', function(event) {
		event.preventDefault();
		controller.toggle( $(this).data("open") );
	});

	$(".menu-close").on('click', function(event) {

		event.preventDefault();
		controller.toggle( $(this).data("close") );
	});

	/*About Us*/

	$(".about-us a").on('click', function(event) {
		var display = $(this).data("display");
		$(".about-us a").removeClass('active');
		$(this).addClass("active");

		$(".display > div").hide();
		$(".display").find(display).fadeIn();

	});

	$(".cat-item").on('click', function(event) {
		var category = $(this).data('catfilter');
		$(".box-blog").fadeOut();
		$.post(adm_url, {action: 'get_posts_by_cat', category: category}, function(data, textStatus, xhr) {
			console.log(data);
			$(".box-blog").html( data );

		}).done( function(){
			$(".box-blog").fadeIn();
		});

	});

	// jQuery.datetimepicker.setLocale('es');
	// jQuery('#datetimepicker').datetimepicker({
	//  inline:true,
	//  i18n:{
	//   es:{
	//    months:[
	//     'Enero','Febrero','Marzo','Abril',
	//     'Mayo','Junio','Julio','Agosto',
	//     'Septiembre','Octubre','Noviembre','Diciembre',
	//    ],
	//    dayOfWeek:[
	//     "Do", "Lu", "Ma", "Mi", 
	//     "Ju", "Vi", "Sa",
	//    ]
	//   }
	//  },
	//  timepicker:false,
	//  format:'d.m.Y'
	// });

	$(".btn-aside-nav").on('click', function(event) {
		$(this).find("i").toggleClass('fa-chevron-up');
		$(this).parent().find("ul.menu").slideToggle();

	});

	// MAPA
    if ($('#peru-map').length > 0) {

      $('#peru-map').JSMaps({
        map: 'peru',
        onStateClick: function(data){
          console.log(data);
          $(".item > div").hide();
          $(".item .info-"+data.abbreviation).fadeIn();
          $(".regions a").removeClass('active');
          $(".regions a."+data.abbreviation).addClass('active');
          $(document).on('click', '.jsmaps-text', function(event) {
          	
          	$(this).fadeIn();

          });
        },
         onReady: function() {
	      $('.regions a').on('click', function() {
	        $('#peru-map').trigger('stateClick', $(this).data("name"));
	      });

	      setTimeout( function(){
	      	$('.regions a:eq(0)').trigger('click');
	      }, 500);
	      
	    },
      });

    }

    $(".btn-more").on('click', function(event) {
    	$(".more").slideToggle();
    });

    $(".btns-dir a").on('click', function(event) {
    	
    	$(this).parent().find("a").removeClass('active');
    	$(this).addClass('active');

    	$(".directorio").hide();
    	$( $(this).data('display') ).fadeIn();

    });

    $(".process .btns a").on('click', function(event) {
    	
    	$(this).parent().find("a").removeClass('active');
    	$(this).addClass('active');

    	$(".process .display-info").hide();
    	$( $(this).data('display') ).fadeIn();

    });

    $(".activities .btns a").on('click', function(event) {
    	
    	$(this).parent().find("a").removeClass('active');
    	$(this).addClass('active');

    	$(".activities .display-info").hide();
    	$( $(this).data('display') ).fadeIn();

    });

    $(".tasks-box .btns a").on('click', function(event) {
    	
    	$(this).parent().find("a").removeClass('active');
    	$(this).addClass('active');

    	$(".tasks-box .display-info").hide();
    	$( $(this).data('display') ).fadeIn();

    });

    /*Links Ver más*/
    $( ".main-content a" ).each( function( index, element ){
    	var tit = $(this).text();
	    if (tit == 'Ver más' || tit == 'ver más' || tit == ' VER MÁS' || tit == 'Ver mas' || tit == 'ver mas') {
	    	$(this).addClass('link-vermas');
	    }
	});



});

  // // Initialize Slidebars
  // var controller = new slidebars();
  // controller.init();
