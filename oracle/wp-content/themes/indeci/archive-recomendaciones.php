<?php
get_header(); 

$asset_path = get_template_directory_uri();
?>

<?php 
	$slug_post_type = get_post_type();
	$post_type_labels = get_post_type_labels( get_post_type_object( $slug_post_type ) );
	$banner_large = $asset_path.'/assets/img/banner-indeci.png';
?>

<div class="banner-featured <?php echo $slug_post_type; ?>" style="background-image: url('<?php echo $banner_large ?>')">
   <div class="container"> <h1>Recomendaciones</h1> </div>
</div>

<div class="crumbs">
  <div class="wrapper">
    <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
  </div>
</div>

<div class="recent-cause-style-two">
	<div class="container">
		<?php $i = 1; ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="single-cause clearfix">
				<div class="image float-<?php echo $i % 2 == 0 ? "right":"left" ?>"><img src="<?php the_post_thumbnail_url( 'full' ); ?>" alt="Image" width="100%"></div>
				<div class="title float-left">
					<h5><a href="cause-details.html" class="tran3s"><?php the_title() ?></a></h5>
					<article>
						<?php the_excerpt(); ?>
					</article>
					<span></span>
					<div class="clearfix">
						<a href="<?php the_permalink() ?>" class="tran3s ch-p-bg-color donate">VER MÁS</a>
					</div>
				</div> <!-- /.title -->
			</div> <!-- /.single-cause -->
			<?php $i++; ?>
		<!-- post -->
		<?php endwhile; ?>
		<!-- post navigation -->
		<?php else: ?>
		<!-- no posts found -->
		<?php endif; ?>

	</div> <!-- /.container -->
</div>

<?php get_footer();
