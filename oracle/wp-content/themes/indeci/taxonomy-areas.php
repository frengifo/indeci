<?php
get_header(); 
$asset_path = get_template_directory_uri(); 

// 
$term = get_queried_object()->term_id;
$term_name = strtolower(single_term_title('',false)); 
$term_slug = get_queried_object()->slug;
?>

<div class="banner-featured" style="background-image: url('<?php echo $asset_path ?>/assets/img/banner-indeci.png')">
   <div class="container"> <h1><?php the_title(); ?></h1> </div>
</div>
<div class="crumbs">
  <div class="container">
    <?php custom_breadcrumb(); ?>
  </div>
</div>
<?php
// if ( function_exists('yoast_breadcrumb') ) {
//   yoast_breadcrumb( '</p><p id="breadcrumbs">','</p><p>' );
// }
?>

<div id="content" class="site-content" style="transform: none;">
   <div class="wrap-content" role="main" style="transform: none;">
      <div class="container" style="transform: none;">

         <div class="row single-main-content single-1" style="transform: none;">
            <div id="archive-sidebarsss" class="sidebar sidebar-left col-sx-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar single-sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
               <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 0px; left: 332.333px;">
                  <aside id="categories-8" class="widget widget_categories">
                     <h2 class="widgettitle"><?php echo $term_name; ?></h2>
                     <?php if ( is_nav_menu( "menu-area-".$term_name) ): ?>
                        <?php wp_nav_menu( ['menu' => 'menu-area-'.$term_name ] ); ?>
                     <?php endif ?>
                  </aside>
               </div>
            </div>
            <div class=" content-padding-left main-content content-left col-sx-12 col-sm-12 col-md-9 col-lg-9">
               <div class="content-inner">
                  <div class="box-article">


                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
                        <h1><?php $term_name; ?></h1>
                        <div class="article">
                           <?php $img =get_field('featured_image', $term); ?>
                              <div class="col-md-6 col-sm-6">
                                 <div class="box">
                                    <a href="<?php echo get_term_link( $term ); ?>">
                                       <img src="<?php echo $img; ?>">
                                       <strong><span><?php echo $term->name; ?></span></strong>
                                       <small><?php echo $term->description; ?></small>
                                    </a>
                                 </div>
                              </div>
                        </div>

                     </article>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php get_footer();
