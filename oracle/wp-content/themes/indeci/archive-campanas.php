<?php
get_header(); 

$asset_path = get_template_directory_uri();
?>

<?php 
  $slug_post_type = get_post_type();
  $post_type_labels = get_post_type_labels( get_post_type_object( $slug_post_type ) );
  $banner_large = $asset_path.'/assets/img/banner-indeci.png';
?>

<div class="banner-featured <?php echo $slug_post_type; ?>" style="background-image: url('<?php echo $banner_large ?>')">
   <div class="container"> <h1>Próximas Campañas</h1> </div>
</div>
<div class="crumbs">
  <div class="wrapper">
    <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
  </div>
</div>
<div id="content" class="site-content" style="transform: none;">

    <div class="container">
      <!-- <h1>Próximas campañas</h1> -->
      <?php query_posts( [ 'post_type' => 'campanas','posts_per_page' => 10, 'paged' => get_query_var( 'paged' ) ] ) ?>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="event-box style-2 list-type">
                    
            <!-- Event -->
            <div class="event">

              <div class="event-date">
                <?php $t = explode('/', get_field('schedule') ); $ft = $t[2]."-".$t[1]."-".$t[0];  ?>
                <h3 class="numb"><?php echo date('d', strtotime($ft) ); ?></h3>
                <h6 class="month"><?php echo date('M', strtotime($ft) ); ?></h6>
                <div class="day"><?php echo date('D', strtotime($ft) ); ?></div>

              </div>
              
              <div class="event-img">
                
                <a href="#"><img src="<?php the_post_thumbnail_url( 'large' ) ?>" alt="" width="370"></a>

              </div>
              
              <div class="event-body">
                
                <h2 class="event-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a> </h2>


                <div>
                  <?php the_excerpt() ?>
                </div>

                <div class="event-action flex-row align-items-center">
                      
                  <a href="<?php the_permalink() ?>" class="btn btn-style-3">Más detalle</a>
                  <div class="event-icons">
                    
                    <a href="#"><i class="licon-share2"></i></a>
                    <a href="#"><i class="licon-map-marker"></i></a>
                    <a href="#"><i class="licon-at-sign"></i></a>

                  </div>

                </div>
                

              </div>

            </div>
        </div>

        <!-- post -->
        <?php endwhile; ?>
        <!-- post navigation -->
        <ul class="pagination text-center">
            <?php echo paginate_links(); ?>
        </ul>
        <?php else: ?>
        <!-- no posts found -->
        <?php endif; ?>
    </div>
</div>

<?php get_footer();


