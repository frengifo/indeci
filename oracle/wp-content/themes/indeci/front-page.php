<?php
get_header(); ?>
<?php $asset_path = get_template_directory_uri(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>  
<section class="content">
   <section class="banners">
      <ul >
         <?php $sliders = get_field('slide'); ?>
         <?php foreach ($sliders as $k => $v): ?>
            <li style="background-image:url('<?php echo $v['imagen_version_movil'] ?>'); ">
               <div class="wrapper">
               </div>
               <a href="<?php echo $v['enlace']; ?>" target="_blank"><img src="<?php echo $v['imagen'] ?>"></a>
            </li>

         <?php endforeach ?>

      </ul>
   </section>
   <?php 
      $titulo_1 = get_field('titulo_1');
      $link_1 = get_field('enlace_1');
      $imagen = get_field('imagen_1');
   ?>

   <section class="recomienda">
      <div class="wrapper">

         <div class="col-xs-12">
            <div class="wpb_wrapper">
               <div class="recent-post_wrap slide">
                  <h2 class="widgettitle">Recomendaciones de #INDECI</h2>
                  <div class="recent-post-slider ">
                     <?php 
                        $recomendations = get_posts([
                          'post_type' => 'recomendaciones',
                          'numberposts' => -1
                        ]);
                     ?>
                     <?php foreach ( $recomendations as $post ) : setup_postdata( $post ); ?>
                        <?php 
                              $terms_post = get_the_terms( get_the_ID(), 'caso_desastres' );
                              if ( !empty( $terms_post ) ){ $term_post = array_shift( $terms_post ); }
                        ?>
                        <div class="recent-post disable_cat " tabindex="-1">
                           <article class="post-item post-sidebar">
                              <div class="article-image">
                                 <div class="post-image">
                                    <a href="" tabindex="-1">
                                    <?php the_post_thumbnail() ?>
                                    </a>
                                 </div>
                              </div>
                              <div class="article-content side-item-text ">
                                 <div class="entry-header sidebar_recent_post clearfix">
                                    <div class="entry-header-title info_post">
                                       <div class="category-view">
                                          <span class="post-cat">                <a href="javascript:;" title="" tabindex="-1"><?php echo $term_post->name ?></a>
                                          </span>
                                       </div>
                                       <h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title() ?></a></h3>
                                    </div>
                                 </div>
                              </div>
                           </article>
                        </div>
                     <?php endforeach; 
                     wp_reset_postdata(); ?> 
                  </div>
               </div>
            </div>
         </div>

      </div>
   </section>

   <section class="notifications">
      <div class="first">
         
         <!-- <div class="slider" style="background-image:url('<?php echo $imagen; ?>');">
            <div >
               <article >
                  <h3><a href="<?php echo $link_1; ?>" target="_blank"><?php echo $titulo_1; ?></a></h3>
               </article>
            </div>
         </div> -->

         <div class="slider">
            <?php echo $imagen; ?>
         </div>

      </div>
      <div class="second">
         <div class="slider">
            <?php $sliders = get_field('slides_2'); ?>
            <?php foreach ($sliders as $k => $v): ?>
               <div>
                  <article class="bg-blue">
                     <a href="<?php echo $v['enlace'] ?>" target="_blank">
                        <!-- <span><b><?php echo $v['titulo'] ?></b></span> -->
                        <img src="<?php echo $v['portada'] ?>">
                        <article class="info">
                           <h4><?php echo $v['titulo'] ?></h4>
                           <p><?php echo $v['descripcion'] ?></p>
                        </article>
                     </a>
                  </article>
               </div>
            <?php endforeach ?>
         </div>

         <div class="slider">
            <?php $sliders = get_field('slides_3'); ?>
            <?php foreach ($sliders as $k => $v): ?>
               <div>
                  <article class="bg-blue">
                     <a href="<?php echo $v['enlace'] ?>" target="_blank">
                        <!-- <span><b><?php echo $v['titulo'] ?></b></span> -->
                        <img src="<?php echo $v['portada'] ?>">
                        <article class="info">
                           <h4><?php echo $v['titulo'] ?></h4>
                           <p><?php echo $v['descripcion'] ?></p>
                        </article>
                     </a>
                  </article>
               </div>
            <?php endforeach ?>
         </div>

         <div class="slider">
            <?php $sliders = get_field('slides_4'); ?>
            <?php foreach ($sliders as $k => $v): ?>
               <div>
                  <article class="bg-blue">
                     <a href="<?php echo $v['enlace'] ?>" target="_blank">
                        <!-- <span><b><?php echo $v['titulo'] ?></b></span> -->
                        <img src="<?php echo $v['portada'] ?>">
                        <article class="info">
                           <h4><?php echo $v['titulo'] ?></h4>
                           <p><?php echo $v['descripcion'] ?></p>
                        </article>
                     </a>
                  </article>
               </div>
            <?php endforeach ?>
         </div>

         <div class="slider">
            <?php $sliders = get_field('slides_5'); ?>
            <?php foreach ($sliders as $k => $v): ?>
               <div>
                  <article class="bg-blue">
                     <a href="<?php echo $v['enlace'] ?>" target="_blank">
                        <!-- <span><b><?php echo $v['titulo'] ?></b></span> -->
                        <img src="<?php echo $v['portada'] ?>">
                        <article class="info">
                           <h4><?php echo $v['titulo'] ?></h4>
                           <p><?php echo $v['descripcion'] ?></p>
                        </article>
                     </a>
                  </article>
               </div>
            <?php endforeach ?>
         </div>
      </div>
   </section>

   <section class="information posts">
      
      <div class="wrapper">

         <div class="col-xs-12 col-md-9 ">
            <div class="vc_column-inner vc_custom_1530519865827">
               <div class="wpb_wrapper">
                  <div class="wrapper-posts box-recent type-loadMore layout-grid pagi-no description-hidden post-format-yes" data-layout="grid" data-paged="3" data-col="col-xs-12 col-sm-6 col-md-6 col-item-2" data-cat="bitcoin" data-number="4" data-ads="large-rectangle">
                  
                      <?php 
                        $noticias = get_posts([
                          'post_status' => 'publish',
                          'numberposts' => 6
                        ]);

                        $all_terms = get_terms( array(
                            'taxonomy' => 'category',
                            'hide_empty' => false,
                            'number' => 4
                        ) );

                      ?>

                     <div class="box-cats wrapper-posts layout-box1 mb-30 " data-layout="box1" data-typepost="views" data-dates="-2 year" data-paged="4" data-col="col-xs-12 col-sm-4 col-md-4" data-cat="bitcoin,blockchain,investment" data-number="6">
                        
                        <div class="box-title clearfix">
                           <h2 class="title-left">Últimas Noticias</h2>
                           <div class="box-filter clearfix">
                              <ul class="wrapper-filter" data-filter="true">
                                 <li class="active"><span class="cat-item loaded" data-catfilter="allCat-28">Todo</span></li>
                                 
                                 <?php foreach ($all_terms as $k): ?>
                                    
                                    <li><span class="cat-item" data-catfilter="<?php echo $k->term_id ?>"><?php echo $k->name ?></span></li>

                                 <?php endforeach ?>

                              </ul>
                           </div>
                        </div>
                        <span class="agr-loading"></span>
                        <div class="tab-content">
                           <div class="box-blog archive-blog row large-vertical clearfix active">

                             
                              <?php foreach ( $noticias as $post ) : setup_postdata( $post ); ?>
                              
                                 <?php 
                                    $terms = get_the_terms( get_the_ID(), 'category' );
                                    if ( !empty( $terms ) ){ $term = array_shift( $terms ); }
                                 ?>

                              <div class="col-xs-12 col-sm-6 col-md-6 hidden-description hidden-meta">
                                 <article class="post-item post-grid disss clearfix post-1448 post type-post status-publish format-video has-post-thumbnail hentry category-bitcoin tag-bitcoin post_format-post-format-video">
                                    <div class="article-tran hover-share-item">
                                       <div class="post-image">
                                          <a href="<?php the_permalink() ?>" class="bgr-item"></a>
                                          <a href="<?php the_permalink() ?>"> <?php the_post_thumbnail() ?> </a>
                                          <span class="post-cat "> <a href="/category/bitcoin/" title=""><?php echo $term->name; ?></a> </span>
                                       </div>
                                       <div class="article-content hidden-view hidden-comments">
                                          <div class="entry-header clearfix">
                                             <div class="entry-header-title">
                                                <h3 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                                             </div>
                                          </div>
                                          <div class="entry-content"></div>
                                       </div>
                                    </div>
                                 </article>
                              </div>

                              <?php endforeach; 
                              wp_reset_postdata(); ?>
                              
                           </div>
                        </div>
                     </div>


                     <a href="/noticias" class="view-posts">VER MÁS</a>
                     
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-md-3">

               <aside class="text-center">
                  <a class="twitter-timeline" data-height="400" data-theme="light" data-link-color="#FAB81E" href="https://twitter.com/indeciperu?ref_src=twsrc%5Etfw"></a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                  <p>&nbsp;</p>
                  <div class="fb-page" data-href="https://www.facebook.com/indeci/" data-tabs="timeline" data-width="390" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/indeci/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/indeci/">Instituto Nacional de Defensa Civil Perú</a></blockquote></div>
               </aside>

         </div>
      </div>

   </section>
   <section class="tato">
      <!-- DESKTOP -->
      <a href="#"><img src="http://indeci2.apprende.com.pe/wp-content/uploads/2018/09/BANNER_TATO.png"></a>
   </section>
</section>

<script type="text/javascript">
   var adm_url = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
</script>
<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>
<?php get_footer();
