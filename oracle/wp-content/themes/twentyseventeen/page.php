<?php

get_header(); ?>

	<section class="content">
		
		<?php
		while ( have_posts() ) : the_post(); ?>

			<?php the_post(); ?>

		<?php endwhile; // End of the loop.
		?>
		
	</section>

<?php get_footer();
