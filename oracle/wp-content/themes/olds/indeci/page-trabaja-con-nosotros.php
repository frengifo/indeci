<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<style type="text/css">
	.page-contact .post-content{
		width: 100%;
	}
	.page-contact .form{
		position: relative;
	}
	.page-contact .form h1{
		color :#fff;
	}
	.page-contact .form:before{
		content: "";
		background-color: #00639E;
		content: "";
		position: absolute;
		left: 0;
		top: 0;
		width: 150%;
		height: 100%;
		z-index: -1;
	}
	.page-contact form input{
		border-radius: 20px;
		border: 0;
		outline: none;
		padding: .25em 0.5em .25em 1em;
		height: 30px;
	}
	.page-contact form label{
		margin-left: 1.5em;
	}
</style>
<section class="content general page-contact m-0 p-0" style="    padding: 0;">
		<div class="bg">
			
			<div class="wrapper">
				<div class="post-content">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

						<?php the_content(); ?>
					<!-- post -->
					<?php endwhile; ?>
					<!-- post navigation -->
					<?php else: ?>
					<!-- no posts found -->
					<?php endif; ?>
				</div>
			</div>	

		</div>
		
	</section>

<?php get_footer();
