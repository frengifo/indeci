<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<link href="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps.css" rel="stylesheet" type="text/css" />
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>Preparación</h3>
					<ul>
						<li ><a href="/que_hacemos/nosotros-preparacion/" >Nosotros</a></li>
						<li ><a href="/que_hacemos/nosotros-preparacion/" >Escenarios de Riesgo</a></li>
						<li ><a href="/que_hacemos/alerta/" class="active">Alerta Temprana</a></li>
						<li ><a href="/que_hacemos/nosotros-preparacion/">Recursos para la Respuesta</a></li>
					</ul>

					<ul class="others">
						<h3 ><a href="/que_hacemos/nosotros-respuesta/">Respuesta</a></h3>
						<h3 ><a href="/que_hacemos/nosotros-defocaph/">DEFOCAPH</a></h3>
					</ul>
				</nav>

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
			<div class="post-content ">
				<article>
					<h1>SUB DIRECCIÓN  DE MONITOREO <br> Y ALERTA TEMPRANA</h1>
					<div class="article">
						<img src="<?php echo $asset_path; ?>/assets/img/alerta-temparana.png">
						<p>&nbsp;</p>
						
						<div class="container">
							<div class="info">
								<div class="item">
									<div class="info-PI active">
										<a ref="javascript:;" class="btn-more" style="background-color: #fff;">
											<img src="<?php echo $asset_path; ?>/assets/img/ddi-piura.png">
											
										</a>
										
									</div>
									<div class="info-AM">
										<h2>DDI AMAZONAS</h2>
										<p>CRL. EP(R) PEREZ LEAL ORISON <br> DIRECTOR DE LA DIRECCIÓN DESCONCENTRADA DE PIURA</p>
										<p>
											Mz 241 - lote 2, Zona industrial - Piura (Junto a Plaza vea)<br>
											Teléfono: (073)-309-800<br>
											Celular: 969-284-266 RPM: #705941<br>
											e-mail: operez@indeci.gob.pe<br>
										</p>
										<h3>ALMACENES:</h3>
										<p>
											Zona Industrial Mza. 241 Lote 2.<br>
											Av. Gráu N° 1681-Urb -San José<br>
											Urb. San Antonio Mz C Lt. 6
										</p>
										<a href="javascript:;" class="btn-more" style="    background-color: #fff;">Mira lo que hacemos <i class="fas fa-arrow-right"></i></a>
										
									</div>

									<div class="info-LL">
										<h2>DDI LA LIBERTAD</h2>
										<p>CRL. EP(R) PEREZ LEAL ORISON <br> DIRECTOR DE LA DIRECCIÓN DESCONCENTRADA DE PIURA</p>
										<p>
											Mz 241 - lote 2, Zona industrial - Piura (Junto a Plaza vea)<br>
											Teléfono: (073)-309-800<br>
											Celular: 969-284-266 RPM: #705941<br>
											e-mail: operez@indeci.gob.pe<br>
										</p>
										<h3>ALMACENES:</h3>
										<p>
											Zona Industrial Mza. 241 Lote 2.<br>
											Av. Gráu N° 1681-Urb -San José<br>
											Urb. San Antonio Mz C Lt. 6
										</p>
										<a href="javascript:;" class="btn-more">Mira lo que hacemos <i class="fas fa-arrow-right"></i></a>
										
									</div>
									<div class="info-TU">
										<h2>DDI TUMBES</h2>
										<p>CRL. EP(R) PEREZ LEAL ORISON <br> DIRECTOR DE LA DIRECCIÓN DESCONCENTRADA DE PIURA</p>
										<p>
											Mz 241 - lote 2, Zona industrial - Piura (Junto a Plaza vea)<br>
											Teléfono: (073)-309-800<br>
											Celular: 969-284-266 RPM: #705941<br>
											e-mail: operez@indeci.gob.pe<br>
										</p>
										<h3>ALMACENES:</h3>
										<p>
											Zona Industrial Mza. 241 Lote 2.<br>
											Av. Gráu N° 1681-Urb -San José<br>
											Urb. San Antonio Mz C Lt. 6
										</p>
										<a href="javascript:;" class="btn-more">Mira lo que hacemos <i class="fas fa-arrow-right"></i></a>
										
									</div>
									<div class="info-LO">
										<h2>DDI LORETO</h2>
										<p>CRL. EP(R) PEREZ LEAL ORISON <br> DIRECTOR DE LA DIRECCIÓN DESCONCENTRADA DE PIURA</p>
										<p>
											Mz 241 - lote 2, Zona industrial - Piura (Junto a Plaza vea)<br>
											Teléfono: (073)-309-800<br>
											Celular: 969-284-266 RPM: #705941<br>
											e-mail: operez@indeci.gob.pe<br>
										</p>
										<h3>ALMACENES:</h3>
										<p>
											Zona Industrial Mza. 241 Lote 2.<br>
											Av. Gráu N° 1681-Urb -San José<br>
											Urb. San Antonio Mz C Lt. 6
										</p>
										<a href="javascript:;" class="btn-more">Mira lo que hacemos <i class="fas fa-arrow-right"></i></a>
										
									</div>
									<div class="info-CJ">
										<h2>DDI CAJAMARCA</h2>
										<p>CRL. EP(R) PEREZ LEAL ORISON <br> DIRECTOR DE LA DIRECCIÓN DESCONCENTRADA DE PIURA</p>
										<p>
											Mz 241 - lote 2, Zona industrial - Piura (Junto a Plaza vea)<br>
											Teléfono: (073)-309-800<br>
											Celular: 969-284-266 RPM: #705941<br>
											e-mail: operez@indeci.gob.pe<br>
										</p>
										<h3>ALMACENES:</h3>
										<p>
											Zona Industrial Mza. 241 Lote 2.<br>
											Av. Gráu N° 1681-Urb -San José<br>
											Urb. San Antonio Mz C Lt. 6
										</p>
										<a href="javascript:;" class="btn-more">Mira lo que hacemos <i class="fas fa-arrow-right"></i></a>
										
									</div>
									<div class="info-LB">
										<h2>DDI LAMBAYEQUE</h2>
										<p>CRL. EP(R) PEREZ LEAL ORISON <br> DIRECTOR DE LA DIRECCIÓN DESCONCENTRADA DE PIURA</p>
										<p>
											Mz 241 - lote 2, Zona industrial - Piura (Junto a Plaza vea)<br>
											Teléfono: (073)-309-800<br>
											Celular: 969-284-266 RPM: #705941<br>
											e-mail: operez@indeci.gob.pe<br>
										</p>
										<h3>ALMACENES:</h3>
										<p>
											Zona Industrial Mza. 241 Lote 2.<br>
											Av. Gráu N° 1681-Urb -San José<br>
											Urb. San Antonio Mz C Lt. 6
										</p>
										<a href="javascript:;" class="btn-more">Mira lo que hacemos <i class="fas fa-arrow-right"></i></a>
										
									</div>
								</div>
								<div class="more">
									<p>&nbsp;</p>
									<p>Las Direcciones Desconcentradas son órganos desconcentrados del INDECI, que brindan asesoramiento y asistencia técnica a las autoridades y a los funcionarios de los Gobiernos regionales y Locales en la Gestión del Riesgo de Desastres en lo referente a los procesos de preparación, respuesta y rehabilitación en concordancia a los lineamientos técnicos establecidos por los órganos de línea competentes. Actúan en representación y por delegación del Jefe Institucional dentro del territorio sobre el cual ejercen jurisdicción. Las principales funciones de las Direcciones Desconcentradas son:</p>
								</div>
							</div>
						    <!-- Map html - add the below to your page -->
						    <div class="jsmaps-wrapper" id="peru-map"></div>
						    <!-- End Map html -->
						  </div>
					</div>
				</article>
			</div>
		</div>	
		
	</section>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps-libs.js" type="text/javascript"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps-panzoom.js"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps.min.js" type="text/javascript"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/peru.js" type="text/javascript"></script>
<?php get_footer();
