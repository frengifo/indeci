		<div class="item">
					
			<?php 
				$regiones = get_posts([
				  'post_type' => 'regiones',
				  'post_status' => 'publish',
				  'numberposts' => -1
				]);
			 ?>

			<?php foreach ( $regiones as $post ) : setup_postdata( $post ); ?>
				
				<div class="info-<?php the_field('region') ?>">
					<h2><?php the_title(); ?></h2>
					<p><?php the_field('responsable') ?></p>
					<p>
						<?php the_field('direccion') ?><br>
						Teléfono: <?php the_field('telefono') ?><br>
						Celular: <?php the_field('celular') ?><br>
						e-mail: <?php the_field('email') ?><br>
					</p>
					<h3>ALMACENES:</h3>
					<p>
						<?php the_field('almacenes') ?>
					</p>
					<a href="javascript:;" class="btn-more" style="    background-color: #fff;">Mira lo que hacemos <i class="fas fa-arrow-right"></i></a>
					
				</div>

			<?php endforeach; 
			wp_reset_postdata();?>
		</div>
		<div class="more">
			<p>&nbsp;</p>
			<p>Las Direcciones Desconcentradas son órganos desconcentrados del INDECI, que brindan asesoramiento y asistencia técnica a las autoridades y a los funcionarios de los Gobiernos regionales y Locales en la Gestión del Riesgo de Desastres en lo referente a los procesos de preparación, respuesta y rehabilitación en concordancia a los lineamientos técnicos establecidos por los órganos de línea competentes. Actúan en representación y por delegación del Jefe Institucional dentro del territorio sobre el cual ejercen jurisdicción. Las principales funciones de las Direcciones Desconcentradas son:</p>
		</div>
    