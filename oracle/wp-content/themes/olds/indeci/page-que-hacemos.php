<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="banner-featured" style="background-image: url('<?php echo $asset_path ?>/assets/img/banner-indeci.png')">
   <div class="container"> <h1><?php the_field('texto_frase') ?></h1> </div>
</div>
<div id="content" class="site-content initial-page" style="transform: none;">
   <div class="wrap-content" role="main" style="transform: none;">
      <div class="container" style="transform: none;">
         <div class="row single-main-content single-1" style="transform: none;">
           
            <div class=" content-padding-left main-content">
               <div class="content-inner">
                  <div class="box-article">
                     <article  class="post-1421 post type-post status-publish format-standard has-post-thumbnail hentry category-scams tag-scams">
                           
                        <!--<h1><?php the_title(); ?></h1>-->
                        <div class="col-xs-12">
                              <h1 class="title-subline"><?php the_title() ?></h1>
                              <div class="article">
                           
                              <?php the_content() ?>
                              </div>

                        </div>

                        <?php 
                           $terms = get_terms( array(
                                        'taxonomy' => 'areas',
                                        'hide_empty' => false,
                                    ) );
                           foreach ($terms as $term) { ?>
                              <?php $img =get_field('featured_image', $term); ?>
                              <div class="col-md-6 col-sm-6">
                                 <div class="box">
                                    <a href="<?php echo get_term_link( $term ); ?>">
                                       <img src="<?php echo $img; ?>">
                                       <strong><span><?php echo $term->name; ?></span></strong>
                                       <small><?php echo $term->description; ?></small>
                                    </a>
                                 </div>
                              </div>

                           <?php } ?>

                     </article>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>
<?php get_footer();
