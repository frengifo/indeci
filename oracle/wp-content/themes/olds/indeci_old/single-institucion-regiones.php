<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<section class="content general">
		
		<div class="wrapper">
			
			<div class="post-content full-width">
				<article>
					<h1><?php the_title(); ?></h1>
					<div class="article">
						<?php the_content() ?>
						<?php if ( get_the_ID() == 19 ): ?>
							<?php get_template_part( 'partials/content', 'regiones' ) ?>
						<?php endif ?>
					</div>
				</article>
			</div>
		</div>	
		
	</section>

<!-- post -->
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>
	
<?php get_footer();
