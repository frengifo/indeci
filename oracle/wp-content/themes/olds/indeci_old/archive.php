<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>Información Institucional</h3>
					<ul>
						<li><a href="#">Acerca de Indeci</a></li>
						<li><a href="#">Principios de Defensa Civil</a></li>
						<li><a href="#">Organigrama</a></li>
						<li><a href="#">Marco Legal</a></li>
						<li><a href="#">Directorio</a></li>
						<li><a href="#">Código de Ética</a></li>
						<li><a href="#">Sistema de Control Interno</a></li>
					</ul>
				</nav>

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
			<div class="post-content">
				
			</div>
		</div>	
		
	</section>

<?php get_footer();
