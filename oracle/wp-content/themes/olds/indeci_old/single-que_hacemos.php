<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<link href="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps.css" rel="stylesheet" type="text/css" />
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>Preparación</h3>
					<?php wp_nav_menu( ['menu' => 'menu-preparacion'] ) ?>
					<!-- <ul>
						<li ><a href="/que_hacemos/nosotros-preparacion/" class="active">Nosotros</a></li>
						<li ><a href="/que_hacemos/nosotros-preparacion/">Escenarios de Riesgo</a></li>
						<li ><a href="/que_hacemos/alerta/">Alerta Temprana</a></li>
						<li ><a href="/que_hacemos/nosotros-preparacion/">Recursos para la Respuesta</a></li>
					</ul> -->
					<div class="others">
						<h3 ><a href="/que_hacemos/nosotros-respuesta/">Respuesta</a></h3>
						<h3 ><a href="/que_hacemos/nosotros-defocaph/">DEFOCAPH</a></h3>
					</div>
				</nav>

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="post-content ">
					<article>
						<h1><?php the_title(); ?></h1>
						<div class="article">
							<?php the_content(); ?>
							
							<?php if(get_field('funciones')): ?>

								<div class="funtions">
									<h3> NUESTRAS FUNCIONES </h3>
						          	<?php while(has_sub_field('funciones')): ?>
										
										<div class="list-aside-image">
											<div>
												<img src="<?php the_sub_field('image'); ?>" width="85" height="85">
												<article>
													<p><?php the_sub_field('description'); ?></p>
												</article>
											</div>
										</div>

						          	<?php endwhile; ?>
								</div>

					        <?php endif; ?>

							<p>&nbsp;</p>
							
							<?php if ( in_array(get_the_ID(), [23,24])  ): ?>
								<div class="regions hidden">
									<?php 
										$regiones = get_posts([
										  'post_type' => 'regiones',
										  'post_status' => 'publish',
										  'numberposts' => -1
										]);
									 ?>
									<?php foreach ( $regiones as $post ) : setup_postdata( $post ); ?>
										<?php $reg = get_field_object('region'); ?>
										<a href="javascript:;" class="<?php the_field('region') ?>" data-code="<?php the_field('region') ?>" data-name="<?php echo $reg['choices'][ get_field('region') ] ?>"><?php the_field('nombre') ?></a>
									<?php endforeach; 
									wp_reset_postdata();?>

								</div>
								<div class="container">
									<div class="info">
										
										<?php get_template_part( 'partials/content', 'regiones-map' ); ?>
									</div>
									<!-- Map html - add the below to your page -->
								    <div class="jsmaps-wrapper" id="peru-map"></div>
								    <!-- End Map html -->

								</div>
							<?php endif ?>

							<?php if(get_field('documentos')): ?>

								<div >
										
									<?php foreach (get_field('documentos') as $k => $v): ?>
										
											<h3><?php echo $v['nombre'] ?></h3>
											<p><strong>SUMILLA:</strong> <?php echo $v['sumilla'] ?></p>
											<p><strong>ARCHIVO:</strong> <a href="<?php echo $v['archivo'] ?>" style="color:#333;font-size: 18px;" target="_blank"><i class="fas fa-download"></i></a>
											</p>

									<?php endforeach ?>

								</div>

					        <?php endif; ?>


						</div>
					</article>
				</div>

			<!-- post -->
			<?php endwhile; ?>
			<!-- post navigation -->
			<?php else: ?>
			<!-- no posts found -->
			<?php endif; ?>
		</div>	
		
	</section>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps-libs.js" type="text/javascript"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps-panzoom.js"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps.min.js" type="text/javascript"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/peru.js" type="text/javascript"></script>
<?php get_footer();
