<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<!-- <nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>COEN</h3>
					<ul>
						<li ><a href="/coen/nosotros/" class="active">Nosotros</a></li>
						<li ><a href="/coen/nosotros/">Instrumento de Gestión<br> de la Información</a></li>
					</ul>
				</nav> -->

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="post-content single-coen">
				<article class="article">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</article>
			</div>
		<!-- post -->
		<?php endwhile; ?>
		<!-- post navigation -->
		<?php else: ?>
		<!-- no posts found -->
		<?php endif; ?>
		</div>	
		
	</section>

<?php get_footer();
