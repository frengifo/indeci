<?php

get_header(); 
?>
<?php $asset_path = get_template_directory_uri(); ?>
<link href="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps.css" rel="stylesheet" type="text/css" />
	<section class="content general">
		
		<div class="wrapper">
			<aside class="aside-nav">
				<nav>
					<a href="javascript:;" class="btn-aside-nav"><i class="fas fa-chevron-down"></i></a>
					<h3>Desarrollo y Fortalecimiento <br> de Capacidades Humanas</h3>
					<ul>
						<li><a href="/que_hacemos/nosotros-defocaph/" class="active">Nosotros</a></li>
						<li ><a href="/que_hacemos/nosotros-defocaph/" style="    padding-top: .25em;    padding-bottom: 1.3em;">Plan de Educación Comunitaria en Gestión Reactiva</a></li>
						<li ><a href="/que_hacemos/nosotros-defocaph/">INDECI @Educ@</a></li>
						<li ><a href="/que_hacemos/nosotros-defocaph/">Biblioteca Virtual</a></li>
					</ul>

					<div class="others">
						<h3 ><a href="/que_hacemos/nosotros-preparacion/">Preparación</a></h3>
						<h3 ><a href="/que_hacemos/nosotros-respuesta/">Respuesta</a></h3>
					</div>
				</nav>

				<div class="calendar">
					<h3>Calendario de Eventos</h3>
					<input id="datetimepicker" type="text" >
				</div>

				<div class="advertising">
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban1.png">
					</a>
					<a href="#">
						<img src="<?php echo $asset_path; ?>/assets/img/ban2.png">
					</a>
				</div>

			</aside>
			<div class="post-content ">
				<article>
					<h1><?php the_title(); ?></h1>
					<div class="article">
						<?php the_content(); ?>
							
						<?php if(get_field('que_hacemos')): ?>

							<div class="funtions defo">
								<h3> NUESTRAS FUNCIONES </h3>
					          	<?php while(has_sub_field('que_hacemos')): ?>
									
									<div class="list-aside-image">
										<div>
											<img src="<?php the_sub_field('imagen'); ?>" width="85" height="85">
											<article>
												<p><?php the_sub_field('descripcion'); ?></p>
											</article>
										</div>
									</div>

					          	<?php endwhile; ?>
							</div>

				        <?php endif; ?>

				        <?php $tareas = get_field('tareas'); ?>
				        <?php if($tareas): ?>
				        	<img src="<?php echo $asset_path; ?>/assets/img/tato-defocaph.png">
				        	<div class="tasks-box">
					        	<div class="btns">
					        		<?php foreach ($tareas as $k => $v): ?>
					        				<a href="javascript:;" data-display="#proc-<?php echo $k ?>" class="<?php echo $k == 0 ? "active":""; ?>"><?php echo $v['titulo'] ?></a>
					        		<?php endforeach ?>
									
					        	</div>
					          	<?php foreach ($tareas as $k => $v): ?>
									<div id="proc-<?php echo $k; ?>" class="display-info text-center" style="display:<?php echo $k == 0 ? "block":"none"; ?>;">
									
										<article> 
											<h3><?php echo $v['titulo'] ?></h3>
											<p><?php echo $v['descripcion']; ?></p> 
											<?php if (!empty($v['enlace'])): ?>
												<a href="<?php echo $v['enlace']; ?>" class="view-more orange" target="_blank">ver más <i class="fas fa-arrow-right"></i></a>
											<?php endif ?>
										</article>

									</div>
					          	<?php endforeach ?>

				        	</div>

				        <?php endif; ?>
						
					</div>
				</article>
			</div>
		</div>	
		
	</section>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps-libs.js" type="text/javascript"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps-panzoom.js"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/jsmaps.min.js" type="text/javascript"></script>
	<script src="<?php echo $asset_path; ?>/assets/js/jsmaps/peru.js" type="text/javascript"></script>
<?php get_footer();
