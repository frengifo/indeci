<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php $asset_path = get_template_directory_uri(); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $asset_path ?>/assets/css/style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<script type="text/javascript" src="<?php echo $asset_path; ?>/assets/js/jquery-3.3.1.min.js"></script>
	<?php wp_head(); ?>
	<style type="text/css"> #wpadminbar{ display: none !important; } html{    margin-top: 0 !important; } </style>

</head>

<body <?php body_class(); ?>>

<body class="page-template-default page page-id-1480 wpb-js-composer js-comp-ver-5.5.2 vc_responsive">
	<div id="page" class="wrapper site">
        <div  class="canvas-overlay"></div>

		<header
	        id="masthead" class="site-header header-left header-left2 style_white">
	        <div
	           id="bitther-header">
	           <div
	              class="top-header">
	              <div
	                 id="ccpw_widget-2" class="widget first ccpw_widget">
	                 <h2 class="widgettitle"></h2>
	                 <div
	                    style="display:none" class="ccpw-container  ccpw-ticker-cont   ">
	                    <ul
	                       id="ccpw-ticker-2058">
	                       <li
	                          data-coin-symbol="BTC" data-coin-price="6326.95357508"
	                          class="add_sub style-1" id="bitcoin">
	                          <div
	                             class="coin-container ccpw-tooltip" data-tooltip-content="#tooltip_content_bitcoin"><span
	                             class="ccpw_icon"><img
	                             style="width:32px;" id="bitcoin" alt="bitcoin" src="http://bitther.nanoagency.co/wp-content/plugins/cryptocurrency-price-ticker-widget-pro/assets/coins-logos/bitcoin.svg"></span><span
	                             class="ticker-name">Bitcoin</span><span
	                             class="ticker-symbol">(BTC)</span><span
	                             class="ticker-price">&#36;6326.95357508</span><span
	                             class="ccpw-changes down"><i
	                             class="fa fa-arrow-down" aria-hidden="true"></i>-0.41%</span></div>
	                          <div
	                             class="ccpw_tooltip_templates">
	                             <span
	                                id="tooltip_content_bitcoin">
	                                <div
	                                   class="tooltip-title"><span
	                                   class="title-style"><strong>BITCOIN</strong></span></div>
	                                <div
	                                   class="24per-change"><span
	                                   class="changes down">24h % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-4.21%</span></div>
	                                <div
	                                   class="7dper-change"><span
	                                   class="changes down">7d % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-17.41%</span></div>
	                                <div
	                                   class="tooltip-list-vol">VOLUME : &#36;4.87B</div>
	                                <div
	                                   class="tooltip-market_cap">MARKET CAP:&#36;108.81B</div>
	                             </span>
	                          </div>
	                       </li>
	                       <li
	                          data-coin-symbol="ETH" data-coin-price="361.80391841"
	                          class="add_sub style-1" id="ethereum">
	                          <div
	                             class="coin-container ccpw-tooltip" data-tooltip-content="#tooltip_content_ethereum"><span
	                             class="ccpw_icon"><img
	                             style="width:32px;" id="ethereum" alt="ethereum" src="http://bitther.nanoagency.co/wp-content/plugins/cryptocurrency-price-ticker-widget-pro/assets/coins-logos/ethereum.svg"></span><span
	                             class="ticker-name">Ethereum</span><span
	                             class="ticker-symbol">(ETH)</span><span
	                             class="ticker-price">&#36;361.80391841</span><span
	                             class="ccpw-changes up"><i
	                             class="fa fa-arrow-up" aria-hidden="true"></i>0.24%</span></div>
	                          <div
	                             class="ccpw_tooltip_templates">
	                             <span
	                                id="tooltip_content_ethereum">
	                                <div
	                                   class="tooltip-title"><span
	                                   class="title-style"><strong>ETHEREUM</strong></span></div>
	                                <div
	                                   class="24per-change"><span
	                                   class="changes down">24h % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-4.21%</span></div>
	                                <div
	                                   class="7dper-change"><span
	                                   class="changes down">7d % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-14.16%</span></div>
	                                <div
	                                   class="tooltip-list-vol">VOLUME : &#36;1.97B</div>
	                                <div
	                                   class="tooltip-market_cap">MARKET CAP:&#36;36.62B</div>
	                             </span>
	                          </div>
	                       </li>
	                       <li
	                          data-coin-symbol="USDT" data-coin-price="1.0044651247"
	                          class="add_sub style-1" id="tether">
	                          <div
	                             class="coin-container ccpw-tooltip" data-tooltip-content="#tooltip_content_tether"><span
	                             class="ccpw_icon"><img
	                             style="width:32px;" id="tether" alt="tether" src="http://bitther.nanoagency.co/wp-content/plugins/cryptocurrency-price-ticker-widget-pro/assets/coins-logos/tether.svg"></span><span
	                             class="ticker-name">Tether</span><span
	                             class="ticker-symbol">(USDT)</span><span
	                             class="ticker-price">&#36;1.0044651247</span><span
	                             class="ccpw-changes up"><i
	                             class="fa fa-arrow-up" aria-hidden="true"></i>0.33%</span></div>
	                          <div
	                             class="ccpw_tooltip_templates">
	                             <span
	                                id="tooltip_content_tether">
	                                <div
	                                   class="tooltip-title"><span
	                                   class="title-style"><strong>TETHER</strong></span></div>
	                                <div
	                                   class="24per-change"><span
	                                   class="changes up">24h % CHANGES : <i
	                                   class="fa fa-arrow-up" aria-hidden="true"></i>0.25%</span></div>
	                                <div
	                                   class="7dper-change"><span
	                                   class="changes up">7d % CHANGES : <i
	                                   class="fa fa-arrow-up" aria-hidden="true"></i>0.53%</span></div>
	                                <div
	                                   class="tooltip-list-vol">VOLUME : &#36;3.28B</div>
	                                <div
	                                   class="tooltip-market_cap">MARKET CAP:&#36;2.42B</div>
	                             </span>
	                          </div>
	                       </li>
	                       <li
	                          data-coin-symbol="XRP" data-coin-price="0.3437513701"
	                          class="add_sub style-1" id="ripple">
	                          <div
	                             class="coin-container ccpw-tooltip" data-tooltip-content="#tooltip_content_ripple"><span
	                             class="ccpw_icon"><img
	                             style="width:32px;" id="ripple" alt="ripple" src="http://bitther.nanoagency.co/wp-content/plugins/cryptocurrency-price-ticker-widget-pro/assets/coins-logos/ripple.svg"></span><span
	                             class="ticker-name">XRP</span><span
	                             class="ticker-symbol">(XRP)</span><span
	                             class="ticker-price">&#36;0.3437513701</span><span
	                             class="ccpw-changes down"><i
	                             class="fa fa-arrow-down" aria-hidden="true"></i>-0.1%</span></div>
	                          <div
	                             class="ccpw_tooltip_templates">
	                             <span
	                                id="tooltip_content_ripple">
	                                <div
	                                   class="tooltip-title"><span
	                                   class="title-style"><strong>XRP</strong></span></div>
	                                <div
	                                   class="24per-change"><span
	                                   class="changes down">24h % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-4.66%</span></div>
	                                <div
	                                   class="7dper-change"><span
	                                   class="changes down">7d % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-22.48%</span></div>
	                                <div
	                                   class="tooltip-list-vol">VOLUME : &#36;359.08M</div>
	                                <div
	                                   class="tooltip-market_cap">MARKET CAP:&#36;13.51B</div>
	                             </span>
	                          </div>
	                       </li>
	                       <li
	                          data-coin-symbol="BCH" data-coin-price="584.875148155"
	                          class="add_sub style-1" id="bitcoin-cash">
	                          <div
	                             class="coin-container ccpw-tooltip" data-tooltip-content="#tooltip_content_bitcoin-cash"><span
	                             class="ccpw_icon"><img
	                             style="width:32px;" id="bitcoin-cash" alt="bitcoin-cash" src="http://bitther.nanoagency.co/wp-content/plugins/cryptocurrency-price-ticker-widget-pro/assets/coins-logos/bitcoin-cash.svg"></span><span
	                             class="ticker-name">Bitcoin Cash</span><span
	                             class="ticker-symbol">(BCH)</span><span
	                             class="ticker-price">&#36;584.875148155</span><span
	                             class="ccpw-changes down"><i
	                             class="fa fa-arrow-down" aria-hidden="true"></i>-1.25%</span></div>
	                          <div
	                             class="ccpw_tooltip_templates">
	                             <span
	                                id="tooltip_content_bitcoin-cash">
	                                <div
	                                   class="tooltip-title"><span
	                                   class="title-style"><strong>BITCOIN CASH</strong></span></div>
	                                <div
	                                   class="24per-change"><span
	                                   class="changes down">24h % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-8.82%</span></div>
	                                <div
	                                   class="7dper-change"><span
	                                   class="changes down">7d % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-23.45%</span></div>
	                                <div
	                                   class="tooltip-list-vol">VOLUME : &#36;447.59M</div>
	                                <div
	                                   class="tooltip-market_cap">MARKET CAP:&#36;10.11B</div>
	                             </span>
	                          </div>
	                       </li>
	                       <li
	                          data-coin-symbol="EOS" data-coin-price="5.6377316818"
	                          class="add_sub style-1" id="eos">
	                          <div
	                             class="coin-container ccpw-tooltip" data-tooltip-content="#tooltip_content_eos"><span
	                             class="ccpw_icon"><img
	                             style="width:32px;" id="eos" alt="eos" src="http://bitther.nanoagency.co/wp-content/plugins/cryptocurrency-price-ticker-widget-pro/assets/coins-logos/eos.svg"></span><span
	                             class="ticker-name">EOS</span><span
	                             class="ticker-symbol">(EOS)</span><span
	                             class="ticker-price">&#36;5.6377316818</span><span
	                             class="ccpw-changes down"><i
	                             class="fa fa-arrow-down" aria-hidden="true"></i>-0.72%</span></div>
	                          <div
	                             class="ccpw_tooltip_templates">
	                             <span
	                                id="tooltip_content_eos">
	                                <div
	                                   class="tooltip-title"><span
	                                   class="title-style"><strong>EOS</strong></span></div>
	                                <div
	                                   class="24per-change"><span
	                                   class="changes down">24h % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-11.31%</span></div>
	                                <div
	                                   class="7dper-change"><span
	                                   class="changes down">7d % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-21.8%</span></div>
	                                <div
	                                   class="tooltip-list-vol">VOLUME : &#36;892.65M</div>
	                                <div
	                                   class="tooltip-market_cap">MARKET CAP:&#36;5.11B</div>
	                             </span>
	                          </div>
	                       </li>
	                       <li
	                          data-coin-symbol="LTC" data-coin-price="62.9637930405"
	                          class="add_sub style-1" id="litecoin">
	                          <div
	                             class="coin-container ccpw-tooltip" data-tooltip-content="#tooltip_content_litecoin"><span
	                             class="ccpw_icon"><img
	                             style="width:32px;" id="litecoin" alt="litecoin" src="http://bitther.nanoagency.co/wp-content/plugins/cryptocurrency-price-ticker-widget-pro/assets/coins-logos/litecoin.svg"></span><span
	                             class="ticker-name">Litecoin</span><span
	                             class="ticker-symbol">(LTC)</span><span
	                             class="ticker-price">&#36;62.9637930405</span><span
	                             class="ccpw-changes down"><i
	                             class="fa fa-arrow-down" aria-hidden="true"></i>-0.89%</span></div>
	                          <div
	                             class="ccpw_tooltip_templates">
	                             <span
	                                id="tooltip_content_litecoin">
	                                <div
	                                   class="tooltip-title"><span
	                                   class="title-style"><strong>LITECOIN</strong></span></div>
	                                <div
	                                   class="24per-change"><span
	                                   class="changes down">24h % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-6.3%</span></div>
	                                <div
	                                   class="7dper-change"><span
	                                   class="changes down">7d % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-19.14%</span></div>
	                                <div
	                                   class="tooltip-list-vol">VOLUME : &#36;305.50M</div>
	                                <div
	                                   class="tooltip-market_cap">MARKET CAP:&#36;3.64B</div>
	                             </span>
	                          </div>
	                       </li>
	                       <li
	                          data-coin-symbol="XLM" data-coin-price="0.2037637833"
	                          class="add_sub style-1" id="stellar">
	                          <div
	                             class="coin-container ccpw-tooltip" data-tooltip-content="#tooltip_content_stellar"><span
	                             class="ccpw_icon"><img
	                             style="width:32px;" id="stellar" alt="stellar" src="http://bitther.nanoagency.co/wp-content/plugins/cryptocurrency-price-ticker-widget-pro/assets/coins-logos/stellar.svg"></span><span
	                             class="ticker-name">Stellar</span><span
	                             class="ticker-symbol">(XLM)</span><span
	                             class="ticker-price">&#36;0.2037637833</span><span
	                             class="ccpw-changes down"><i
	                             class="fa fa-arrow-down" aria-hidden="true"></i>-0.3%</span></div>
	                          <div
	                             class="ccpw_tooltip_templates">
	                             <span
	                                id="tooltip_content_stellar">
	                                <div
	                                   class="tooltip-title"><span
	                                   class="title-style"><strong>STELLAR</strong></span></div>
	                                <div
	                                   class="24per-change"><span
	                                   class="changes down">24h % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-8.16%</span></div>
	                                <div
	                                   class="7dper-change"><span
	                                   class="changes down">7d % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-26.2%</span></div>
	                                <div
	                                   class="tooltip-list-vol">VOLUME : &#36;67.63M</div>
	                                <div
	                                   class="tooltip-market_cap">MARKET CAP:&#36;3.82B</div>
	                             </span>
	                          </div>
	                       </li>
	                       <li
	                          data-coin-symbol="IOT" data-coin-price="0.6310205234"
	                          class="add_sub style-1" id="iota">
	                          <div
	                             class="coin-container ccpw-tooltip" data-tooltip-content="#tooltip_content_iota"><span
	                             class="ccpw_icon"><img
	                             style="width:32px;" id="iota" alt="iota" src="http://bitther.nanoagency.co/wp-content/plugins/cryptocurrency-price-ticker-widget-pro/assets/coins-logos/iota.svg"></span><span
	                             class="ticker-name">IOTA</span><span
	                             class="ticker-symbol">(MIOTA)</span><span
	                             class="ticker-price">&#36;0.6310205234</span><span
	                             class="ccpw-changes down"><i
	                             class="fa fa-arrow-down" aria-hidden="true"></i>-1.57%</span></div>
	                          <div
	                             class="ccpw_tooltip_templates">
	                             <span
	                                id="tooltip_content_iota">
	                                <div
	                                   class="tooltip-title"><span
	                                   class="title-style"><strong>IOTA</strong></span></div>
	                                <div
	                                   class="24per-change"><span
	                                   class="changes down">24h % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-11.21%</span></div>
	                                <div
	                                   class="7dper-change"><span
	                                   class="changes down">7d % CHANGES : <i
	                                   class="fa fa-arrow-down" aria-hidden="true"></i>-31.53%</span></div>
	                                <div
	                                   class="tooltip-list-vol">VOLUME : &#36;70.15M</div>
	                                <div
	                                   class="tooltip-market_cap">MARKET CAP:&#36;1.75B</div>
	                             </span>
	                          </div>
	                       </li>
	                    </ul>
	                 </div>
	                 <style type='text/css'>.tickercontainer #ccpw-ticker-2058{background-color:#eee}.tooltipster #ccpw-ticker-2058{background-color:#eee}.tooltipster-sidetip .tooltipster-box{background-color:#eee;border-color:#eee}.tooltipster-sidetip.tooltipster-top .tooltipster-arrow-background{border-top-color:#eee}.tooltipster-sidetip.tooltipster-bottom .tooltipster-arrow-background{border-bottom-color:#eee}.tooltipster-sidetip.tooltipster-top .tooltipster-arrow-border{border-top-color:#eee}.tooltipster-sidetip.tooltipster-bottom .tooltipster-arrow-border{border-bottom-color:#eee}.list-vol #wid-2058{color:#000}.market_cap #wid-2058{color:#000}.supply #wid-2058{color:#000}.tickercontainer #ccpw-ticker-2058 .ticker-name,
	                    .tickercontainer #ccpw-ticker-2058 .ticker-symbol,
	                    .tickercontainer #ccpw-ticker-2058 .live-pricing,
	                    .tickercontainer #ccpw-ticker-2058  span.changes:after,
	                    .tickercontainer #ccpw-ticker-2058 .ticker-price,.tooltip-title,.tooltip-list-vol,.tooltip-market_cap{color:#000}
	                 </style>
	              </div>
	           </div>
	           <div
	              class="header-content-logo container">
	              <div
	                 class="site-logo" id="logo">
	                 <a
	                    href="http://bitther.nanoagency.co/" rel="home">
	                 <img
	                    src="http://bitther.nanoagency.co/wp-content/uploads/2018/05/logo_black.png" alt="logo" /></a>
	              </div>
	              <div
	                 class="header-middle">
	                 <div
	                    id="media_image-5" class="widget first widget_media_image"><img
	                    width="720" height="90" src="http://bitther.nanoagency.co/wp-content/uploads/2018/04/ad.jpg" class="image wp-image-1351  attachment-full size-full" alt="" style="max-width: 100%; height: auto;" srcset="http://bitther.nanoagency.co/wp-content/uploads/2018/04/ad.jpg 720w, http://bitther.nanoagency.co/wp-content/uploads/2018/04/ad-300x38.jpg 300w, http://bitther.nanoagency.co/wp-content/uploads/2018/04/ad-600x75.jpg 600w" sizes="(max-width: 720px) 100vw, 720px" /></div>
	              </div>
	           </div>
	           <div
	              class="header-content-right hidden-md hidden-lg">
	              <div
	                 class="searchform-mini searchform-moblie hidden-md hidden-lg">
	                 <button
	                    class="btn-mini-search"><i
	                    class="ti-search"></i></button>
	              </div>
	              <div
	                 class="searchform-wrap search-transition-wrap bitther-hidden">
	                 <div
	                    class="search-transition-inner">
	                    <form
	                       method="get"  class="searchform" action="http://bitther.nanoagency.co/">
	                       <div
	                          class="input-group">
	                          <input
	                             type="text" class="form-control" placeholder="Search ... " value="" name="s" />
	                          <span
	                             class="input-group-btn">
	                          <button
	                             class="btn btn-primary"><i
	                             class="ti-search"></i></button>
	                          </span>
	                       </div>
	                    </form>
	                    <button
	                       class="btn-mini-close pull-right"><i
	                       class="ti-close"></i></button>
	                 </div>
	              </div>
	           </div>
	           <div
	              class="header-content bar header-fixed">
	              <div
	                 class="bitther-header-content container">
	                 <div
	                    id="na-menu-primary" class="nav-menu clearfix">
	                    <nav
	                       class="text-center na-menu-primary clearfix">
	                       <ul
	                          id="menu-primary-navigation" class="nav navbar-nav na-menu mega-menu">
	                          <li
	                             id="menu-item-1589" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-1589">
	                             <a
	                                href="#">Home</a>
	                             <ul
	                                class="sub-menu">
	                                <li
	                                   id="menu-item-1503" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-1503"><a
	                                   href="http://bitther.nanoagency.co/">Home 1</a></li>
	                                <li
	                                   id="menu-item-1502" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1502"><a
	                                   href="http://bitther.nanoagency.co/home-2/">Home 2</a></li>
	                                <li
	                                   id="menu-item-1501" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1480 current_page_item menu-item-1501"><a
	                                   href="http://bitther.nanoagency.co/home-3/">Home 3</a></li>
	                                <li
	                                   id="menu-item-1588" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1588"><a
	                                   href="http://bitther.nanoagency.co/home-4/">Home 4</a></li>
	                             </ul>
	                          </li>
	                          <li
	                             id="menu-item-1885" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1885">
	                             <a
	                                href="#">Feature</a>
	                             <ul
	                                class="sub-menu">
	                                <li
	                                   id="menu-item-1954" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1954">
	                                   <a
	                                      href="#">Header</a>
	                                   <ul
	                                      class="sub-menu">
	                                      <li
	                                         id="menu-item-2094" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2094"><a
	                                         href="http://bitther.nanoagency.co/header-left/">Header Left</a></li>
	                                      <li
	                                         id="menu-item-2095" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2095"><a
	                                         href="http://bitther.nanoagency.co/header-center/">Header Center</a></li>
	                                      <li
	                                         id="menu-item-2103" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2103"><a
	                                         href="http://bitther.nanoagency.co/header-white/">Header White</a></li>
	                                      <li
	                                         id="menu-item-2097" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2097"><a
	                                         href="http://bitther.nanoagency.co/home-4/">Header Simple</a></li>
	                                   </ul>
	                                </li>
	                                <li
	                                   id="menu-item-1934" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1934">
	                                   <a
	                                      href="#">Category</a>
	                                   <ul
	                                      class="sub-menu">
	                                      <li
	                                         id="menu-item-1935" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1935"><a
	                                         href="http://bitther.nanoagency.co/category/bitcoin/?col=col-md-6&#038;content=grid&#038;layout=right">Categories Grid</a></li>
	                                      <li
	                                         id="menu-item-1936" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1936"><a
	                                         href="http://bitther.nanoagency.co/category/bitcoin/">Categories List</a></li>
	                                      <li
	                                         id="menu-item-2086" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-2086"><a
	                                         href="http://bitther.nanoagency.co/category/investment/">Category Left Sidebar</a></li>
	                                      <li
	                                         id="menu-item-1940" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1940"><a
	                                         href="http://bitther.nanoagency.co/category/bitcoin/?layout=right">Category Right Sidebar</a></li>
	                                      <li
	                                         id="menu-item-2087" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2087"><a
	                                         href="http://bitther.nanoagency.co/category/investment/?layout=full">Without Sidebar</a></li>
	                                   </ul>
	                                </li>
	                                <li
	                                   id="menu-item-1886" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1886">
	                                   <a
	                                      href="#">Single post</a>
	                                   <ul
	                                      class="sub-menu">
	                                      <li
	                                         id="menu-item-1888" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1888"><a
	                                         href="http://bitther.nanoagency.co/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-3/">Right Sidebar</a></li>
	                                      <li
	                                         id="menu-item-2084" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2084"><a
	                                         href="http://bitther.nanoagency.co/2018/04/15/singapore-central-bank-assessing-need-for-more-cryptocurrency/?layout=left">Left Sidebar</a></li>
	                                      <li
	                                         id="menu-item-2085" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2085"><a
	                                         href="http://bitther.nanoagency.co/2018/04/15/northern-ireland-property-developer-to-accept-bitcoin-as-payment/?layout=full">Without Sidebar</a></li>
	                                      <li
	                                         id="menu-item-1921" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1921"><a
	                                         href="http://bitther.nanoagency.co/2018/04/15/liechtensteins-bank-frick-introduces-direct-cryptocurrency-investment-4/">Single Style 1</a></li>
	                                      <li
	                                         id="menu-item-2082" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2082"><a
	                                         href="http://bitther.nanoagency.co/2018/04/15/at-tax-time-who-really-owns-that-crypto-anyway-expert-take-2/?single=2">Single Style 2</a></li>
	                                      <li
	                                         id="menu-item-2083" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2083"><a
	                                         href="http://bitther.nanoagency.co/2018/04/15/bank-of-china-files-patent-for-new-blockchain-scaling-solution/?single=3">Single Style 3</a></li>
	                                   </ul>
	                                </li>
	                             </ul>
	                          </li>
	                          <li
	                             id="menu-item-2104" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-2104"><a
	                             href="http://bitther.nanoagency.co/category/bitcoin/">Bitcoin</a></li>
	                          <li
	                             id="menu-item-2105" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-2105"><a
	                             href="http://bitther.nanoagency.co/category/investment/">Investment</a></li>
	                          <li
	                             id="menu-item-1774" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1774"><a
	                             href="http://bitther.nanoagency.co/contact-us/">Contact Us</a></li>
	                          <li
	                             id="menu-item-2108" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-has-children menu-item-2108">
	                             <a
	                                href="http://bitther.nanoagency.co/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-3/">Single</a>
	                             <ul
	                                class="sub-menu">
	                                <li
	                                   id="menu-item-1950" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1950"><a
	                                   href="http://bitther.nanoagency.co/2018/04/15/goldman-sachs-keeps-criticizing-bitcoin-but-there-are-certain-ties-3/">Post Format Youtube</a></li>
	                                <li
	                                   id="menu-item-1951" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1951"><a
	                                   href="http://bitther.nanoagency.co/2018/04/15/liechtensteins-bank-frick-introduces-direct-cryptocurrency-investment-4/">Post format gallery</a></li>
	                                <li
	                                   id="menu-item-1952" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1952"><a
	                                   href="http://bitther.nanoagency.co/2018/04/15/singapore-central-bank-assessing-need-for-more-cryptocurrency-2/">Post Audio</a></li>
	                                <li
	                                   id="menu-item-1953" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1953"><a
	                                   href="http://bitther.nanoagency.co/2018/04/15/at-tax-time-who-really-owns-that-crypto-anyway-expert-take/">Post format standard</a></li>
	                             </ul>
	                          </li>
	                       </ul>
	                    </nav>
	                 </div>
	                 <div
	                    class="header-content-right">
	                    <div
	                       class="searchform-mini ">
	                       <button
	                          class="btn-mini-search"><i
	                          class="ti-search"></i></button>
	                    </div>
	                    <div
	                       class="searchform-wrap search-transition-wrap bitther-hidden">
	                       <div
	                          class="search-transition-inner">
	                          <form
	                             method="get"  class="searchform" action="http://bitther.nanoagency.co/">
	                             <div
	                                class="input-group">
	                                <input
	                                   type="text" class="form-control" placeholder="Search ... " value="" name="s" />
	                                <span
	                                   class="input-group-btn">
	                                <button
	                                   class="btn btn-primary"><i
	                                   class="ti-search"></i></button>
	                                </span>
	                             </div>
	                          </form>
	                          <button
	                             class="btn-mini-close pull-right"><i
	                             class="fa fa-close"></i></button>
	                       </div>
	                    </div>
	                 </div>
	              </div>
	           </div>
	        </div>
	    </header>

	