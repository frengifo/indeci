<?php $asset_path = get_template_directory_uri(); ?>
<div class="table-block blue-table">
	<!-- <p>Ley del Sistema Nacional de Gestión del Riesgo de Desastres</p> -->
	<?php $codigos = get_field('documento'); ?>

	<?php if ($codigos): ?>
		
		<?php foreach ($codigos as $key => $v): ?>
		
			<table>
				<thead>
					<tr>
						<th width="270">Documento</th>
						<th>Sumilla</th>
						<th width="80">PDF</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($v['lista'] as $j => $documento): ?>
						
						<tr>
							<td><?php echo $v['nombre'] ?></td>
							<td><?php echo $documento['sumilla'] ?></td>
							<td><a href="<?php echo $documento['pdf'] ?>" target="_blank"><img src="<?php echo $asset_path; ?>/assets/img/icon-pdf.png"></a></td>
						</tr>

					<?php endforeach ?>

				</tbody>
			</table>
		
		<?php endforeach ?>
	
	<?php endif ?>

</div>